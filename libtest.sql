-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-09-2014 a las 12:29:10
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `libtest`
--
CREATE DATABASE IF NOT EXISTS `libtest` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `libtest`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alerta`
--

DROP TABLE IF EXISTS `alerta`;
CREATE TABLE IF NOT EXISTS `alerta` (
  `idALERTA` int(11) NOT NULL AUTO_INCREMENT,
  `FECHA` date NOT NULL,
  `TIPO` int(11) NOT NULL,
  `DESTINATARIO` varchar(64) COLLATE utf8_spanish_ci NOT NULL,
  `MENSAJE` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `ENLACE` varchar(45) COLLATE utf8_spanish_ci NOT NULL DEFAULT '#',
  `RECIBIDA` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idALERTA`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=24 ;

--
-- Volcado de datos para la tabla `alerta`
--

INSERT INTO `alerta` (`idALERTA`, `FECHA`, `TIPO`, `DESTINATARIO`, `MENSAJE`, `ENLACE`, `RECIBIDA`) VALUES
(4, '2014-09-20', 3, 'adm@test.com', 'La siguiente auditoría se encuentra ahora Cerrada.', 'auditoria/7', 0),
(5, '2014-09-18', 3, 'adm@test.com', 'La siguiente auditoría se encuentra ahora Cerrada.', 'auditoria/7', 0),
(9, '2014-09-03', 1, 'adm@test.com', 'Test', '#', 0),
(13, '2014-09-03', 1, 'aud@test.com', 'Test', '#', 0),
(22, '2014-09-30', 5, 'aud2@test.com', '', '#', 0),
(23, '2014-09-30', 5, 'aud2@test.com', 'Felicidades, su petición ha sido aprobada.', '#', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alerta_tipos`
--

DROP TABLE IF EXISTS `alerta_tipos`;
CREATE TABLE IF NOT EXISTS `alerta_tipos` (
  `idTIPO` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `DESCRIPCION` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idTIPO`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `alerta_tipos`
--

INSERT INTO `alerta_tipos` (`idTIPO`, `NOMBRE`, `DESCRIPCION`) VALUES
(1, 'AUDIT_FECHA_INI', 'Auditoría próxima a su apertura.'),
(2, 'AUDIT_NOCON_NEW', 'Nuevo fallo/mejora detectado.'),
(3, 'AUDIT_STATUS_CHANGE', 'Cambio de estado en auditoría'),
(4, 'LIB_STATUS_CHANGE', 'Cambio en su estado'),
(5, 'AUD_STATUS_CHANGE', 'Cambio en su estado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditor`
--

DROP TABLE IF EXISTS `auditor`;
CREATE TABLE IF NOT EXISTS `auditor` (
  `idAUDITOR` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `NOMBRE` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `APELLIDOS` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CODIGO_SOLICITUD` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `HOMOLOGADO` tinyint(1) DEFAULT NULL,
  `FECHA_HOMOLOGACION` date DEFAULT NULL,
  PRIMARY KEY (`idAUDITOR`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=24 ;

--
-- Volcado de datos para la tabla `auditor`
--

INSERT INTO `auditor` (`idAUDITOR`, `user_email`, `NOMBRE`, `APELLIDOS`, `CODIGO_SOLICITUD`, `HOMOLOGADO`, `FECHA_HOMOLOGACION`) VALUES
(2, 'aud@test.com', 'Jose Manuel', 'Rodrigo Peña', '', 1, '0000-00-00'),
(3, 'aud2@test.com', 'AUDITOR 2', 'APELLIDO 2', '', 0, '2014-09-30'),
(4, 'GeremasUrbinaMontalvo@gustr.com', 'Geremías', 'Urbina Montalvo', NULL, 1, '2014-01-01'),
(5, 'HeidyGarayJaime@superrito.com', 'Heidy', 'Garay Jaime', NULL, 1, '2014-02-01'),
(6, 'AristeoCarreraTrevio@gustr.com', 'Aristeo', 'Carrera Treviño', NULL, 1, '2014-03-01'),
(7, 'AgripinaArenasGodoy@superrito.com', 'Agripina', 'Arenas Godoy', NULL, 1, '2014-04-01'),
(8, 'AnianGastelumBonilla@gustr.com', 'Anian', 'Gastelum Bonill', NULL, 1, '2014-05-01'),
(9, 'TerencioNaranjoAlfaro@superrito.com', 'Terencio', 'Naranjo Alfaro', NULL, 1, '2014-06-01'),
(10, 'BernarditaMrquezMota@superrito.com', 'Bernardita', 'Marquez Mota', NULL, 1, '2014-07-01'),
(11, 'BalbinoOrtizGranado@superrito.com', 'Balbino', 'Ortiz Granado', NULL, 1, '2014-08-01'),
(12, 'aezacariasramon4@yopmail.com', 'Zacarias Ramón', '	Principal Buzila ', NULL, 1, '2014-09-01'),
(13, 'eolindao14@yopmail.com', 'Saúl Rosario', 'De la Guerra Lindao', NULL, 1, '2014-10-01'),
(14, 'hddiazdegarayo3@yopmail.com', 'Celia A.', 'Savchuk Díaz de Garayo', NULL, 1, '2014-11-01'),
(15, 'bgrubau6@yopmail.com', 'Jennifer B.', 'Barluenga Rubau', NULL, 1, '2014-12-01'),
(16, 'FabricioSalgadoMaestas@gustr.com', 'Fabricio', 'Salgado Maestas', NULL, 0, '2014-08-04'),
(17, 'cmc@mail.com', 'Cecilio', 'Mariano Conchita', NULL, 0, '2014-08-05'),
(18, 'stp@miemail.es', 'Sofía', 'Tristán Pascual', NULL, 0, '2014-08-06'),
(19, 'pcc0001@email.es', 'Poncio', 'Carlota Cruz', NULL, 0, '2014-08-07'),
(20, 'apr0045@email.es', 'Ángela', 'Perpetua Rolando', NULL, 0, '2014-08-08'),
(21, 'VaninaBarriosQuintero@gustr.com', 'Vanina', 'Barrios Quintero', NULL, 0, '2014-08-09'),
(22, 'PaddyMiramontesRodrguez@gustr.com', 'Paddy', 'Miramontes Rodrguez', NULL, 0, '2014-08-10'),
(23, 'aaa@aaa.aaa', 'A', 'a', '', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria`
--

DROP TABLE IF EXISTS `auditoria`;
CREATE TABLE IF NOT EXISTS `auditoria` (
  `idAUDITORIA` int(11) NOT NULL AUTO_INCREMENT,
  `TIPO_AUDITORIA` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `ALCANCE` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FECHA_APERTURA` date NOT NULL,
  `FECHA_REALIZACION` date NOT NULL,
  `FECHA_CIERRE` date NOT NULL,
  `HORA_INICIO` time NOT NULL,
  `HORA_FIN` time NOT NULL,
  `ESTADO` tinyint(1) NOT NULL,
  `idLIBRERIA` int(11) NOT NULL,
  `idAUDITOR` int(11) DEFAULT NULL,
  `idCERTIFICADO` int(11) DEFAULT NULL,
  PRIMARY KEY (`idAUDITORIA`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=26 ;

--
-- Volcado de datos para la tabla `auditoria`
--

INSERT INTO `auditoria` (`idAUDITORIA`, `TIPO_AUDITORIA`, `ALCANCE`, `FECHA_APERTURA`, `FECHA_REALIZACION`, `FECHA_CIERRE`, `HORA_INICIO`, `HORA_FIN`, `ESTADO`, `idLIBRERIA`, `idAUDITOR`, `idCERTIFICADO`) VALUES
(3, 'CERTIFICACION', 'Objeto y alcance', '2014-09-04', '2014-09-10', '2014-09-30', '21:00:00', '21:12:00', 3, 5, 2, 5),
(4, 'MANTENIMIENTO', 'Objeto y alcance', '2014-08-01', '2014-07-13', '2014-09-01', '09:25:00', '12:15:00', 4, 5, 6, NULL),
(7, 'CERTIFICACION', '', '2014-09-09', '2014-09-11', '2014-11-07', '00:00:00', '01:01:00', 3, 4, 3, 14),
(13, 'MANTENIMIENTO', '', '2015-09-10', '0000-00-00', '2015-12-10', '00:00:00', '01:00:00', 3, 5, NULL, 5),
(14, 'MANTENIMIENTO', '', '2015-09-10', '0000-00-00', '2015-12-10', '00:00:00', '01:00:00', 3, 5, NULL, 5),
(18, 'RENOVACION', '', '2015-09-10', '0000-00-00', '2015-12-10', '00:00:00', '06:00:00', 3, 5, NULL, 6),
(19, 'MANTENIMIENTO', '', '2015-09-10', '0000-00-00', '2015-12-10', '00:00:00', '01:00:00', 3, 5, NULL, 6),
(20, 'MANTENIMIENTO', '', '2015-09-10', '0000-00-00', '2015-12-10', '00:00:00', '00:01:00', 3, 5, NULL, 6),
(21, 'RENOVACION', '', '2015-09-10', '0000-00-00', '2015-12-10', '00:00:00', '00:01:00', 3, 5, NULL, 9),
(22, 'MANTENIMIENTO', NULL, '2015-09-10', '0000-00-00', '2015-12-10', '00:00:00', '00:00:00', 0, 5, NULL, 7),
(23, 'MANTENIMIENTO', NULL, '2015-09-10', '0000-00-00', '2015-12-10', '00:00:00', '00:00:00', 0, 5, NULL, 8),
(24, 'MANTENIMIENTO', NULL, '2015-09-10', '0000-00-00', '2015-12-10', '00:00:00', '00:00:00', 0, 5, NULL, 9),
(25, 'MANTENIMIENTO', '', '2014-09-17', '0000-00-00', '2015-12-18', '00:00:00', '01:00:00', 2, 4, NULL, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `certificado`
--

DROP TABLE IF EXISTS `certificado`;
CREATE TABLE IF NOT EXISTS `certificado` (
  `idCERTIFICADO` int(11) NOT NULL AUTO_INCREMENT,
  `FECHA_EMISION` date DEFAULT NULL,
  `FECHA_CADUCIDAD` date DEFAULT NULL,
  `FECHA_ENVIO` date DEFAULT NULL,
  `ENVIADO` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idCERTIFICADO`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `certificado`
--

INSERT INTO `certificado` (`idCERTIFICADO`, `FECHA_EMISION`, `FECHA_CADUCIDAD`, `FECHA_ENVIO`, `ENVIADO`) VALUES
(1, '2014-07-01', '2017-09-29', '2014-07-02', 1),
(2, '2014-09-10', '2017-09-10', '0000-00-00', 0),
(5, '2014-09-10', '2017-09-10', '0000-00-00', 0),
(6, '2014-09-10', '2017-09-10', '0000-00-00', 0),
(7, '2014-09-10', '2017-09-10', '0000-00-00', 0),
(8, '2014-09-10', '2017-09-10', '0000-00-00', 0),
(9, '2014-09-10', '2017-09-10', '0000-00-00', 0),
(10, '2014-09-18', '2017-09-18', '0000-00-00', 0),
(11, '2014-09-18', '2017-09-18', '0000-00-00', 0),
(12, '2014-09-18', '2017-09-18', '0000-00-00', 0),
(13, '2014-09-18', '2017-09-18', '0000-00-00', 0),
(14, '2014-09-18', '2017-09-18', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentacion`
--

DROP TABLE IF EXISTS `documentacion`;
CREATE TABLE IF NOT EXISTS `documentacion` (
  `idDOCUMENTACION` int(11) NOT NULL AUTO_INCREMENT,
  `RUTA` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `FECHA` date NOT NULL,
  `AUTOR` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `idREF` int(11) DEFAULT NULL,
  `TIPO` int(11) NOT NULL,
  PRIMARY KEY (`idDOCUMENTACION`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=21 ;

--
-- Volcado de datos para la tabla `documentacion`
--

INSERT INTO `documentacion` (`idDOCUMENTACION`, `RUTA`, `FECHA`, `AUTOR`, `idREF`, `TIPO`) VALUES
(1, 'private/audits/audit_3/20140804/a.pdf', '2014-08-04', '2', 3, 4),
(4, 'private/audits/audit_4/20140804/a.pdf', '2014-08-04', '5', 4, 1),
(7, 'private/audits/audit_4/20140804/a.pdf', '2014-08-04', '4', 7, 1),
(8, 'private/audits/audit_6/20140807/Cronograma Librería de Referencia Cultural.pdf', '2014-08-07', '4', 6, 1),
(9, 'private/audits/audit_3/20140807/a.pdf', '2014-08-07', '5', 3, 1),
(10, 'private/audits/audit_4/20140808/a.pdf', '2014-08-08', '2', 4, 4),
(11, 'private/audits/audit_4/20140808/a.pdf', '2014-08-08', '2', 4, 4),
(12, 'private/audits/audit_4/20140808/a.pdf', '2014-09-02', '5', 1, 2),
(13, 'private/audits/audit_4/20140808/b.pdf', '2014-09-02', '5', 1, 2),
(14, 'private/audits/audit_4/20140808/b.pdf', '2014-09-02', '5', 3, 2),
(15, 'http://localhost/libreria/private/Cronograma%20Librer%C3%ADa%20de%20Referencia%20Cultural.pdf', '2014-09-30', 'lib@test.com', 4, 3),
(16, 'http://localhost/libreria/private/a.pdf', '2014-09-30', 'lib@test.com', 4, 3),
(17, 'http://localhost/libreria/private/a%20%281%29.pdf', '2014-09-30', 'lib@test.com', 4, 3),
(18, 'http://localhost/libreria/private/Manual.odt', '2014-09-30', 'lib@test.com', 4, 3),
(19, 'http://localhost/libreria/private/Frangipani%20Flowers.jpg', '2014-09-30', 'aud2@test.com', 3, 5),
(20, 'http://localhost/libreria/private/Incidencias%20Llodio.pdf', '2014-09-30', 'aud2@test.com', 3, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentacion_tipos`
--

DROP TABLE IF EXISTS `documentacion_tipos`;
CREATE TABLE IF NOT EXISTS `documentacion_tipos` (
  `idTIPO` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `DESCRIPCION` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idTIPO`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `documentacion_tipos`
--

INSERT INTO `documentacion_tipos` (`idTIPO`, `NOMBRE`, `DESCRIPCION`) VALUES
(1, 'LIB_AUDIT_DOC', 'Documentación de la auditoría'),
(2, 'LIB_AUDIT_NOCON', 'Adjunto de no conformidad'),
(3, 'LIB_DOC', 'Documentación de librería'),
(4, 'AUD_AUDIT_DOC', 'Informe de auditoría del auditor'),
(5, 'AUD_DOC', 'Documentación del auditor'),
(6, 'ADM', 'Archivos del responsable de la marca');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

DROP TABLE IF EXISTS `evento`;
CREATE TABLE IF NOT EXISTS `evento` (
  `idEVENTO` int(11) NOT NULL AUTO_INCREMENT,
  `TITULO` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `DESCRIPCION` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `URL` varchar(45) COLLATE utf8_spanish_ci DEFAULT '#',
  `FECHA_INICIO` timestamp NOT NULL,
  `FECHA_FIN` timestamp NOT NULL,
  `DESTINATARIO` varchar(64) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idEVENTO`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `evento`
--

INSERT INTO `evento` (`idEVENTO`, `TITULO`, `DESCRIPCION`, `URL`, `FECHA_INICIO`, `FECHA_FIN`, `DESTINATARIO`) VALUES
(2, 'Mañana y pasado', 'Mañana y pasado', '#', '2014-09-17 22:00:00', '2014-09-18 22:00:00', NULL),
(5, 'Mi evento', 'Descripción', '#', '2014-09-20 09:15:00', '2014-09-23 19:50:00', NULL),
(6, 'Evento privado', 'Evento privado desc', '#', '2014-09-17 09:09:28', '2014-09-17 21:59:00', 'adm@test.com'),
(9, 'Navidad', 'Navidad fun fun fun', '#', '2014-12-24 23:00:00', '2014-12-25 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evidencia`
--

DROP TABLE IF EXISTS `evidencia`;
CREATE TABLE IF NOT EXISTS `evidencia` (
  `idEVIDENCIA` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `DESCRIPCION` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `idAUDITORIA` int(11) NOT NULL,
  PRIMARY KEY (`idEVIDENCIA`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `evidencia`
--

INSERT INTO `evidencia` (`idEVIDENCIA`, `NOMBRE`, `DESCRIPCION`, `idAUDITORIA`) VALUES
(1, 'C', 'Ok', 4),
(2, 'B', 'Ok', 4),
(3, 'A', 'Mal', 4),
(4, '1gytfgyty', '2', 3),
(5, 'ytgy', '', 3),
(6, '', '', 3),
(7, '', '', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libreria`
--

DROP TABLE IF EXISTS `libreria`;
CREATE TABLE IF NOT EXISTS `libreria` (
  `idLIBRERIA` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `RAZON_SOCIAL` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CIF` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `NOMBRE_COMERCIAL` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `NOMBRE_TITULAR` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `DIRECCION` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CP` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `POBLACION` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PROVINCIA` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `APTO` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idLIBRERIA`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `libreria`
--

INSERT INTO `libreria` (`idLIBRERIA`, `user_email`, `RAZON_SOCIAL`, `CIF`, `NOMBRE_COMERCIAL`, `NOMBRE_TITULAR`, `DIRECCION`, `CP`, `POBLACION`, `PROVINCIA`, `APTO`) VALUES
(4, 'lib@test.com', 'Razon social', 'CIF', 'Nombre comercia', 'Nombre titular', 'Direccion', 'CP', 'Poblacion', 'Provincia', 1),
(5, 'lib2@test.com', 'RAZON SOCIAL 2', 'CIF 2', 'NOMBRE COMERCIAL 2', 'NOMBRE TITULAR 2', 'DIRECCIÓN 2', 'CP 2', 'POBLACION 2', 'PROVINCIA 2', 0),
(6, 'arcu@mattisvelit.co.uk', 'lobortis ultrices.', '93668', 'Rhoncus Donec Associates', 'Elvis Ross', 'Ap #227-4902 Dui. Avenue', '47121', 'Córdoba', 'Andalucía', 0),
(7, 'Aliquam.erat.volutpat@miacmattis.ca', 'risus', '48830', 'Tellus Suspendisse Sed LLC', 'Unity Randall', '866-6695 Egestas Av.', '16296', 'Alcalá de Henares', 'Madrid', 0),
(8, 'orci@litora.com', 'ornare tortor at risus. Nunc', '05373', 'Luctus Ut Foundation', 'Sybil Perez', '2314 Vel Avenue', '13065', 'Córdoba', 'AN', 0),
(9, 'commodo.ipsum@ipsum.ca', 'non sapien molestie orci tincidunt adipiscing', '81081', 'Adipiscing Elit Etiam Associates', 'Quyn Mccullough', 'Ap #294-7640 Nulla St.', '27006', 'Jerez de la Frontera', 'Andalucía', 0),
(10, 'eros.non.enim@senectusetnetus.com', 'mauris, rhoncus id, mollis nec,', '64201', 'Purus Accumsan Interdum PC', 'Giacomo Vega', '388-1519 Purus St.', '26533', 'Cartagena', 'Murcia', 0),
(11, 'ut.aliquam.iaculis@malesuada.net', 'nisi dictum augue malesuada malesuada. Intege', '60511', 'At LLC', 'Justin Riggs', 'Ap #836-6137 Ligula. St.', '93566', 'Jaén', 'Andalucía', 0),
(12, 'dapibus.ligula@luctus.net', 'non, luctus', '54671', 'A Nunc Corporation', 'Conan Russell', 'P.O. Box 638, 9094 Semper Road', '07855', 'Salamanca', 'Castilla y León', 0),
(13, 'faucibus.orci.luctus@risusDuisa.edu', 'tortor nibh sit amet orci. Ut sagittis lobort', '48139', 'Enim Ltd', 'Maile Clarke', 'Ap #573-4271 Adipiscing, St.', '99117', 'Albacete', 'CM', 0),
(14, 'lobortis@sitamet.com', 'feugiat non, lobortis quis, pede. Suspendisse', '29957', 'Dictum Ultricies Limited', 'Yardley Ferrell', '8284 Enim. Road', '83922', 'Murcia', 'MU', 0),
(15, 'velit.justo@Classaptent.ca', 'molestie tortor nibh sit amet orci. Ut', '63027', 'Quisque Associates', 'Cade Guzman', 'P.O. Box 225, 230 Morbi Rd.', '57488', 'Lugo', 'GA', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `no_conformidades`
--

DROP TABLE IF EXISTS `no_conformidades`;
CREATE TABLE IF NOT EXISTS `no_conformidades` (
  `idNO_CONFORMIDADES` int(11) NOT NULL AUTO_INCREMENT,
  `TIPO` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `DESCRIPCION` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FECHA_DETECCION` date DEFAULT NULL,
  `FECHA_RESOLUCION` date DEFAULT NULL,
  `idAUDITORIA` int(11) NOT NULL,
  PRIMARY KEY (`idNO_CONFORMIDADES`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `no_conformidades`
--

INSERT INTO `no_conformidades` (`idNO_CONFORMIDADES`, `TIPO`, `DESCRIPCION`, `FECHA_DETECCION`, `FECHA_RESOLUCION`, `idAUDITORIA`) VALUES
(1, 'CERRADA', 'Bien', '2014-12-18', '2014-12-22', 3),
(2, 'CERRADA', 'Fallo 11', '2000-12-20', '2001-01-19', 3),
(3, 'CERRADA', 'Error Grave', '2000-12-12', '2014-09-02', 3),
(4, 'CERRADA', '123', '2015-07-10', '0000-00-00', 3),
(8, 'GRAVE', 'A', '2012-12-12', '2014-12-10', 4),
(9, 'GRAVE', 'B', '2012-12-12', '0000-00-00', 4),
(10, 'CERRADA', 'Ok', '2009-12-01', '0000-00-00', 3),
(11, 'LEVE', 'Test', '2014-08-29', '0000-00-00', 3),
(12, 'GRAVE', 'Test2', '2013-11-25', '2014-02-14', 3),
(13, 'GRAVE', '', '2013-11-25', '0000-00-00', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto incrementing user_id of each user, unique index',
  `user_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s password in salted and hashed format',
  `user_email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s email, unique',
  `user_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'user''s activation status',
  `user_activation_hash` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'user''s email verification hash string',
  `user_password_reset_hash` char(40) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'user''s password reset code',
  `user_password_reset_timestamp` bigint(20) DEFAULT NULL COMMENT 'timestamp of the password reset request',
  `user_rememberme_token` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'user''s remember-me cookie token',
  `user_failed_logins` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'user''s failed login attemps',
  `user_last_failed_login` int(10) DEFAULT NULL COMMENT 'unix timestamp of last failed login attempt',
  `user_registration_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_registration_ip` varchar(39) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0.0.0.0',
  `user_type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user data' AUTO_INCREMENT=25 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `user_password_hash`, `user_email`, `user_active`, `user_activation_hash`, `user_password_reset_hash`, `user_password_reset_timestamp`, `user_rememberme_token`, `user_failed_logins`, `user_last_failed_login`, `user_registration_datetime`, `user_registration_ip`, `user_type`) VALUES
(22, '$2y$10$.tWvOq./SU1/fH2L/IEyqOSsPunyY0n5cxJ97WfPegdJXCGGArs0i', 'adm@test.com', 1, '114701e78710a96b4e2aa2b7a24a46a64b9c96c2', NULL, NULL, NULL, 0, NULL, '2014-08-05 09:58:27', '::1', 'ADM'),
(19, '$2y$10$1SFb3CaX4XdGmSmXItXta.MPgKDt0mxz5XbGRmNxRVIXm2Jv4Vgc2', 'lib2@test.com', 1, '83b6c57419a24bffb8c8007efab7cea53f5797fd', NULL, NULL, NULL, 0, NULL, '2014-07-16 11:16:25', '::1', 'LIB'),
(18, '$2y$10$IcQocNgKBRIGUJQN.8jj6uuYuoU/D.JO.N8dMXFeQI7bBJzHqf0oG', 'aud@test.com', 1, '6b3cf4cb70d0a5c141779b17e63267896651ef02', NULL, NULL, NULL, 0, NULL, '2014-07-15 09:16:40', '::1', 'AUD'),
(17, '$2y$10$aqEMLQi1Cu6vmGkAQgOeruDDes40rnuBWA87oOVMUpq8wWzY8iRFq', 'lib@test.com', 1, '6cb6329b8d0194cf1b1dc70691e32b2cd4fb303e', NULL, NULL, NULL, 0, NULL, '2014-07-10 13:04:55', '::1', 'LIB'),
(20, '$2y$10$1ac9k7XPgfLFcScDL0S3dOyyTH1mK9b/.HchmjCvotsxIft.BrSO.', 'aud2@test.com', 1, '630270835c764e145cfec4affbb88fc102b79041', NULL, NULL, NULL, 0, NULL, '2014-07-16 11:23:43', '::1', 'AUD'),
(24, '$2y$10$ExbPa7vjX16gd4nhtVLvxePJguwIv5E.059t1VrbOD2Jy4v/H1ZEe', 'aaa@aaa.aaa', 1, NULL, NULL, NULL, NULL, 0, NULL, '2014-09-18 11:32:09', '::1', 'AUD');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
