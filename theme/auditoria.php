<?php
global $GET;
$return = false;
$post = !isset($_POST['id']) || !($audit_id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT)) ? true : false;
$get = $GET == null || !($audit_id = filter_var($GET, FILTER_VALIDATE_INT)) ? true : false;

if ($post && $get) {
    header('Location: inicio');
    exit;
}
try {
    $audit = new Auditoria($audit_id);
    $libreria = new Libreria($audit->getLibreria());

    if (($id_auditor = $audit->getAuditor()) != null) {
        $auditor = new Auditor($id_auditor);
        $aud_doc = $audit->getAuditorDoc($auditor->getEmail());
    }

    // Cada librero/auditor solo puede acceder a sus auditorias.
    if (($_SESSION['user_type'] == 'LIB' && $libreria->getEmail() != $_SESSION['user_email']) ||
            ($_SESSION['user_type'] == 'AUD' && $auditor->getEmail() != $_SESSION['user_email'])) {
        header('Location:' . Theme::roleRedirect($_SESSION['user_type']));
        exit;
    }

    if (($id_certificado = $audit->getCertificado()) != null) {
        $certificado = new Certificado($id_certificado);
    }

    $lib_doc = $audit->getLibDoc($libreria->getEmail());
    $no_conformidades = $audit->getNoConformidades();
    $evidencias = $audit->getEvidencias();
} catch (Exception $e) {
    ?>
    <section class="container"><div class="row"><div class="col-md-12">
                <div class="alert alert-danger">
                    <?php echo($e->getMessage()); ?>
                    <a href="<?php echo THEME::roleRedirect($_SESSION['user_type']) ?>">Volver atrás.</a>
                </div>
            </div></div></section>
    <?php
    exit;
}

$adm_edit = ($_SESSION['user_type'] == 'ADM' ? true : false);
$edit = (($_SESSION['user_type'] == 'AUD' && $audit->getEstado() == 'Abierta') || $adm_edit ? true : false);
?>
<section class="container">
    <div class="row">  
        <header class="b-line">
            <h1>Informe de Auditoría</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>
        <?php if ($adm_edit) { ?>
            <div class="col-sm-3 col-md-2">
                <?php Theme::loadAdminSideMenu(); ?>
            </div>
            <div class="col-md-9">
            <?php } else { ?>
                <div class="col-md-12"> 
                <?php } ?>
                <ul class="nav nav-tabs" role="tablist">                
                    <li  class="active"><a href="#auditoria" role="tab" data-toggle="tab">Datos de Auditoría</a></li>
                    <li><a href="#usuarios" role="tab" data-toggle="tab">Datos de Librería y Auditor</a></li>
                    <li><a href="#evidencias" role="tab" data-toggle="tab">Evidencias Auditadas</a></li>
                    <li><a href="#nocon" role="tab" data-toggle="tab">Fallos y Mejoras</a></li>
                </ul>
                <?php if ($audit->errors) { ?>            
                    <div class="col-md-12">
                        <div class="alert alert-danger"><?php $audit->showErrors(); ?></div>
                    </div>                      
                <?php } ?>
                <div class="tab-content">
                    <div class="tab-pane active" id="auditoria">                                  
                        <div class="col-md-6"> 
                            <h4>Informe de Auditoría</h4>
                            <?php if ($id_auditor != null && $aud_doc != null) { ?>
                                <ul class="list-unstyled">
                                    <?php foreach ($aud_doc as $doc) { ?>
                                        <li><i class="fa fa-file"></i><a href="<?php echo $doc->getRuta() ?>"> <?php echo $doc->getName(); ?></a><small> <?php echo Util::formatDate($doc->getFecha()); ?></small></li>
                                    <?php } ?> 
                                </ul>
                            <?php } if ($edit) { ?>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            <span>Subir archivos</span>
                                            <!-- The file input field used as target for the file upload widget -->
                                            <input class="fileupload" type="file" name="files[]" multiple data-ref="audit-inf">
                                            <input type="hidden" data-ref="audit-inf" value="AUD_AUDIT_DOC" name="resourcetype[]">
                                            <input type="hidden" data-ref="audit-inf" value="<?php echo $audit->getId(); ?>" name="resourceid[]">
                                        </span>
                                        <div id="progress" class="progress">
                                            <div class="progress-bar progress-bar-success"></div>
                                        </div>
                                        <!-- The container for the uploaded files -->
                                        <div id="files" class="files"></div>
                                        <br>
                                    </div></div>
                            <?php } ?>
                        </div>
                        <div class="col-md-6">
                            <h4>Documentación</h4>
                            <?php if ($lib_doc != null) { ?>
                                <ul class="list-unstyled">
                                    <?php foreach ($lib_doc as $doc) { ?>
                                        <li><i class="fa fa-file"></i></span><a href="<?php echo $doc->getRuta() ?>"> <?php echo $doc->getName(); ?></a><small> <?php echo Util::formatDate($doc->getFecha()); ?></small></li>
                                    <?php } ?> 
                                </ul>
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <h4>Datos de la Auditoría<?php if ($edit) { ?> <small><button class="btn btn-success" id="edit_audit">Editar</button></small><?php } ?></h4>                
                        <form id="audit" class="form" action="auditoria" method="post">
                            <input type="hidden" name="id" value="<?php echo $audit_id; ?>">
                            <div class="form-group col-md-6">
                                <label for="tipo_audit" class="control-label">Tipo de Auditoría</label>
                                <input type="text" class="form-control" id="tipo_audit" name="tipo_audit" value="<?php echo $audit->getTipoAuditoria(); ?>" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="alcance_audit" class="control-label">Objeto y alcance de la auditoría</label>
                                <textarea id="alcance_audit" name="alcance_audit" class="form-control" disabled><?php echo $audit->getAlcance(); ?></textarea>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="fecha_apertura_audit" class="control-label">Fecha de apertura<small> (dd/mm/aaaa)</small></label>
                                <div class="input-group"><input type="text" class="form-control" id="fecha_apertura_audit" name="fecha_apertura_audit" value="<?php echo Util::formatDateLocal($audit->getFechaApertura()); ?>" readonly><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="fecha_cierre_audit" class="control-label date">Fecha de cierre<small> (dd/mm/aaaa)</small></label> <abbr title="Fecha tope en la que la librería podrá aportar respuesta a la auditoría."><span class="fa fa-question-circle"></span></abbr>
                                <div class="input-group date"><input type="text" class="form-control" id="fecha_cierre_audit" name="fecha_cierre_audit" value="<?php echo Util::formatDateLocal($audit->getFechaCierre()); ?>" disabled><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>
                            </div>
                            <?php
                            $date = $audit->getFechaRealizacion();
                            if ((!Util::validateDate($date) || Util::compareTwoDates(date('Y-m-d'), $date)) &&
                                    $_SESSION['user_type'] == 'LIB') {
                                
                            } else {
                                ?>
                                <div class="form-group col-md-4">
                                    <label for="fecha_realizacion_audit" class="control-label date">Fecha de realización<small> (dd/mm/aaaa)</small></label>
                                    <div class="input-group date"><input type="text" class="form-control" id="fecha_realizacion_audit" name="fecha_realizacion_audit" value="<?php echo Util::formatDateLocal($audit->getFechaRealizacion()); ?>" disabled><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inicio_audit" class="control-label">Hora de inicio<small> (hh:mm)</small></label>
                                    <input type="time" class="form-control" id="inicio_audit" name="inicio_audit" value="<?php echo Util::formatTime($audit->getHoraInicio()); ?>" disabled>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="fin_audit" class="control-label">Hora de fin<small> (hh:mm)</small></label>
                                    <input type="time" class="form-control" id="fin_audit" name="fin_audit" value="<?php echo Util::formatTime($audit->getHoraFin()); ?>" disabled>
                                </div>                     
                                <div class="form-group col-md-6">
                                    <label for="conforme_audit" class="control-label">Conforme</label> <abbr title="Indicando la conformidad de la auditoría, esta será enviada al administrador para su validación, por lo que no podrá ser modificada por ninguna de las partes."><i class="fa fa-question-circle"></i></abbr>
                                    <select size="1" class="form-control" id="conforme_audit" name="conforme_audit" disabled>
                                        <?php $conforme = ($audit->isConforme() ? 'SI' : 'NO'); ?>
                                        <option selected><?php echo $conforme; ?></option>
                                        <option><?php echo ($conforme == 'SI' ? 'NO' : 'SI'); ?></option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="estado_audit" class="control-label">Estado</label>
                                    <input type="text" class="form-control" id="estado_audit" name="estado_audit" value="<?php echo $audit->getEstado(); ?>" readonly>
                                </div>
                                <div class="form-group col-md-12" id="btn-send"></div>
                            <?php } ?>
                        </form>
                        <div class="clearfix"></div>
                        <hr/>
                        <h4>Certificado</h4><?php if ($adm_edit && ($audit->getEstado() == 'Pendiente de revisión' || $audit->getEstado() == 'Cerrada')) { ?>
                            <form method="post" action="admin-certificado">
                                <button class="btn btn-info" onclick="submit" name="id" value="<?php echo $audit_id ?>">Emitir o editar certificado</button>
                            </form>
                        <?php } ?>
                        <?php
                        if ($id_certificado != null) {
                            ?>
                            <form class="form-" role="form">
                                <div class="form-group col-md-6">
                                    <label for="fecha_emision_cer" class="control-label">Fecha de emisión</label>
                                    <input type="text" class="form-control" id="fecha_emision_cer" name="fecha_emision_cer" value="<?php echo Util::formatDate($certificado->getFechaEmision()); ?>" disabled>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="fecha_caducidad_cer" class="control-label">Fecha de caducidad</label>
                                    <input type="text" class="form-control" id="fecha_caducidad_cer" name="fecha_caducidad_cer" value="<?php echo Util::formatDate($certificado->getFechaCaducidad()); ?>" disabled>
                                </div>                                                           
                            </form>
                        <?php } else { ?>
                            <p>El ceritificado no ha sido emitido.</p>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                    <!-- User Tab-->
                    <div class="tab-pane" id="usuarios">                      
                        <h4>Datos de Librería</h4>
                        <div class="col-md-6">
                            <strong>Nombre Comercial</strong>
                            <div class="well well-sm"><?php echo $libreria->getNombreComercial(); ?></div>
                        </div>
                        <div class="col-md-6">
                            <strong>Razón Social</strong>
                            <div class="well well-sm"><?php echo $libreria->getRaZonSocial(); ?></div>
                        </div>
                        <div class="col-md-6">
                            <strong>CIF</strong>
                            <div class="well well-sm"><?php echo $libreria->getCIF(); ?></div>
                        </div>                        
                        <div class="col-md-6">
                            <strong>Nombre del Titular</strong>
                            <div class="well well-sm"><?php echo $libreria->getNombreTitular(); ?></div>
                        </div>
                        <div class="col-md-6">
                            <strong>Dirección</strong>
                            <div class="well well-sm"><?php echo $libreria->getDireccion(); ?></div>
                        </div>
                        <div class="col-md-6">
                            <strong>Código Postal</strong>
                            <div class="well well-sm"><?php echo $libreria->getCP(); ?></div>
                        </div>                        
                        <div class="col-md-6">
                            <strong>Población</strong>
                            <div class="well well-sm"><?php echo $libreria->getPoblacion(); ?></div>
                        </div>                        
                        <div class="col-md-6">
                            <strong>Provincia</strong>
                            <div class="well well-sm"><?php echo $libreria->getProvincia(); ?></div>
                        </div>                     
                        <div class="clearfix"></div>
                        <hr/>
                        <h4>Datos del Auditor</h4>
                        <?php if ($id_auditor != null) { ?>
                            <div class="col-md-6">
                                <strong>Nombre</strong>
                                <div class="well well-sm"><?php echo $auditor->getNombre(); ?></div>
                            </div>                        
                            <div class="col-md-6">
                                <strong>Apellidos</strong>
                                <div class="well well-sm"><?php echo $auditor->getApellidos(); ?></div>
                            </div>                                                                                                            
                        <?php } else { ?>
                            <p>Sin asignar</p>
                            <?php if ($adm_edit) { ?>
                                <form method="post" action="admin-auditEdit">
                                    <button onclick="submit" class="btn btn-info" name="id" value="<?php echo $audit_id; ?>">Editar</button>
                                </form>
                                <?php
                            }
                        }
                        ?>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Evidence Tab -->
                    <div class="tab-pane" id="evidencias">
                        <h4>Evidencias Auditadas<?php if ($edit) { ?> <small><button class="btn btn-success" id="edit_evd">Editar</button></small><?php } ?></h4>
                        <form action="auditoria" method="post" id="evidencia">
                            <input type="hidden" name="id" value="<?php echo $audit_id; ?>">
                            <table class="table table-responsive" id="table_evd">
                                <thead><tr><th>Número</th><th>Nombre</th><th>Descripción</th></tr></thead><tbody>
                                    <?php if ($evidencias == null) { ?>
                                    <div class="alert alert-warning">Ningún registro</div> 
                                    <?php
                                } else {
                                    $i = 1;
                                    foreach ($evidencias as $registro) {
                                        ?>
                                        <tr><td><input type="hidden" name="id_evd_<?php echo $i; ?>" value="<?php echo $registro->idEVIDENCIA; ?>"><?php echo $i; ?></td>                                            
                                            <td><input type="text" name="name_evd_<?php echo $i; ?>" value="<?php echo $registro->NOMBRE; ?>" class="simple" disabled></td>
                                            <td><input type="text" name="desc_evd_<?php echo $i; ?>" value="<?php echo $registro->DESCRIPCION; ?>" class="simple" disabled></td>                                            
                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </tbody></table>
                            <div id="btn-evd"></div>
                        </form>
                    </div>
                    <!-- Nocon Tab -->
                    <div class="tab-pane" id="nocon">
                        <h4>Fallos o mejoras<?php if ($edit) { ?> <small><button class="btn btn-success" id="edit_nocon">Editar</button></small><?php } ?></h4>
                        <form action="auditoria" method="post" id="no_conformidad">
                            <input type="hidden" name="id" value="<?php echo $audit_id; ?>">
                            <table class="table table-responsive" id="table_nocon"><thead>
                                    <tr><th>Número</th><th>Tipo</th><th>Descripción</th><th>Fecha de detección<small> (dd/mm/aaaa)</small></th><th>Fecha de resolución<small> (dd/mm/aaaa)</small></th><th>Acciones</th></tr>
                                </thead><tbody>
                                    <?php if ($no_conformidades == null) { ?>
                                    <div class="alert alert-warning">Ningún registro</div> 
                                    <?php
                                } else {
                                    $i = 1;
                                    foreach ($no_conformidades as $nocon) {
                                        ?>
                                        <tr><td><input type="hidden" name="id_nocon_<?php echo $i; ?>" value="<?php echo $nocon->getId(); ?>"><?php echo $i; ?></td>
                                            <td><select name="tipo_nocon_<?php echo $i; ?>" class="simple" disabled>
                                                    <?php foreach ($audit->noconformidad_tipo as $tipo) { ?>
                                                        <option <?php echo ($tipo == $nocon->getTipo() ? 'selected' : ''); ?> ><?php echo $tipo; ?></option>
                                                    <?php } ?>
                                                </select></td>
                                            <td><input type="text" name="desc_nocon_<?php echo $i; ?>" value="<?php echo $nocon->getDescripcion(); ?>" class="simple" disabled></td>
                                            <td><input type="text" name="detect_nocon_<?php echo $i; ?>" value="<?php echo Util::formatDateLocal($nocon->getFechaDeteccion()); ?>" class="simple date" disabled><i class="fa fa-calendar"></i></td>
                                            <td><input type="text" name="resol_nocon_<?php echo $i; ?>" value="<?php echo Util::formatDateLocal($nocon->getFechaResolucion()); ?>" class="simple date" disabled><i class="fa fa-calendar"></i></td>

                                            <?php $nocon_doc = $nocon->getDocumentacion($libreria->getEmail()); ?>

                                            <td>                                                
                                                <a href="#" data-toggle="modal" data-target="#modal<?php echo $i; ?>" title="Adjuntos">
                                                    <i class="fa fa-paperclip"></i>
                                                </a>                                                
                                                <a href="#" title="Eliminar"><i class="fa fa-trash-o alert-danger"></i></a>


                                                <div class="modal fade" id="modal<?php echo $i; ?>">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                <h4 class="modal-title">Adjuntos</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                
                                                                    <ul class="list-unstyled">
                                                                        <?php foreach ($nocon_doc as $doc) { ?>                                                                        
                                                                            <li><i class="fa fa-file"></i> <a href="<?php echo $doc->getRuta(); ?>"><?php echo $doc->getName(); ?></a><small> <?php echo Util::formatDate($doc->getFecha()); ?></small></li>
                                                                        <?php } ?>
                                                                    </ul> 
                                                                
                                                                    <span class="btn btn-success fileinput-button">
                                                                        <i class="glyphicon glyphicon-plus"></i>
                                                                        <span>Subir archivos</span>
                                                                        <!-- The file input field used as target for the file upload widget -->
                                                                        <input class="fileupload" type="file" name="files[]" multiple data-ref="nocon-doc<?php echo $i; ?>">
                                                                        <input type="hidden" data-ref="nocon-doc<?php echo $i; ?>" value="LIB_AUDIT_NOCON" name="resourcetype[]">
                                                                        <input type="hidden" data-ref="nocon-doc<?php echo $i; ?>" value="<?php echo $nocon->getId(); ?>" name="resourceid[]">
                                                                    </span>                                        
                                                                    <!-- The container for the uploaded files -->
                                                                    <div id="files" class="files"></div>
                                                                
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </td>
                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </tr></tbody> </table>
                            <div id="btn-nocon"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</section>
<script src="<?php echo SITE_URL; ?>/theme/assets/js/editform.js"></script>
<script src="<?php echo SITE_URL; ?>/theme/assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo SITE_URL; ?>/theme/assets/js/locales/bootstrap-datepicker.es.js"></script>
<script>    $('#fecha_realizacion_audit').datepicker({
                                            format: "dd/mm/yyyy",
                                            weekStart: 1,
                                            startDate: $('#fecha_apertura_audit').val(),
                                            endDate: $('#fecha_cierre_audit').val(),
                                            language: "es",
                                            daysOfWeekDisabled: "0,6"
                                        });
                                        $('#fecha_cierre_audit').datepicker({
                                            format: "dd/mm/yyyy",
                                            weekStart: 1,
                                            startDate: $('#fecha_apertura_audit').val(),
                                            language: "es",
                                            daysOfWeekDisabled: "0,6"
                                        });
                                        $('input.date').datepicker({
                                            format: "dd/mm/yyyy",
                                            weekStart: 1,
                                            language: "es",
                                            daysOfWeekDisabled: "0,6"
                                        });

</script>
<script>$(document).ready(function() {
        $('#table_evd').DataTable({paging: true, "pagingType": "simple", "sDom": 't<"botton"p>', "pageLength": 10, "ordering": false});
    });</script>

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?php echo SITE_URL; ?>/theme/assets/js/jquery.ui.widget.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo SITE_URL; ?>/theme/assets/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo SITE_URL; ?>/theme/assets/js/jquery.fileupload.js"></script>

<script src="<?php echo SITE_URL; ?>/theme/assets/js/fileuploadapp.js"></script>
