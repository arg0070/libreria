<?php
global $login;
if (!$login->isUserLoggedIn()) {
    header('Location: inicio');
    exit;
}
?>
<section class="container">    
    <div class="row">   
        <header class="b-line">
            <h1>Opciones de cuenta</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>        
        <div class="col-md-9">            
            <?php if ($login->errors) { ?>            
                    <div class="col-md-12">
                        <div class="alert alert-danger"><?php $login->showErrors(); ?></div>
                    </div>                      
                <?php }
                if ($login->messages) { ?>            
                    <div class="col-md-12">
                        <div class="alert alert-info"><?php $login->showMessages(); ?></div>
                    </div>                      
                <?php }?>
            
            <div class="box">
                <h4 class="h-inner">Cambiar dirección de correo electrónico</h4>
            <form action="opciones" method="POST">
                <div class="form-group">
                    <label for="user_email">Nueva dirección de correo electrónico</label>
                    <input class="form-control" type="email" id="user_email" name="user_email" placeholder="Nueva dirección de correo electrónico" required>
                </div>
                <input type="submit" class="btn btn-primary" name="user_edit_submit_email" value="Guardar">
            </form>
            </div>
            <div class="box">

                <h4 class="h-inner">Cambiar contraseña</h4>
            <form action="opciones" method="POST">
                <div class="form-group">
                    <label for="user_password_old">Contraseña actual</label>
                    <input class="form-control" type="password" id="user_password_old" name="user_password_old" placeholder="Contraseña actual" required>
                </div>
                <div class="form-group">
                    <label for="user_password_new">Nueva contraseña</label>
                    <input class="form-control" type="password" id="user_password_new" name="user_password_new" placeholder="Contraseña nueva" required>
                </div>
                <div class="form-group">
                    <label for="user_password_repeat">Repetir contraseña nueva</label>
                    <input class="form-control" type="password" id="user_password_repeat" name="user_password_repeat" placeholder="Repetir la contraseña nueva" required>
                </div>
                <input type="submit" class="btn btn-primary" name="user_edit_submit_password" value="Guardar">
            </form>
            </div>
        </div>
        <aside class="col-md-3 l-line">
            <?php Theme::loadSidePlugins(); ?>
        </aside>
    </div>
</section>

