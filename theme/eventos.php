
<section class="container">    
    <div class="row">   
        <header class="b-line">
            <h1>Calendario de eventos</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>        
        <div class="col-md-9">  

            <div class="pull-right form-inline">
                <div class="btn-group">
                    <button class="btn btn-primary" data-calendar-nav="prev"><i class="fa fa-angle-double-left"></i> Anterior</button>
                    <button class="btn btn-default" data-calendar-nav="today">Hoy</button>
                    <button class="btn btn-primary" data-calendar-nav="next">Siguiente <i class="fa fa-angle-double-right"></i></button>
                </div>
                <div class="btn-group">
                    <button class="btn btn-warning" data-calendar-view="year">Año</button>
                    <button class="btn btn-warning active" data-calendar-view="month">Mes</button>
                    <button class="btn btn-warning" data-calendar-view="week">Semana</button>
                    <button class="btn btn-warning" data-calendar-view="day">Día</button>
                </div>
            </div>
            <div class="page-header">
                <h3></h3>		
            </div>            			
            <div id="calendar"></div>					
            <h4>Eventos</h4>            
            <ul id="eventlist" class="nav nav-list"></ul>		                                    
        </div>
        <aside class="col-md-3 l-line">
            <?php Theme::loadSidePlugins(); ?>
        </aside>
    </div>

    <div class="modal fade" id="events-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body" style="height: 400px">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$edit = (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'ADM') ? 'true' : 'false';
Theme::setToFooter('<script type="text/javascript"> var editable = ' . $edit . ';</script>');
Theme::setToFooter('<script type="text/javascript" src="' . SITE_URL . '/theme/assets/js/calendarapp.js"></script>');


