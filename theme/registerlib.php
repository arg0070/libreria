<section class="container">
    <div class="row">
        <header class="b-line">
            <h1>Librerías</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>
        <div class="col-md-9">           
            <h2>Requisitos Librerías</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
            <ul>
                <li>Requisito</li>
                <li>Requisito</li>
                <li>Requisito</li>
                <li>Requisito</li>
                <li>Requisito</li>
            </ul>
            <p>
                Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris. Integer in mauris eu nibh euismod gravida. Duis ac tellus et risus vulputate vehicula. Donec lobortis risus a elit. Etiam tempor. Ut ullamcorper, ligula eu tempor congue, eros est euismod turpis, id tincidunt sapien risus a quam. Maecenas fermentum consequat mi. Donec fermentum. Pellentesque malesuada nulla a mi. Duis sapien sem, aliquet nec, commodo eget, consequat quis, neque. Aliquam faucibus, elit ut dictum aliquet, felis nisl adipiscing sapien, sed malesuada diam lacus eget erat. Cras mollis scelerisque nunc. Nullam arcu. Aliquam consequat. Curabitur augue lorem, dapibus quis, laoreet et, pretium ac, nisi. Aenean magna nisl, mollis quis, molestie eu, feugiat in, orci. In hac habitasse platea dictumst.
            </p>

            <hr>            
            <h2>Formulario de registro para Librerías</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
            <?php
            $registration = new Registration();
            if ($registration->errors) {
                echo'<div class="alert alert-danger"><p>';
                $registration->showErrors();
                echo'</p></div>';
            }
            if ($registration->messages) {
                echo'<div class="alert alert-info"><p>';
                $registration->showMessages();
                echo'</p></div>';
            }
            ?>
            <form class="box box-border" method="post" action="registerlib" name="registerform">
                <h4 class="h-inner">Datos de cuenta</h4>
                <div class="form-group">
                    <label for="user_email">Dirección de correo electrónico</label> <abbr title="Su dirección de correo electrónico será necesaria para iniciar sesión. Introduzca una dirección de correo válida. Se le enviará un correo de confirmación."><span class="glyphicon glyphicon-question-sign"></span></abbr>
                    <input type="email" class="form-control" id="user_email" placeholder="Introduzca su dirección de correo electrónico" name="user_email" required>
                </div>
                <div class="form-group">
                    <label for="user_password_new">Contraseña</label> <abbr title="Introduzca una contraseña de más de 6 caracteres."><span class="glyphicon glyphicon-question-sign"></span></abbr>
                    <input type="password" class="form-control" id="user_password_new" name="user_password_new" placeholder="Introduzca una contraseña" pattern=".{6,}" required autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="user_password_repeat">Repita la contraseña</label>
                    <input type="password" class="form-control" id="user_password_repeat" name="user_password_repeat" placeholder="Repita la contraseña" pattern=".{6,}" required autocomplete="off">
                </div>
                <h4 class="h-inner">Datos de la empresa</h4>
                <div class="form-group">
                    <label for="lib_razon_social">Razón social</label>
                    <input type="text" class="form-control" id="lib_razon_social" name="lib_razon_social" placeholder="Introduzca la razón social de la empresa." required>
                </div>
                <div class="form-group">
                    <label for="lib_cif">CIF</label>
                    <input type="text" class="form-control" id="lib_cif" name="lib_cif" placeholder="Introduzca el CIF de la empresa." required>
                </div>
                <div class="form-group">
                    <label for="lib_nombre_comercial">Nombre comercial</label>
                    <input type="text" class="form-control" id="lib_nombre_comercial" name="lib_nombre_comercial" placeholder="Introduzca el nombre comercial de la empresa." required>
                </div>
                <div class="form-group">
                    <label for="lib_nombre_titular">Nombre del titular</label>
                    <input type="text" class="form-control" id="lib_nombre_titular" name="lib_nombre_titular" placeholder="Introduzca el nombre del titular de la empresa." required>
                </div>
                <div class="form-group">
                    <label for="lib_direccion">Dirección</label>
                    <input type="text" class="form-control" id="lib_direccion" name="lib_direccion" placeholder="Introduzca la dirección de la empresa." required>
                </div>
                <div class="form-group">
                    <label for="lib_cp">Código postal</label>
                    <input type="text" class="form-control" id="lib_cp" name="lib_cp" placeholder="Introduzca el código postal de la empresa." required>
                </div>
                <div class="form-group">
                    <label for="lib_población">Población</label>
                    <input type="text" class="form-control" id="lib_población" name="lib_poblacion" placeholder="Introduzca la población en la que se ubica la empresa." required>
                </div>
                <div class="form-group">
                    <label for="lib_provincia">Provincia</label>
                    <input type="text" class="form-control" id="lib_provincia" name="lib_provincia" placeholder="Introduzca la provincia en la que se ubica la empresa." required>
                </div>

                <div class="checkbox">
                    <label><input type="checkbox" value="0" id="chk-tos"> Acepto los términos y condiciones.</label></div>                    
                <div class="col-md-5">
                    <input type="submit" id="btn-tos" class="btn btn-info" name="registerlib" value="Enviar" disabled="true"/>
                </div>
            </form>                 
        </div>

        <aside class="col-md-3 l-line">
<?php Theme::loadSidePlugins(); ?>
        </aside>
    </div>
</section>
<script src="<?php echo SITE_URL; ?>/theme/assets/js/tos.js"></script>