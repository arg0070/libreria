
        <footer class="main-footer">
            <div class="container">
                <div class="col-md-4">
                    <h3 class="b-line">Navegación</h3>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-caret-right"></i> <a href="inicio">Inicio</a></li>
                        <li><i class="fa fa-caret-right"></i> <a href="registerlib">Registro de Librerías</a></li>
                        <li><i class="fa fa-caret-right"></i> <a href="registeraud">Registro de Auditores</a></li>
                    </ul>
                </div>
                <div class="col-md-8">
                    <h3 class="b-line">Enlaces</h3>
                    <img class="img-responsive" src="<?php echo SITE_URL; ?>/theme/assets/img/logo.png" alt="Logo" title="Logo">
                </div>
            </div>
        </footer>
                
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo SITE_URL; ?>/theme/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo SITE_URL; ?>/theme/assets/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo SITE_URL; ?>/theme/assets/js/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo SITE_URL; ?>/theme/assets/js/underscore-min.js"></script>
        <script type="text/javascript" src="<?php echo SITE_URL; ?>/theme/assets/js/jstz.min.js"></script>
        <script type="text/javascript" src="<?php echo SITE_URL; ?>/theme/assets/js/calendar.js"></script>
        <script type="text/javascript" src="<?php echo SITE_URL; ?>/theme/assets/js/jquery.nanoscroller.min.js"></script>           
        <script type="text/javascript" src="<?php echo SITE_URL; ?>/theme/assets/js/notifications.js"></script>          
        <?php echo Theme::getToFooter();?>
    </body>
</html>