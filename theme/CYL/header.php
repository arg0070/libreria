<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?php echo SITE_URL; ?>/theme/assets/ico/favicon.ico">
        <title>Librería de referencia cultural</title>

        <!-- Bootstrap -->
        <link href="<?php echo SITE_URL; ?>/theme/assets/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo SITE_URL; ?>/theme/assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo SITE_URL; ?>/theme/assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo SITE_URL; ?>/theme/assets/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="<?php echo SITE_URL; ?>/theme/assets/css/datepicker.css" rel="stylesheet">    
        <link href="<?php echo SITE_URL; ?>/theme/assets/css/calendar.min.css" rel="stylesheet">    

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo SITE_URL; ?>/theme/assets/js/jquery.min.js"></script>
        <script src="<?php echo SITE_URL; ?>/theme/assets/js/modernizr.custom.14123.js"></script>
        <script src="<?php echo SITE_URL; ?>/theme/assets/js/cookies.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->        
    </head>
    <body>
        <div class="alert-min alert-cookies alert-dismissible" id="cookies" role="alert">
            <button onclick="controlcookies()" type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
            <div class="container">
                <strong>Uso de Cookies</strong>: Este sitio utiliza cookies. Si continúa navegando acepta su uso. <a href="#" class="alert-link">Más información</a>
            </div></div>
        <noscript><div class="alert-min alert-danger"><div class="container"><strong>JavaScript desactivado</strong></div></div></noscript>
        <nav class="navbar">
            <div class="container">
                <div class="col-md-6">
                    <a href="inicio"><img class="img-responsive" src="<?php echo SITE_URL; ?>/theme/assets/img/logo1.png" alt="Logo" title="Logo"></a>
                </div>
                <div class="col-md-6">
                    <?php Theme::loadPlugin('loginpanel'); ?>
                </div>
            </div>
        </nav>


        <nav class="navbar navbar-default " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>                    
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="inicio">Inicio</a></li>
                        <li><a href="#">Actualidad</a></li>
                        <li><a href="eventos">Eventos</a></li>
                        <li><a href="registerlib">Libreros</a></li>
                        <li><a href="registeraud">Auditores</a></li>
                    </ul>

                </div><!--/.navbar-collapse -->         
            </div>
        </nav>

