<?php
global $login;
if (!$login->isUserLoggedIn() || $_SESSION['user_type'] != 'LIB') {
    header('Location: inicio');
    exit;
}
try {
    $libreria = new Libreria($_SESSION['user_email']);
    $audits = $libreria->getAudits(true);
    $current_audit = $libreria->getCurrentAudit();
    $lib_doc = $libreria->getDocumentacion();
} catch (Exception $e) {
    ?>
    <section class="container"><div class="row"><div class="col-md-12">
                <div class="alert alert-danger">
                    <?php echo($e->getMessage()); ?>
                    <a href="<?php echo THEME::roleRedirect($_SESSION['user_type']) ?>">Volver atrás.</a>
                </div>
            </div></div></section>
    <?php
    exit;
}

if ($current_audit != null) {
    $id_certificado = $libreria->getCurrentAudit()->getCertificado();
}
?>
<section class="container">    
    <div class="row">
        <header class="b-line">
            <h1>Panel de control</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>
        <div class="col-md-9">
            <div class="box">
                <?php if ($libreria->errors) { ?>
                    <div class="col-md-12">
                        <div class="alert alert-danger">
                            <?php $libreria->showErrors(); ?>
                        </div>
                    </div><div class="clearfix"></div>
                    <?php
                }
                if ($libreria->messages) {
                    ?>
                    <div class="col-md-12">
                        <div class="alert alert-warning">
                            <?php $libreria->showMessages(); ?>
                        </div>
                    </div><div class="clearfix"></div>
                <?php } ?>
            </div>
            <?php if (!$libreria->isApto()) { ?>                                
                <h1>Cuenta pendiente de revisión</h1>                                
                <p>Su cuenta está pendiente de revisión por parte del administrador.</p>
                <h4  class="h-inner">Términos y Requisitos</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <br/>

                <h4  class="h-inner">Aportar documentación</h4>
                <p>Para proceder a revisar su petición, debe proporcionar la siguiente documentación:</p>
                <div class="col-md-6">                        
                    <ul>
                        <li>Documento 1</li>
                        <li>Documento 2</li>
                        <li>Documento 3</li>
                        <li>Documento 4</li>
                    </ul>
                </div>
                <div class="col-md-6 l-line">                                                                                  
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Subir archivos</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input class="fileupload" type="file" name="files[]" multiple data-ref="lib-doc">
                        <input type="hidden" data-ref="lib-doc" value="LIB_DOC" name="resourcetype[]">
                        <input type="hidden" data-ref="lib-doc" value="<?php echo $libreria->getId(); ?>" name="resourceid[]">
                    </span> 
                    <hr>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files"></div>
                    <ul class="list-unstyled">
                        <?php foreach ($lib_doc as $doc) { ?>
                            <li><i class="fa fa-file"></i><a href="<?php echo $doc->getRuta() ?>"> <?php echo $doc->getName(); ?></a><small> <?php echo Util::formatDate($doc->getFecha()); ?></small></li>
                        <?php } ?> 
                    </ul> 
                </div>                             
            <?php } else { ?>    
                <div class="box box-border">
                    <h4><a href="eventos"><i class="fa fa-calendar"></i> Calendario</a></h4>
                </div>
                <div class="col-md-6">
                    <h4>Auditoría</h4>
                    <p>Fecha de su próxima auditoría:
                        <br/><strong><?php echo Util::formatDate($current_audit->getFechaApertura(), $libreria->showAudit($current_audit)); ?></strong></p>

                </div>                                     
                <div class="col-md-6">
                    <h4>Certificado</h4>
                    <?php
                    if ($id_certificado != null) {
                        $certificado = new Certificado($id_certificado);
                        if ($certificado->isValid()) {
                            ?>
                            <div class="alert alert-success" role="alert">Su certificado es válido.<span class="glyphicon glyphicon-ok"></span></div>
                        <?php } else { ?>
                            <div class="alert alert-danger" role="alert">Su certificado ha caducado.<span class="glyphicon glyphicon-remove"></span></div>
                        <?php } ?>
                        <p><strong>Fecha de emision</strong>:<?php echo Util::formatDate($certificado->getFechaEmision()); ?></p>
                        <p><strong>Fecha de caducidad</strong>: <?php echo Util::formatDate($certificado->getFechaCaducidad()); ?></p>
                        <p><?php echo ($certificado->hasBeenSent() ? 'Su certificado fue enviado el ' . Util::formatDate($certificado->getFechaEnvio()) : 'Su certificado no ha sido enviado.') ?></p>
                    <?php } else { ?>
                        <p>No dispone de ningun certificado</p>
                    <?php } ?> 
                </div>

                <div class="clearfix"></div> 
                <hr>
                <h4>Historial de auditorias</h4>
                <form action="auditoria" method="post">
                    <div class="table-responsive">
                        <table class="table table-hover" id="table_lib">
                            <thead>
                                <tr><th>Fecha de realización</th>
                                <th>Hora de inicio</th>
                                <th>Hora de fin</th>
                                <th>Auditor</th>
                                <th>Tipo de auditoría</th>
                                <th>Informe</th></tr>
                            </thead>
                            <tbody>
                            <?php
                            if ($audits != null) {
                                foreach ($audits as $audit) {
                                    $id_auditor = $audit->getAuditor();
                                    if ($id_auditor != null) {
                                        $auditor = new Auditor($id_auditor);
                                    }
                                    ?>
                                    <tr><td><?php echo Util::formatDate($audit->getFechaRealizacion()); ?></td>
                                        <td><?php echo Util::formatTime($audit->getHoraInicio()); ?></td>
                                        <td><?php echo Util::formatTime($audit->getHoraFin()); ?></td>
                                        <td><?php echo ($id_auditor != null ? ($auditor->getNombre() . ' ' . $auditor->getApellidos()) : 'Sin asignar'); ?></td>
                                        <td><?php echo $audit->getTipoAuditoria(); ?></td>
                                        <td><button onclick="submit" class="btn btn-info" name="id" value="<?php echo $audit->getId(); ?>">Informe</button></td></tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </form>                                        

            <?php } ?> 
        </div>    
        <aside class="col-md-3 l-line">
            <?php Theme::loadSidePlugins(); ?>
        </aside>
    </div>
</section>
<script>$(document).ready(function() {
        $('#table_lib').DataTable({paging: true, "pagingType": "simple", "pageLength": 10});
    });</script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?php echo SITE_URL; ?>/theme/assets/js/jquery.ui.widget.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo SITE_URL; ?>/theme/assets/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo SITE_URL; ?>/theme/assets/js/jquery.fileupload.js"></script>
<script src="<?php echo SITE_URL; ?>/theme/assets/js/fileuploadapp.js"></script>