<?php
global $login;
if (!$login->isUserLoggedIn() || $_SESSION['user_type'] != 'AUD') {
    header('Location: inicio');
    exit;
}
try {
    $auditor = new Auditor($_SESSION['user_email']);
    $aud_doc = $auditor->getDocumentacion();
} catch (Exception $e) {
    ?>
    <section class="container"><div class="row"><div class="col-md-12">
                <div class="alert alert-danger">
                    <?php echo($e->getMessage()); ?>
                    <a href="<?php echo THEME::roleRedirect($_SESSION['user_type']) ?>">Volver atrás.</a>
                </div>
            </div></div></section>
    <?php
    exit;
}
$current_audits = $auditor->getAudits(false);
$past_audits = $auditor->getAudits(true);
?>
<section class="container">    
    <div class="row">   
        <header class="b-line">
        <h1>Panel de control</h1>
        <ol class="breadcrumb"><?php Theme::getBreadCrumb();?></ol>
    </header>
        <div class="col-md-9">
            <div class="box box-border"><a href="#" class="btn btn-info">Mostrar manual de uso</a></div>
            <div class="box box-border"><h4><a href="eventos"><i class="fa fa-calendar"></i> Calendario</a></h4></div>
                          
            <?php
            if ($auditor->isHomologado()) {
                ?>
                
                    <h4 class="h-inner">Auditorias pendientes</h4>
                    <?php if ($current_audits == null) { ?>
                        <div class="alert alert-warning">Ninguna auditoria pendiente.</div>
                    <?php } else { ?>
                        <form action="auditoria" method="post">
                            <div class="table-responsive">
                                <table class="table table-hover" id='tableact'>
                                    <thead>
                                        <tr><th>Estado</th>
                                            <th>Librería</th>
                                            <th>Tipo</th>
                                            <th>Fecha de apertura</th>
                                            <th>Fecha de cierre</th>                                                                                                                                    
                                            <th>Informe</th></tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($current_audits as $audit) {
                                            $libreria = new Libreria($audit->getLibreria());
                                            ?>
                                            <tr>
                                                <td><?php echo $audit->getEstado(); ?></td>
                                                <td><?php echo $libreria->getNombreComercial(); ?></td>
                                                <td><?php echo $audit->getTipoAuditoria(); ?></td>
                                                <td><?php echo Util::formatDate($audit->getFechaApertura()); ?></td>
                                                <td><?php echo Util::formatDate($audit->getFechaCierre()); ?></td>                                                                                                                                                                                                
                                                <td><button onclick="submit" class="btn btn-info" name="id" value="<?php echo $audit->getId(); ?>">Informe</button></td></tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody></table>
                            </div>
                        </form>
                    <?php } ?>
                    <hr>

                
                    <h4  class="h-inner">Histórial de auditorias</h4>
                    <?php if ($past_audits == null) { ?>
                        <div class="alert alert-warning">Ninguna auditoria realizada.</div>
                    <?php } else { ?>
                        <form action="auditoria" method="post">
                            <div class="table-responsive">
                                <table class="table table-hover" id="tablehis">
                                    <thead>
                                        <tr><th>Estado</th>
                                            <th>Librería</th>
                                            <th>Tipo</th>
                                            <th>Fecha de apertura</th>
                                            <th>Fecha de cierre</th>                                                                                                                                    
                                            <th>Informe</th></tr>
                                    <thead>
                                    <tbody>
                                        <?php
                                        foreach ($past_audits as $audit) {
                                            $libreria = new Libreria($audit->getLibreria());
                                            ?>
                                            <tr><td><?php echo $audit->getEstado(); ?></td>
                                                <td><?php echo $libreria->getNombreComercial(); ?></td>
                                                <td><?php echo $audit->getTipoAuditoria(); ?></td>
                                                <td><?php echo Util::formatDate($audit->getFechaApertura()); ?></td>
                                                <td><?php echo Util::formatDate($audit->getFechaCierre()); ?></td>                                                                                                                                                                                                
                                                <td><button onclick="submit" class="btn btn-info" name="id" value="<?php echo $audit->getId(); ?>">Informe</button></td></tr>
                                            <?php }
                                            ?>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    <?php } ?>

<?php } else { ?>
                <h1>Cuenta pendiente de revisión</h1>                                
                <p>Su cuenta está pendiente de revisión por parte del administrador.</p>
                <h4  class="h-inner">Términos y Requisitos</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <br/>

                <h4  class="h-inner">Aportar documentación</h4>
                <p>Para proceder a revisar su petición, debe proporcionar la siguiente documentación:</p>
                <div class="col-md-6">                        
                    <ul>
                        <li>Documento 1</li>
                        <li>Documento 2</li>
                        <li>Documento 3</li>
                        <li>Documento 4</li>
                    </ul>
                </div>
                <div class="col-md-6 l-line">                                                                                  
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Subir archivos</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input class="fileupload" type="file" name="files[]" multiple data-ref="aud-doc">
                        <input type="hidden" data-ref="aud-doc" value="AUD_DOC" name="resourcetype[]">
                        <input type="hidden" data-ref="aud-doc" value="<?php echo $auditor->getId(); ?>" name="resourceid[]">
                    </span> 
                    <hr>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files"></div>
                    <ul class="list-unstyled">
                        <?php foreach ($aud_doc as $doc) { ?>
                            <li><i class="fa fa-file"></i><a href="<?php echo $doc->getRuta() ?>"> <?php echo $doc->getName(); ?></a><small> <?php echo Util::formatDate($doc->getFecha()); ?></small></li>
                        <?php } ?> 
                    </ul> 
                </div>
            <?php } ?>
        </div>                        
        <aside class="col-md-3 l-line">
<?php Theme::loadSidePlugins(); ?>
        </aside>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('#tableact').DataTable();
        $('#tablehis').DataTable();
    });
</script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?php echo SITE_URL; ?>/theme/assets/js/jquery.ui.widget.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo SITE_URL; ?>/theme/assets/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo SITE_URL; ?>/theme/assets/js/jquery.fileupload.js"></script>
<script src="<?php echo SITE_URL; ?>/theme/assets/js/fileuploadapp.js"></script>

