<div class="news plugin">
         <h2>Últimas Noticias</h2>         
        <ul class="media-list">        
            <li class="media">
                <a class="pull-left" href="#">
                    
                </a>
                <div class="media-body">
                    <h4 class="media-heading">Título de la noticia</h4>
                    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                </div>
            </li>

            <li class="media">
                <a class="pull-left" href="#"> <!-- http://placehold.it/50x50 -->
                    
                </a>
                <div class="media-body">
                    <h4 class="media-heading">Título de la noticia</h4>
                    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                </div>
            </li>
        </ul>
        <p><a href="#"><i class="fa fa-chevron-right"></i> Todas las noticias</a></p>
    </div>