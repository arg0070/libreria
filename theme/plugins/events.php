<?php 
$evento = new Evento();
$eventos = $evento->getEventsPanel();?>
<div class="events plugin">        
    <h2>Próximos eventos</h2>    

    <?php if($eventos == null) { ?>
    <p>Ningún evento disponible</p>
    <?php } else { 
        foreach($eventos as $evt) { ?>
  
    <article class="event-item row">
        <div class="col-md-2 col-sm-2 date-label">           
            <span class="month"><?php echo strftime("%b", strtotime($evt->FECHA_INICIO)); ?></span>
            <span class="date-number"><?php echo strftime("%d", strtotime($evt->FECHA_INICIO)); ?></span>                                    
        </div>
        <div class="col-md-10 col-sm-10 event-content">
            <h3 class="title"><?php echo $evt->TITULO; ?></h3>
            <p class="time"><i class="fa fa-clock-o"></i>
                <?php 
                $hora_inicio = strftime("%H:%S", strtotime($evt->FECHA_INICIO));
                $hora_fin = strftime("%H:%S", strtotime($evt->FECHA_FIN));                
                echo (($hora_inicio == '00:00' && $hora_fin == '00:00') ? 'Todo el día' : $hora_inicio .'-'.$hora_fin); ?>                                                                 
            </p>
            <p class="location"><a href="<?php echo $evt->URL; ?>"><i class="fa fa-navicon"></i>Info</a></p>                                         
        </div>
    </article>
    
        <?php }} ?>
    

    <p><a href="eventos"><i class="fa fa-chevron-right"></i> Todos los eventos</a></p>   
</div>