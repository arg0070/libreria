<?php
$admin = false;
$audit = false;
$user = false;
$informe = false;
list($uri,) = Theme::getUri();
switch ($uri) {
    case(preg_match('/^(auditoria|admin-audit[a-zA-Z]*)/', $uri) ? true : false) :
        $audit = true;
        break;
    case(preg_match('/admin-user[a-zA-Z]+/', $uri) ? true : false) :
        $user = true;
        break;
    case(preg_match('/admin-informes/', $uri) ? true : false) :
        $informe = true;
        break;
    case(preg_match('/admin/', $uri) ? true : false) :
        $admin = true;
        break;
}
?>

<div class="affix-sidebar">
    <div class="sidebar-nav">
        <div class="navbar navbar-inverse" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>                    
            </div>
            <div class="navbar-collapse collapse sidebar-navbar-collapse">
                <ul class="nav navbar-nav" id="sidenav01">
                    <li <?php echo ($admin ? 'class="active"' : ''); ?>>
                        <a href="admin"><i class="fa fa-home fa-3x"></i> <p>Administración</p></a>
                    </li>
                    <li <?php echo ($audit ? 'class="active"' : ''); ?>>
                        <a href="#" data-toggle="collapse" data-target="#auditMenu" data-parent="#sidenav01" class="collapsed">
                            <i class="fa fa-file fa-3x"></i> <p>Auditorías <i class="fa fa-caret-down"></i></p>
                        </a>
                        <div class="collapse" id="auditMenu" style="height: 0px;">
                            <ul class="nav nav-list">
                                <li><a href="admin-audit">Todas las Auditorías</a></li>
                                <li><a href="admin-auditPre">Pendientes de asignación</a></li>
                                <li><a href="admin-auditPos">Pendientes de revisión</a></li>
                            </ul>
                        </div>
                    </li>
                    <li <?php echo ($user ? 'class="active"' : ''); ?>>
                        <a href="#" data-toggle="collapse" data-target="#toggleDemo2" data-parent="#sidenav01" class="collapsed">
                            <i class="fa fa-users fa-3x"></i> <p>Usuarios <i class="fa fa-caret-down"></i></p>
                        </a>
                        <div class="collapse" id="toggleDemo2" style="height: 0px;">
                            <ul class="nav nav-list">
                                <li><a href="admin-userLib">Librerías</a></li>
                                <li><a href="admin-userAud">Auditores</a></li>
                            </ul>
                        </div>
                    </li>
                    <li <?php echo ($informe ? 'class="active"' : ''); ?>><a href="admin-informes"><i class="fa fa-pencil-square-o fa-3x"></i> <p>Informes</p></a></li>
                    <li><a href="#"><i class="fa fa-calendar fa-3x"></i> <p>WithBadges <span class="badge pull-right">42</span></p></a></li>
                    <li><a href=""><i class="fa fa-cog fa-3x"></i> <p>Opciones</p></a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div>