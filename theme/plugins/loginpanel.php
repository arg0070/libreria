<?php
global $login;
if (!$login->isUserLoggedIn()) {
    ?>
    <form class="navbar-form" method="post" action="inicio">
        <div class="form-group">
            <input type="email" name="user_mail" placeholder="Email" class="form-control" required>            
        </div>
        <div class="form-group botton-in">
            <input type="password" name="user_password" placeholder="Contraseña" class="form-control" required autocomplete="off">
            <div class="input-link">
                <a class="input-button" href="ayudacuenta" title="Recordar contraseña"></a>
            </div>
        </div>        
        <input type="submit" class="btn btn-primary" name="login" value="Entrar">
    </form>
    <?php if ($login->errors) { ?>
        <div class="col-md-10">
            <div class="alert alert-danger">                
                <?php $login->showErrors(); ?>
            </div>
        </div>
    <?php }
    if ($login->messages) {
        ?>
        <div class="col-md-10">
            <div class="alert alert-warning">                
        <?php $login->showMessages(); ?>
            </div>
        </div>
    <?php
    }
} else {
    ?>

    <p class="navbar-text">Bienvenido <strong><a href="<?php echo Theme::roleRedirect($_SESSION['user_type']); ?>" title="Panel"><?php echo $_SESSION['user_email'] ?></a></strong></p>
    <ul class="list-inline">
        <li><div class="dropdown">                                       
                <a class="btn btn-default" href="#" title="Notificaciones" id="dropdownMenu1" data-toggle="dropdown">
                    <i class="fa fa-bell"></i>
                  
                    <span class="label label-primary" id="alertcount"></span></a>
               
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li>
                        <div class="nano">
                            <div class="nano-content" id="notification-panel">
                                
                                
                            </div>
                        </div> 
                    </li>                                                                                          
                    <li role="presentation" class="text-center"><strong><a id="notification-button" tabindex="-1" href="#"></a></strong></li>
                </ul>

            </div>
        </li>
        <li><a class="btn btn-default" href="opciones" title="Opciones"><i class="fa fa-cog"></i></span> Opciones</a></li>
        <li><a class="btn btn-default" href="?logout" title="Salir"><i class="fa fa-power-off"></i></span> Salir</a></li>
    </ul>
    <?php
    THEME::setToFooter("<script>$('#notification-panel').notification();$('.nano').nanoScroller();</script>");
} 