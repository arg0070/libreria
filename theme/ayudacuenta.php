<?php
global $login;
if ($login->isUserLoggedIn()) {
    header('Location: inicio');
    exit;
}
?>
<section class="container">    
    <div class="row">   
        <header class="b-line">
            <h1>Solicitar nueva contraseña</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>        
        <div class="col-md-9">                        

<?php if (isset($_SESSION['password_reset_valid']) && $_SESSION['password_reset_valid'] == true) { ?>
                <div class="box">
                    <p>Introduzca la nueva contraseña para su cuenta</p>
                    <form method="post" action="ayudacuenta" name="new_password_form">
                        <input type='hidden' name='user_email' value='<?php echo $_SESSION['password_reset_email']; ?>' />
                        <input type='hidden' name='user_password_reset_hash' value='<?php echo $_SESSION['verification_code']; ?>' />
                        <div class="form-group">
                            <label for="user_password_new">Nueva contraseña</label>
                            <input id="user_password_new" type="password" name="user_password_new" class="form-control" pattern=".{6,}" required autocomplete="off" />
                        </div>
                        <div class="form-group">
                            <label for="user_password_repeat">Repetir contraseña nueva</label>
                            <input id="user_password_repeat" type="password" name="user_password_repeat" class="form-control"  pattern=".{6,}" required autocomplete="off" />
                        </div>
                        <input type="submit" class="btn btn-primary" name="submit_new_password" value="Guardar" />
                    </form>
                </div>
                <!-- no data from a password-reset-mail has been provided, so we simply show the request-a-password-reset form -->
<?php } else { ?>
                <div class="box">
                    <p>Para recuperar su contraseña introduzca su dirección de correo electrónico. Recibirá un email con las instrucciones a seguir.</p>
                    <form method="post" action="ayudacuenta" name="password_reset_form">
                        <div class="form-group">
                            <label for="user_name">Dirección de correo electrónico</label>
                            <input id="user_name" class="form-control" type="email" name="user_email" placeholder="Dirección de correo electrónico" required />
                        </div>
                        <input type="submit" name="request_password_reset" class="btn btn-primary" value="Solicitar nueva contraseña" />
                    </form>
                </div>
<?php } ?>
        </div>
        <aside class="col-md-3 l-line">
<?php Theme::loadSidePlugins(); ?>
        </aside>
    </div>
</section>