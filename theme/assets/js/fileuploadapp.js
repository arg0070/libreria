/*jslint unparam: true */
/*global window, $ */
$(function() {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '/libreria/app/modules/UploadHandler.php';
    $('.fileupload').each(function() {
        $(this).fileupload({
            url: url,
            dataType: 'json',
            done: function(e, data) {
                $.each(data.result.files, function(index, file) {
                    $('<a href="' + file.url + '">').text(file.name).appendTo('#files');
                });
            },
            progressall: function(e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                        );
            }
        }).on('fileuploadsubmit', function(e, data) {
            var ref = $(e.target).data('ref');
            data.formData = $('input:hidden[data-ref="' + ref + '"]').serializeArray();
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
});