/**
 * Notifications
 */
"use strict";

(function($) {

    var defaults = {
        notification_source: '/libreria/app/modules/Alerta.php',
        css_class: 'notification',
        date_start: '',
        date_end: ''
    };

    var strings = {
        clear: 'Limpiar todo',
        error_source: 'No se ha especificado el archivo fuente o este es inválido'
    };


    function warn(message) {
        if ($.type(window.console) === 'object' && $.type(window.console.warn) === 'function') {
            window.console.warn('[Notifications] ' + message);
        }
    }

    function Notification(params, context) {
        this.options = $.extend(defaults, params);
        this.strings = strings;
        this.context = context;


        this.init();
        return this;
    }

    Notification.prototype.init = function() {
        this._loadNotifications();
    };



    Notification.prototype._loadNotifications = function() {
        var source = null;
        if ('notification_source' in this.options && this.options.notification_source !== '') {
            source = this.options.notification_source;
        }
        var loader;

        if (source.length) {
            loader = function() {
                var notifications = [];

                $.ajax({
                    url: source,
                    dataType: 'json',
                    type: 'POST',
                    data: {action: "_loadNotifications"},
                    async: false
                }).done(function(json) {
                    if (!json.success) {
                        warn(json.error);
                    }
                    if (json.result) {
                        notifications = json.result;
                    }
                });

                return notifications;
            };
        }


        if (!loader) {
            warn(this.strings.error_source);
        }

        this.options.notifications = loader();
        this._printNotifications(this.options.notifications);

    };

    Notification.prototype._printNotifications = function(notifications) {
        var self = this;
        var source = this.options.source;
        var panel = $('#notification-panel');
        var list = $('<ul class="list-unstyled">');
        var count = notifications.length;
        $('#alertcount').html(count);

        var clear_all = $('#notification-button');
        clear_all.html(self.strings.clear);
        clear_all.click(function(event) {
            event.preventDefault();
            event.stopPropagation();            
            $.ajax({
                url: source,
                type: 'POST',
                data: {action: "_delAllNotifications"}
            }).done(function(data) {                
                location.reload(true);
            });
        });

        $.each(notifications, function(value, n) {
            list.append($('<li role="presentation" class="' + self.options.css_class + '">' +
                    '<button class="close btndel" id="btndel-' + n.id + '"><i class="fa fa-times"></i></button>' +
                    '<a role="menuitem" tabindex="-1" href="' + n.enlace + '">' +
                    '<small class="h-inner">' + n.fecha + '</small><br>' +
                    '<strong>' + n.descripcion + '</strong><br>' +
                    '<span class="small">' + n.mensaje + '</span></a>' +
                    '</li>' +
                    '<li role="presentation" class="divider"></li>'));

            var button = list.find('#btndel-' + n.id);
            button.click(function(event) {
                event.preventDefault();
                event.stopPropagation();
                $.ajax({
                    url: source,
                    type: 'POST',
                    data: {action: "_delNotifications", n_id: n.id, n_destinatario: n.destinatario}
                }).done(function(data) {
                    location.reload(true);
                });
            });
        });

        panel.append(list);
    };



    $.fn.notification = function(params) {
        return new Notification(params, this);
    };
}(jQuery));
