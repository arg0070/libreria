document.getElementById("edit_audit").addEventListener("click", editAudit, false);
document.getElementById("edit_evd").addEventListener("click", editEvd, false);
document.getElementById("edit_nocon").addEventListener("click", editNocon, false);

function editAudit() {
    $('#edit_audit').attr('disabled', true);
    $('#alcance_audit').attr('disabled', false);
    $('#fecha_realizacion_audit').attr('disabled', false);
    $('#fecha_cierre_audit').attr('disabled', false);
    $('#inicio_audit').attr('disabled', false);
    $('#fin_audit').attr('disabled', false);
    $('#conforme_audit').attr('disabled', false);
   
    $submit = document.createElement('input');
    setAttributes($submit, {"type": "submit", "value": "Guardar", "name": "editaudit", "class": "btn btn-info"});
    $('#btn-send').append($submit);
}

function editNocon() {
    document.getElementById("edit_nocon").disabled = true;
    var tbody = document.getElementById('table_nocon').getElementsByTagName("TBODY")[0];
    var tdbody = tbody.getElementsByTagName('td');
    for (i = 0; i < tdbody.length; i++) {
        tdbody[i].childNodes[0].disabled = false;
    }
    
    $addrow = document.createElement('botton');
    $addrow.innerHTML = "Añadir registro";
    setAttributes($addrow, {"class": "btn btn-warning", "onclick": "addNoconRow('table_nocon')"});
    document.getElementById("btn-nocon").appendChild($addrow);
    
    $submit = document.createElement('input');
    setAttributes($submit, {"type": "submit", "value": "Guardar", "name": "editnocon", "class": "btn btn-info"});
    document.getElementById('btn-nocon').appendChild($submit);
}

function editEvd(){
    document.getElementById("edit_evd").disabled = true;
    var tbody = document.getElementById('table_evd').getElementsByTagName("TBODY")[0];
    var tdbody = tbody.getElementsByTagName('td');
    for (i = 0; i < tdbody.length; i++) {
        tdbody[i].childNodes[0].disabled = false;
    }
    
    $addrow = document.createElement('botton');
    $addrow.innerHTML = "Añadir registro";
    setAttributes($addrow, {"class": "btn btn-warning", "onclick": "addEvdRow('table_evd')"});
    document.getElementById("btn-evd").appendChild($addrow);
    
    $submit = document.createElement('input');
    setAttributes($submit, {"type": "submit", "value": "Guardar", "name": "editevd", "class": "btn btn-info"});
    document.getElementById('btn-evd').appendChild($submit);
}

function addNoconRow(id) {

    var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");
    var td1 = document.createElement("TD");
    var td2 = document.createElement("TD");
    var td3 = document.createElement("TD");
    var td4 = document.createElement("TD");
    var td5 = document.createElement("TD");
    var td6 = document.createElement("TD");

    //first col: Counter
    var count = countRows(id);
    td1.appendChild(document.createTextNode(count));

    //second col: Type
    var select = document.createElement('select');
    setAttributes(select, {"name": "tipo_nocon_" + count});
    var array = ["GRAVE", "LEVE", "CERRADA"];
    for (var i = 0; i < array.length; i++) {
        var option = document.createElement("option");
        option.value = array[i];
        option.text = array[i];
        select.appendChild(option);
    }
    td2.appendChild(select);

    //third col: Description
    var desc = document.createElement("input");
    setAttributes(desc, {"name": "desc_nocon_" + count, "type": "text"});
    td3.appendChild(desc);

    //founth col: Detection date
    var detect = document.createElement("input");
    setAttributes(detect, {"name": "detect_nocon_" + count, "type": "date"});
    td4.appendChild(detect);

    //fiveth col: Detection date
    var resol = document.createElement("input");
    setAttributes(resol, {"name": "resol_nocon_" + count, "type": "date"});
    td5.appendChild(resol);

    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);
    row.appendChild(td5);
    row.appendChild(td6);
    tbody.appendChild(row);
}

function addEvdRow(id) {

    var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");
    var td1 = document.createElement("TD");
    var td2 = document.createElement("TD");
    var td3 = document.createElement("TD");   

    //first col: Counter
    var count = countRows(id);
    td1.appendChild(document.createTextNode(count));
    
    //second col: Name
    var name = document.createElement("input");
    setAttributes(name, {"name": "name_evd_" + count, "type": "text"});
    td2.appendChild(name);
    
     //third col: Description
    var desc = document.createElement("input");
    setAttributes(desc, {"name": "desc_evd_" + count, "type": "text"});
    td3.appendChild(desc);

    

    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);    
    tbody.appendChild(row);
}

function countRows(id) {
    return document.getElementById(id).getElementsByTagName('tr').length;
}

function setAttributes(el, attrs) {
    for (var key in attrs) {
        el.setAttribute(key, attrs[key]);
    }
}
