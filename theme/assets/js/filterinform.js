
$(document).ready(function() {
    
    table = $('#tablelib').DataTable();
    $('#tableaud').DataTable();
    $('ul[role="tablist"] li a').click(function(event) {
        table = $($(event.target).data('table')).DataTable();
    });
    $('input:checkbox').change(function(event) {
        var target = event.target;
        var value = $(target).val();
        var status = $(target).prop('checked');
        table.column(value).visible(status);
        table.columns.adjust().draw(false); // adjust column sizing and redraw
    });
    $('.advfilter input:not([class*="date"])').on('keyup change', function() {
        var colid = $(this).data('colid');
        if (typeof colid !== typeof undefined && colid !== false) {
            table.column(colid).search(this.value).draw();
        }
    });
    $('.advfilter select').change(function() {
        var colid = $(this).data('colid');
        if (typeof colid !== typeof undefined && colid !== false) {
            table.column(colid).search(this.value).draw();
        }
    });

    $('input.date').datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        weekStart: 1
    }).on('keyup change', function(event) {
        table.draw();
    });

    /* Filtro personalizado para rango de fechas.
     * return true si la fila debe ser mostrada(campos de búsqueda vacíos, solo
     * uno de los campos rellenado, ambos campos rellenados).
     * El método se ejecuta cada vez que se dibuja (draw) la tabla.*/
    $.fn.dataTable.ext.search.push(
            function(settings, data, dataIndex) {
                var min = getDate($('#audadv_from').val());
                var max = getDate($('#audadv_to').val());
                var date = getDate(data[5]); // use data for column

                if ((min === '' && max === '') ||
                        (min === '' && date <= max) ||
                        (max === '' && date >= min) ||
                        (date >= min && date <= max)) {
                    return true;
                }
                return false;
            });

    $.fn.dataTable.ext.search.push(
            function(settings, data, dataIndex) {
                var min = getDate($('#auditopen_from').val());
                var max = getDate($('#auditopen_to').val());
                var date = getDate(data[4]); // use data for column

                if ((min === '' && max === '') ||
                        (min === '' && date <= max) ||
                        (max === '' && date >= min) ||
                        (date >= min && date <= max)) {
                    return true;
                }
                return false;
            });

    $.fn.dataTable.ext.search.push(
            function(settings, data, dataIndex) {
                var min = getDate($('#auditclose_from').val());
                var max = getDate($('#auditclose_to').val());
                var date = getDate(data[5]); // use data for column

                if ((min === '' && max === '') ||
                        (min === '' && date <= max) ||
                        (max === '' && date >= min) ||
                        (date >= min && date <= max)) {
                    return true;
                }
                return false;
            });

    function getDate(date) {
        var rule = new RegExp('(0[1-9]|[1-2][0-9]|3[0-1])/(0[1-9]|1[0-2])/[0-9]{4}');
        if (date.match(rule)) {
            date = date.split('/');
            return new Date(date[2], date[1] - 1, date[0]);
        }
        return '';
    }

    $('button.export').click(function(event) {
        // Get filtered data to export
        var header = [];
        $(table.columns(':visible').header()).each(function(index, value) {
            header.push($(value).html());
        });

        var content = jQuery.makeArray(table.columns(':visible', {search: 'applied'}).data());

        if (typeof header[0] !== typeof undefined && typeof content[0][0] !== typeof undefined) {
            $.ajax({
                url: '/libreria/app/modules/Informe.php',
                dataType: 'json',
                type: 'POST',
                data: {action: "_exportTable", content: content, header: header},
                async: false
            }).done(function(data) {
                window.open(data, '_blank');
            });
        }
    });

});