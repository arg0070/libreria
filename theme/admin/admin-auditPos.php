<?php
if ($_SESSION['user_type'] != 'ADM') {
    header('Location: inicio');
    exit;
}
require ROOT . DS . 'app' . DS . 'modules' . DS . 'ManagerADM.php';
$manager = new ManagerADM();
$auditorias = $manager->getFinishedAudits();
?>

<section class="container">
    <div class="row">
        <header class="b-line">
            <h1>Auditorías pendientes de revisión</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>
        <div class="col-sm-3 col-md-2">
            <?php Theme::loadAdminSideMenu(); ?>
        </div>
        <div class="col-md-9">               
            <?php if ($auditorias == null) { ?>
                <h4>Ninguna auditoría pendiente</h4>
                <?php
            } else {
                foreach ($auditorias as $auditoria) {
                    $libreria = new Libreria($auditoria->getLibreria());
                    if(($id_auditor = $auditoria->getAuditor()) != null){
                    $auditor = new Auditor($id_auditor);}
                    ?>                    
                    <div class="box-border box">                       
                        <h3><strong>Librería:</strong> <?php echo $libreria->getNombreComercial(); ?></h3>
                        <h3><strong>Auditor:</strong> <?php echo ($id_auditor != null ? $auditor->getNombre().' '.$auditor->getApellidos() : 'Sin asignar'); ?></h3>
                        <p><strong>Tipo de auditoría:</strong> <?php echo $auditoria->getTipoAuditoria(); ?></p>
                        <p><strong>Fecha de apertura:</strong> <?php echo Util::formatDate($auditoria->getFechaApertura()); ?></p>
                        <p><strong>Fecha de realización:</strong> <?php echo Util::formatDate($auditoria->getFechaRealizacion()); ?></p>
                        <p><strong>Fecha de cierre:</strong> <?php echo Util::formatDate($auditoria->getFechaCierre()); ?></p>
                        <form method="post" action="auditoria">
                            <button onclick="submit" class="btn btn-info" name="id" value="<?php echo $auditoria->getId(); ?>">Ver informe</button>
                        </form>
                    </div>                                                       
                <?php }
                ?>                               
            </div>
        <?php }
        ?>
    </div></div></section>