<?php
if ($_SESSION['user_type'] != 'ADM') {
    header('Location: inicio');
    exit;
}
require ROOT . DS . 'app' . DS . 'modules' . DS . 'ManagerADM.php';
$manager = new ManagerADM();
$librerias = $manager->manageLibs();
$auditores = $manager->manageAuditors();
$auditorias = $manager->manageAudits();
?>

<section class="container">
    <div class="row">
        <header class="b-line">
            <h1>Informes</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>
        <div class="col-sm-3 col-md-2">
            <?php Theme::loadAdminSideMenu(); ?>
        </div>
        <div class="col-md-9 col-sm-9">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#lib" role="tab" data-toggle="tab" data-table="#tablelib">Librerías</a></li>
                <li><a href="#aud" role="tab" data-toggle="tab" data-table="#tableaud">Auditores</a></li>
                <li><a href="#audit" role="tab" data-toggle="tab" data-table="#tableaudit">Auditorías</a></li>
            </ul>
        </div>

        <div class="tab-content">

            <div class="tab-pane active col-md-9" id="lib">
                <h2>Librerías</h2>            
                <div class="col-md-4 col-sm-4 panel panel-primary">                    
                    <h4 class="h-inner">Campos</h4>
                    <div class="checkbox"><label><input type="checkbox" value="0" checked> Razón social</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="1" checked> CIF</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="2" checked> Nombre comercial</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="3" checked> Nombre del titular</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="4" checked> Email</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="5" checked> Dirección</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="6" checked> Código postal</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="7" checked> Población</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="8" checked> Provincia</label></div> 
                    <div class="checkbox"><label><input type="checkbox" value="9" checked> Apto</label></div> 
                </div>
                <div class="col-md-8 col-sm-8 advfilter">
                    <h4 class="h-inner">Avanzado</h4>
                    <div class="form-group">
                        <label class="control-label" for="libadv_cp">Código postal</label>
                        <input type="text" class="form-control" data-colid="6" name="libadv_cp" placeholder="Código postal">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="libadv_poblacion">Población</label>
                        <input type="text" class="form-control" data-colid="7" name="libadv_poblacion" placeholder="Población">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="libadv_provincia">Provincia</label>
                        <input type="text" class="form-control" data-colid="8" id="libadv_provincia" placeholder="Provincia">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="libadv_isApt">Apto</label>
                        <select class="form-control" data-colid="9" id="libadv_isApt">
                            <option selected disabled>Elija una opción</option>
                            <option value="Si">Si</option>
                            <option value="No">No</option>
                            <option value="">Todos</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div> 

                <button class="btn btn-primary export"><i class="fa fa-2x fa-file-word-o"></i> Exportar a Word</button>

                <hr>
                <div class="table-responsive">
                    <table class="table table-hover" id="tablelib">
                        <thead>
                            <tr>
                                <th>Razón social</th>
                                <th>CIF</th>
                                <th>Nombre comercial</th>
                                <th>Titular</th>
                                <th>Email</th>
                                <th>Dirección</th>
                                <th>Código postal</th>
                                <th>Población</th>
                                <th>Provincia</th> 
                                <th>Apto</th> 
                            </tr>
                        </thead>
                        <tbody data-link="row" class="rowlink">
                            <?php
                            foreach ($librerias as $libreria) {
                                ?>
                                <tr>
                                    <td><a href="admin-userLibEdit/<?php echo $libreria->getId(); ?>"><?php echo $libreria->getRaZonSocial(); ?></a></td>
                                    <td><?php echo $libreria->getCIF(); ?></td>
                                    <td><?php echo $libreria->getNombreComercial(); ?></td>
                                    <td><?php echo $libreria->getNombreTitular(); ?></td>
                                    <td><?php echo $libreria->getEmail(); ?></td>
                                    <td><?php echo $libreria->getDireccion(); ?></td>
                                    <td><?php echo $libreria->getCP() ?></td>
                                    <td><?php echo $libreria->getPoblacion(); ?></td>
                                    <td><?php echo $libreria->getProvincia(); ?></td> 
                                    <td><?php echo ($libreria->isApto() ? 'Si': 'No'); ?></td>
                                </tr>
                            <?php }
                            ?>                            
                        </tbody>
                    </table> 
                </div>
            </div>

            <div class="tab-pane col-md-9" id="aud">

                <h2>Auditores</h2>            
                <div class="col-md-4 col-sm-4 panel panel-primary">                    
                    <h4 class="h-inner">Campos</h4>
                    <div class="checkbox"><label><input type="checkbox" value="0" checked> Nombre</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="1" checked> Apellidos</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="2" checked> Email</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="3" checked> Código de solicitud</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="4" checked> Homologado</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="5" checked> Fecha de Homologación</label></div>                                     
                </div>
                <div class="col-md-8 col-sm-8 advfilter">
                    <h4 class="h-inner">Avanzado</h4>
                    <div class="form-group">
                        <label class="control-label" for="aud_isHo">Homologado</label>
                        <select class="form-control" data-colid="4" id="aud_isHo">
                            <option selected disabled>Elija una opción</option>
                            <option value="Si">Si</option>
                            <option value="No">No</option>
                            <option value="">Todos</option>
                        </select>
                    </div>
                    <div class="form-inline">
                        <p><strong>Fecha de Homologación</strong></p>
                        <div class="input-group"><input id="audadv_from" type="text" class="form-control date" data-colid="5" placeholder="Desde: dd/mm/aaaa"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>
                        <div class="input-group"><input id="audadv_to" type="text" class="form-control date" data-colid="5" placeholder="Hasta: dd/mm/aaaa"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>

                    </div>                    
                </div>
                <div class="clearfix"></div>            
                <button class="btn btn-primary export"><i class="fa fa-2x fa-file-word-o"></i> Exportar a Word</button>
                <hr>
                <div class="table-responsive">
                    <table class="table table-hover" id="tableaud">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>Email</th>
                                <th>Código de solicitud</th>
                                <th>Homologado</th>                               
                                <th>Fecha de homologación</th>                                                                
                            </tr>
                        </thead>
                        <tbody data-link="row" class="rowlink">
                            <?php
                            foreach ($auditores as $auditor) {
                                ?>
                                <tr>
                                    <td><a href="admin-userAudEdit/<?php echo $auditor->getId(); ?>"><?php echo $auditor->getNombre(); ?></a></td>
                                    <td><?php echo $auditor->getApellidos(); ?></td>
                                    <td><?php echo $auditor->getEmail(); ?></td>
                                    <td><?php echo $auditor->getCodigoSolicitud(); ?></td>
                                    <td><?php echo ($auditor->isHomologado() ? 'Si' : 'No'); ?></td>
                                    <td><?php echo Util::formatDateLocal($auditor->getFechaHomologacion()); ?></td>                                    
                                </tr>
                            <?php }
                            ?>                            
                        </tbody>
                    </table> 
                </div>

            </div>
            <div class="tab-pane col-md-9" id="audit">



                <h2>Auditorías</h2>            
                <div class="col-md-4 col-sm-4 panel panel-primary">                    
                    <h4 class="h-inner">Campos</h4>
                    <div class="checkbox"><label><input type="checkbox" value="0" checked> Estado</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="1" checked> Librería</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="2" checked> Auditor</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="3" checked> Tipo</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="4" checked> Fecha apertura</label></div>
                    <div class="checkbox"><label><input type="checkbox" value="5" checked> Fecha cierre</label></div>                                     
                </div>
                <div class="col-md-8 col-sm-8 advfilter">
                    <h4 class="h-inner">Avanzado</h4>
                    <div class="form-group">
                        <label class="control-label" for="audit_status">Estado</label>
                        <select class="form-control" data-colid="0" id="audit_status">
                            <option selected disabled>Elija una opción</option>
                            <option value="Pendiente">Pendiente</option>
                            <option value="Cerrada">Cerrada</option>
                            <option value="Fuera de plazo">Fuera de plazo</option>
                            <option value="Pendiente de revisión">Pendiente de revisión</option>
                            <option value="">Todos</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="audit_type">Tipo de auditoría</label>
                        <select class="form-control" data-colid="3" id="audit_type">
                            <option selected disabled>Elija una opción</option>
                            <option value="CERTIFICACION">Certificación</option>
                            <option value="MANTENIMIENTO">Mantenimiento</option>
                            <option value="REVISION">Revisión</option>                            
                            <option value="">Todos</option>
                        </select>
                    </div>
                    <div class="form-inline">
                        <p><strong>Fecha de Apertura</strong></p>
                        <div class="input-group"><input id="auditopen_from" type="text" class="form-control date" placeholder="Desde: dd/mm/aaaa"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>
                        <div class="input-group"><input id="auditopen_to" type="text" class="form-control date" placeholder="Hasta: dd/mm/aaaa"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>

                    </div>
                    <div class="form-inline">
                        <p><strong>Fecha de Cierre</strong></p>
                        <div class="input-group"><input id="auditclose_from" type="text" class="form-control date" placeholder="Desde: dd/mm/aaaa"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>
                        <div class="input-group"><input id="auditclose_to" type="text" class="form-control date" placeholder="Hasta: dd/mm/aaaa"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>

                    </div>
                </div>
                <div class="clearfix"></div>                   
                <button class="btn btn-primary export"><i class="fa fa-2x fa-file-word-o"></i> Exportar a Word</button>
                <hr>

                <div class="table-responsive">
                    <table class="table table-hover" id="tableaudit">
                        <thead>
                            <tr>
                                <th>Estado</th>
                                <th>Librería</th>
                                <th>Auditor</th>    
                                <th>Tipo de auditoría</th>
                                <th>Fecha de apertura</th>                                    
                                <th>Fecha de cierre</th>                                    
                            </tr>
                        </thead>
                        <tbody data-link="row" class="rowlink">
                            <?php
                            foreach ($auditorias as $auditoria) {
                                $libreria = new Libreria($auditoria->getLibreria());
                                if (($id_auditor = $auditoria->getAuditor()) != null) {
                                    $auditor = new Auditor($id_auditor);
                                }
                                ?>
                                <tr>
                                    <td><a href="auditoria/<?php echo $auditoria->getId(); ?>"><?php echo $auditoria->getEstado(); ?></a></td>
                                    <td><?php echo $libreria->getNombreComercial(); ?></td>
                                    <td><?php echo ($id_auditor == null ? 'Sin asignar' : $auditor->getNombre() . ' ' . $auditor->getApellidos()); ?></td>
                                    <td><?php echo $auditoria->getTipoAuditoria(); ?></td>
                                    <td><?php echo Util::formatDateLocal($auditoria->getFechaApertura()); ?></td>                                        
                                    <td><?php echo Util::formatDateLocal($auditoria->getFechaCierre()); ?></td>                                        
                                </tr>
                            <?php }
                            ?>                            
                        </tbody>
                    </table> 
                </div>

            </div>
        </div>

    </div></div></section>
<script src="<?php echo SITE_URL; ?>/theme/assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo SITE_URL; ?>/theme/assets/js/locales/bootstrap-datepicker.es.js"></script>
<script src="<?php echo SITE_URL; ?>/theme/assets/js/filterinform.js"></script>
