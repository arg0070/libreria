<?php
global $GET;
$return = false;
$post = !isset($_POST['id']) || !($lib_id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT)) ? true : false;
$get = $GET == null || !($lib_id = filter_var($GET, FILTER_VALIDATE_INT)) ? true : false;

if (($post && $get) || $_SESSION['user_type'] != 'ADM') {
    header('Location: inicio');
    exit;
}
try {
    $libreria = new Libreria($lib_id);
    $current_audits = $libreria->getAudits(false);
    $past_audits = $libreria->getAudits(true);
    $lib_doc = $libreria->getDocumentacion();
} catch (Exception $e) {
    ?>
    <section class="container"><div class="row"><div class="col-md-12">
                <div class="alert alert-danger">
                    <?php echo($e->getMessage()); ?>
                    <a href="<?php echo THEME::roleRedirect($_SESSION['user_type']) ?>">Volver atrás.</a>
                </div>
            </div></div></section>
    <?php
    exit;
}
?>
<section class="container">
    <div class="row">
        <header class="b-line">
            <h1>Datos de Librería</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>
        <div class="col-sm-3 col-md-2">
            <?php Theme::loadAdminSideMenu(); ?>
        </div>
        <div class="col-md-9"> 
            <h4 class="h-inner">Datos personales</h4>
            <div class="col-md-6">
                <strong>Nombre Comercial</strong>
                <div class="well well-sm"><?php echo $libreria->getNombreComercial(); ?></div>
            </div>
            <div class="col-md-6">
                <strong>CIF</strong>
                <div class="well well-sm"><?php echo $libreria->getCIF(); ?></div>
            </div>
            <div class="col-md-6">
                <strong>Dirección</strong>
                <div class="well well-sm"><?php echo $libreria->getDireccion(); ?></div>
            </div>
            <div class="col-md-6">
                <strong>Población</strong>
                <div class="well well-sm"><?php echo $libreria->getPoblacion(); ?></div>
            </div>
            <div class="col-md-6">
                <strong>Razón Social</strong>
                <div class="well well-sm"><?php echo $libreria->getRaZonSocial(); ?></div>
            </div>
            <div class="col-md-6">
                <strong>Nombre del Titular</strong>
                <div class="well well-sm"><?php echo $libreria->getNombreTitular(); ?></div>
            </div>
            <div class="col-md-6">
                <strong>Código Postal</strong>
                <div class="well well-sm"><?php echo $libreria->getCP(); ?></div>
            </div>
            <div class="col-md-6">
                <strong>Provincia</strong>
                <div class="well well-sm"><?php echo $libreria->getProvincia(); ?></div>
            </div> 
            <div class="col-md-6">
                <strong>Email</strong>
                <div class="well well-sm"><?php echo $libreria->getEmail(); ?></div>
            </div>
            <div class="clearfix"></div>
            <hr>

            <div class="col-md-6">
                <h4 class="h-inner">Estado</h4>
                <form method="post" action="admin-userLibEdit">
                    <input type="hidden" name="id" value="<?php echo $lib_id; ?>">  
                    <div class="form-group">
                        <label for="apto">¿Apto?</label>
                        <select size="1" class="form-control" id="apto" name="apto">
                            <?php $apto = ($libreria->isApto() ? 'SI' : 'NO'); ?>
                            <option selected><?php echo $apto; ?></option>
                            <option><?php echo ($apto == 'SI' ? 'NO' : 'SI'); ?></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="apto_msg">Mensaje</label>
                        <textarea name="apto_msg" id="apto_msg" class="form-control"></textarea>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="1" name="sendemail">
                            Enviar correo electrónico
                        </label>
                    </div>
                    <input type="submit" class="btn btn-info" value="Guardar" name="editalib"> 
                </form>
            </div>
            <div class="col-md-6">
                <h4 class="h-inner">Documentación</h4>
                <ul class="list-unstyled">
                    <?php foreach ($lib_doc as $doc) { ?>
                        <li><i class="fa fa-file"></i><a href="<?php echo $doc->getRuta() ?>"> <?php echo $doc->getName(); ?></a><small> <?php echo Util::formatDate($doc->getFecha()); ?></small></li>
                    <?php } ?> 
                </ul> 
            </div>



            <div class="clearfix"></div>
            <hr>
            <h4>Auditorias pendientes</h4>
            <?php if ($current_audits == null) { ?>
                <div class="alert alert-warning">No tiene ninguna auditoria pendiente.</div>
            <?php } else { ?>                    
                <form action="auditoria" method="post">
                    <div class="table-responsive">
                        <table class="table table-hover" id="tableact">
                            <thead>
                                <tr><th>Estado</th>
                                    <th>Auditor</th>
                                    <th>Tipo</th>
                                    <th>Fecha de apertura</th>
                                    <th>Fecha de cierre</th>                                       
                                    <th>Informe</th></tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($current_audits as $audit) {
                                    if (($id_auditor = $audit->getAuditor()) != null) {
                                        $auditor = new Auditor($id_auditor);
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $audit->getEstado(); ?></td>
                                        <td><?php echo ($id_auditor == null ? 'Sin asignar' : $auditor->getNombre() . ' ' . $auditor->getApellidos()); ?></td>
                                        <td><?php echo $audit->getTipoAuditoria(); ?></td>  
                                        <td><?php echo Util::formatDate($audit->getFechaApertura()); ?></td>
                                        <td><?php echo Util::formatDate($audit->getFechaCierre()); ?></td>                                                                                                                              
                                        <td><button onclick="submit" class="btn btn-info" name="id" value="<?php echo $audit->getId(); ?>">Informe</button></td></tr>
                                    <?php
                                }
                                ?>                                        
                            </tbody>
                        </table>
                    </div>
                </form>
            <?php } ?>
            <hr>
            <h4>Histórial de auditorias</h4>
            <?php if ($past_audits == null) { ?>
                <div class="alert alert-warning">No tiene ninguna auditoria realizada.</div>
            <?php } else { ?>
                <form action="auditoria" method="post">
                    <div class="table-responsive">
                        <table class="table table-hover" id="tablehis">
                            <thead>
                                <tr><th>Estado</th>
                                    <th>Auditor</th>
                                    <th>Tipo</th>
                                    <th>Fecha de apertura</th>
                                    <th>Fecha de cierre</th>                                       
                                    <th>Informe</th></tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($past_audits as $audit) {
                                    if (($id_auditor = $audit->getAuditor()) != null) {
                                        $auditor = new Auditor($id_auditor);
                                    }
                                    ?>
                                    <tr><td><?php echo $audit->getEstado(); ?></td>
                                        <td><?php echo ($id_auditor == null ? 'Sin asignar' : $auditor->getNombre() . ' ' . $auditor->getApellidos()); ?></td>
                                        <td><?php echo $audit->getTipoAuditoria(); ?></td>  
                                        <td><?php echo Util::formatDate($audit->getFechaApertura()); ?></td>
                                        <td><?php echo Util::formatDate($audit->getFechaCierre()); ?></td>                                                                                                                              
                                        <td><button onclick="submit" class="btn btn-info" name="id" value="<?php echo $audit->getId(); ?>">Informe</button></td></tr>
                                    <?php } ?>                                                                              
                            </tbody>
                        </table>
                    </div>
                </form>
            <?php } ?>                                                         
        </div>
    </div>    
</section>
<script>
    $(document).ready(function() {
        $('#tableact').DataTable();
        $('#tablehis').DataTable();
    });
</script>