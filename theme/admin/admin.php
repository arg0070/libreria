<?php
if ($_SESSION['user_type'] != 'ADM') {
    header('Location: inicio');
}
require ROOT . DS . 'app' . DS . 'modules' . DS . 'ManagerADM.php';
$manager = new ManagerADM();
$auditorias_asignar = $manager->getUnassignedAudits();
?>
<section class="container">
    <div class="row">
        <header class="b-line">
            <h1>Panel de Administración</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>
        <div class="col-sm-3 col-md-2">
            <?php Theme::loadAdminSideMenu(); ?>
        </div>
        <div class="col-md-9 ">
            <div class="stats">
                <div class="box box-border">0 Auditorías nuevas</div>
                <div class="box box-border">0 Auditorías nuevas</div>
            </div>
            
                <h2>Auditorias</h2>
                <div class="col-md-6">
                    <h4>Auditorias sin asignar</h4>
                    <p><?php
                        if ($auditorias_asignar != null) {
                            echo count($auditorias_asignar);
                        }
                        ?> <a href="admin-auditPre">auditorías pendientes</a></p>
                </div>

                <div class="col-md-6">
                    <h4>Auditorias pendientes de revisión</h4>
                </div>                                                                           
        </div>          
    </div>
</section>

