<?php
if ($_SESSION['user_type'] != 'ADM') {
    header('Location: ../inicio');
}
require ROOT . DS . 'app' . DS . 'modules' . DS . 'ManagerADM.php';
$manager = new ManagerADM();
$auditores = $manager->manageAuditors();
?>
<section class="container">
    <div class="row">
        <header class="b-line">
            <h1>Todos los auditores</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>
        <div class="col-sm-3 col-md-2">
            <?php Theme::loadAdminSideMenu(); ?>
        </div>
        <div class="col-md-9 ">
            <?php if ($auditores == null) { ?> 
                <h4>Ningún auditor en el sistema</h4>                
            <?php } else { ?>      
                <form action="admin-userAudEdit" method="post">
                    <div class="table-responsive">
                    <table class="table table-hover" id="tableaud">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>Email</th>
                                <th>Homologado</th>                               
                                <th>Fecha de homologación</th>                                
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($auditores as $auditor) {                                                        
                                ?>
                                <tr>
                                    <td><?php echo $auditor->getNombre(); ?></td>
                                    <td><?php echo $auditor->getApellidos(); ?></td>
                                    <td><?php echo $auditor->getEmail(); ?></td>
                                    <td><?php echo ($auditor->isHomologado()? 'Si' : 'No'); ?></td>
                                    <td><?php echo Util::formatDate($auditor->getFechaHomologacion()); ?></td>
                                    <td><button onclick="submit" class="btn btn-info" name="id" value="<?php echo $auditor->getId(); ?>">Editar</button></td>
                                </tr>
                            <?php }
                            ?>                            
                        </tbody>
                    </table> 
                    </div>
                </form>
                <script>
                    $(document).ready(function() {
                        $('#tableaud').DataTable();
                    });
                </script>
            <?php } ?>
        </div> 
    </div>
</section>
