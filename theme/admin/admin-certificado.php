<?php
if ($_SESSION['user_type'] != 'ADM' || !isset($_POST['id']) || !($id_auditoria = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT))) {
    header('Location: inicio');
    exit;
}

try {
    $auditoria = new Auditoria($id_auditoria);
    $id_certificado = $auditoria->getCertificado();    
    $certificado = new Certificado($id_certificado);
} catch (Exception $e) {
    ?>
    <section class="container"><div class="row"><div class="col-md-12">
                <div class="alert alert-danger">
                    <?php echo($e->getMessage()); ?>
                    <a href="<?php echo THEME::roleRedirect($_SESSION['user_type']) ?>">Volver atrás.</a>
                </div>
            </div></div></section>
    <?php
    exit;
}
$historico = $certificado->getHistorico();     
?>

<section class="container">
    <div class="row">
        <header class="b-line">
            <h1>Emisión de certificado</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>
        <div class="col-sm-3 col-md-2">
            <?php Theme::loadAdminSideMenu(); ?>
        </div>
        <div class="col-md-9">  
            <h4>Histórico del certificado</h4>
            <?php if ($certificado->errors) { ?>            
                <div class="col-md-12">
                    <div class="alert alert-danger"><?php $certificado->showErrors(); ?></div>
                </div>                      
            <?php }if ($certificado->messages) { ?>            
                <div class="col-md-12">
                    <div class="alert alert-info"><?php $certificado->showMessages(); ?></div>
                </div>                      
            <?php } ?>
            <?php if ($historico != null) { ?>
                <form action="auditoria" method="post">
                    <div class="table-responsive">
                        <table class="table table-hover" id="tableaudit">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Librería</th>
                                    <th>Auditor</th>  
                                    <th>Tipo</th>
                                    <th>Fecha de apertura</th>
                                    <th>Fecha de cierre</th>
                                    <th>Informe</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($historico as $auditoria) {
                                    $libreria = new Libreria($auditoria->getLibreria());
                                    if (($id_auditor = $auditoria->getAuditor()) != null) {
                                        $auditor = new Auditor($id_auditor);
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $auditoria->getEstado(); ?></td>
                                        <td><?php echo $libreria->getNombreComercial(); ?></td>
                                        <td><?php echo ($id_auditor == null ? 'Sin asignar' : $auditor->getNombre() . ' ' . $auditor->getApellidos()); ?></td>
                                        <td><?php echo $auditoria->getTipoAuditoria(); ?></td>
                                        <td><?php echo Util::formatDateMin($auditoria->getFechaApertura()); ?></td>                                        
                                        <td><?php echo Util::formatDateMin($auditoria->getFechaCierre()); ?></td>
                                        <td><button onclick="submit" class="btn btn-info" name="id" value="<?php echo $auditoria->getId(); ?>">Ver informe</button></td>
                                    </tr>
                                <?php }
                                ?>                            
                            </tbody>
                        </table> 
                    </div>
                </form>
                <script>
                    $(document).ready(function() {
                        $('#tableaudit').DataTable({paging: false, "sDom": '<"top">t<"botton">', "pageLength": 3});
                    });
                </script>

            <?php } else { ?>
                <p>El certificado es nuevo.</p>
            <?php } ?>
            <hr>
            <h4>Datos del certificado</h4>            
                <?php
                if ($historico != null) { ?>
                    <div class="box box-border">
                  <?php  if ($certificado->isValid()) {
                        ?>                       
                <p><i class="fa fa-check fa-2x text-success"></i> El certificado es válido</p>
                    <?php } else { ?>
                        <p><i class="fa fa-times fa-2x text-danger"></i> El certificado ha caducado</p>
                    <?php } ?>
                    </div>
             <?php   }
                ?>            
            <form method="post" action="admin-certificado" name="newcert">
                <input type="hidden" value="<?php echo $id_auditoria; ?>" name="id">
                <div class="form-group col-md-6">
                    <label for="fecha_emision" class="control-label">Fecha de emisión<small> (dd/mm/aaaa)</small></label>
                    <div class="input-group date"><input type="text" class="form-control" id="fecha_emision" name="fecha_emision" value="<?php echo Util::formatDateLocal($historico == null ? date('Y-m-d') : $certificado->getFechaEmision()); ?>"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>
                </div>
                <div class="form-group col-md-6">
                    <label for="fecha_caducidad" class="control-label date">Fecha de caducidad<small> (dd/mm/aaaa)</small></label>
                    <div class="input-group date"><input type="text" class="form-control" id="fecha_caducidad" name="fecha_caducidad" value="<?php echo Util::formatDateLocal($historico == null ? date('Y-m-d', strtotime('+3 years')) : $certificado->getFechaCaducidad()); ?>"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>
                </div>
                <div class="form-group col-md-6">
                    <label for="enviado" class="control-label">Enviado</label>
                    <select size="1" class="form-control" id="enviado" name="enviado">
<?php $enviado = ($certificado->hasBeenSent() ? 'SI' : 'NO'); ?>
                        <option selected><?php echo $enviado; ?></option>
                        <option><?php echo ($enviado == 'SI' ? 'NO' : 'SI'); ?></option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="fecha_envio" class="control-label date">Fecha de envío<small> (dd/mm/aaaa)</small></label>
                    <div class="input-group date"><input type="text" class="form-control" id="fecha_envio" name="fecha_envio" value="<?php echo $historico == null ? '' : Util::formatDateLocal($certificado->getFechaEnvio()); ?>" disabled><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>
                </div>                    
                <input type="submit" class="btn btn-info" name="<?php echo ($auditoria->getEstado() == 'Cerrada' ? 'updatecert' : 'newcert'); ?>"  value="Guardar">                              
            </form>            
        </div>

    </div></div></section>
<script src="<?php echo SITE_URL; ?>/theme/assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo SITE_URL; ?>/theme/assets/js/locales/bootstrap-datepicker.es.js"></script>
<script>$('.date input').datepicker({format: "dd/mm/yyyy", weekStart: 1, daysOfWeekDisabled: "0,6", language: "es"});</script>
<script>$('#fecha_envio').attr('disabled', ($('#enviado').val() === 'SI' ? false : true));
    $('#enviado').change(function() {
        $('#fecha_envio').attr('disabled', ($('#enviado').val() === 'SI' ? false : true));
    });</script>