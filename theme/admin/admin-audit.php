<?php
if ($_SESSION['user_type'] != 'ADM') {
    header('Location: inicio');
}
require ROOT . DS . 'app' . DS . 'modules' . DS . 'ManagerADM.php';
$manager = new ManagerADM();
$auditorias = $manager->manageAudits();
?>
<section class="container">
    <div class="row">
        <header class="b-line">
            <h1>Todas las auditorías</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>
        <div class="col-sm-3 col-md-2">
            <?php Theme::loadAdminSideMenu(); ?>
        </div>
        <div class="col-md-9 ">
            <?php if ($auditorias == null) { ?> 
                <h4>Ninguna auditoría en el sistema</h4>                
            <?php } else { ?>      
                <form action="auditoria" method="post">
                    <div class="table-responsive">
                        <table class="table table-hover" id="tableaudit">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Librería</th>
                                    <th>Auditor</th>    
                                    <th>Tipo de auditoría</th>
                                    <th>Fecha de apertura</th>                                    
                                    <th>Fecha de cierre</th>
                                    <th>Informe</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($auditorias as $auditoria) {
                                    $libreria = new Libreria($auditoria->getLibreria());
                                    if (($id_auditor = $auditoria->getAuditor()) != null) {
                                        $auditor = new Auditor($id_auditor);
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $auditoria->getEstado(); ?></td>
                                        <td><?php echo $libreria->getNombreComercial(); ?></td>
                                        <td><?php echo ($id_auditor == null ? 'Sin asignar' : $auditor->getNombre() . ' ' . $auditor->getApellidos()); ?></td>
                                        <td><?php echo $auditoria->getTipoAuditoria(); ?></td>
                                        <td><?php echo Util::formatDate($auditoria->getFechaApertura()); ?></td>                                        
                                        <td><?php echo Util::formatDate($auditoria->getFechaCierre()); ?></td>
                                        <td><button onclick="submit" class="btn btn-info" name="id" value="<?php echo $auditoria->getId(); ?>">Ver informe</button></td>
                                    </tr>
                                <?php }
                                ?>                            
                            </tbody>
                        </table> 
                    </div>
                </form>
                <script>
                    $(document).ready(function() {
                        $('#tableaudit').DataTable();
                    });
                </script>
            <?php } ?>
        </div> 
    </div>
</section>