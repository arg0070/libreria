<?php
global $GET;
$return = false;
$post = !isset($_POST['id']) || !($aud_id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT)) ? true : false;
$get = $GET == null || !($aud_id = filter_var($GET, FILTER_VALIDATE_INT)) ? true : false;

if (($post && $get) || $_SESSION['user_type'] != 'ADM') {
    header('Location: inicio');
    exit;
}
try {
    $auditor = new Auditor($aud_id);
    $aud_doc = $auditor->getDocumentacion();
    $current_audits = $auditor->getAudits(false);
    $past_audits = $auditor->getAudits(true);
} catch (Exception $e) {
    ?>
    <section class="container"><div class="row"><div class="col-md-12">
                <div class="alert alert-danger">
                    <?php echo($e->getMessage()); ?>
                    <a href="<?php echo THEME::roleRedirect($_SESSION['user_type']) ?>">Volver atrás.</a>
                </div>
            </div></div></section>
    <?php
    exit;
}
?>
<section class="container">
    <div class="row">
        <header class="b-line">
            <h1>Datos de Auditor</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>
        <div class="col-sm-3 col-md-2">
            <?php Theme::loadAdminSideMenu(); ?>
        </div>
        <div class="col-md-9">  
            <h4 class="h-inner">Datos personales</h4>                                                            
            <div class="col-md-6">
                <strong>Nombre</strong>
                <div class="well well-sm"><?php echo $auditor->getNombre(); ?></div>
            </div>
            <div class="col-md-6">
                <strong>Apellidos</strong>
                <div class="well well-sm"><?php echo $auditor->getApellidos(); ?></div>
            </div>
            <div class="col-md-6">
                <strong>Email</strong>
                <div class="well well-sm"><?php echo $auditor->getEmail(); ?></div>
            </div>                
            <div class="col-md-6">
                <strong>Fecha de homologación</strong>
                <div class="well well-sm"><?php echo Util::formatDateMin($auditor->getFechaHomologacion()); ?></div>
            </div>
            <div class="clearfix"></div>
            <hr>

            <div class="col-md-6">
                <h4 class="h-inner">Homologado</h4>
                <form method="post" action="admin-userAudEdit">
                    <input type="hidden" name="id" value="<?php echo $aud_id; ?>">

                    <div class="form-group">
                        <label for="aud_homologado" class="control-label">¿Homologado?</label>
                        <select size="1" class="form-control" id="aud_homologado" name="homologado">
                            <?php $homologado = ($auditor->isHomologado() ? 'SI' : 'NO'); ?>
                            <option selected><?php echo $homologado; ?></option>
                            <option><?php echo ($homologado == 'SI' ? 'NO' : 'SI'); ?></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="homo_msg">Mensaje</label>
                        <textarea name="homo_msg" id="homo_msg" class="form-control"></textarea>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="1" name="sendemail">
                            Enviar correo electrónico
                        </label>
                    </div>
                    <input type="submit" class="btn btn-info" value="Guardar" name="editaud"> 
                </form>
            </div>
            <div class="col-md-6">
                <h4 class="h-inner">Documentación</h4>
                <ul class="list-unstyled">
                    <?php foreach ($aud_doc as $doc) { ?>
                        <li><i class="fa fa-file"></i><a href="<?php echo $doc->getRuta() ?>"> <?php echo $doc->getName(); ?></a><small> <?php echo Util::formatDate($doc->getFecha()); ?></small></li>
                    <?php } ?> 
                </ul> 
            </div>
            <div class="clearfix"></div>
            <hr>
            <?php
            if ($auditor->isHomologado()) {
                ?>
                <div class="col-md-12">
                    <h4 class="h-inner">Auditorias pendientes</h4>
                    <?php if ($current_audits == null) { ?>
                        <div class="alert alert-warning">Ninguna auditoria pendiente.</div>
                    <?php } else { ?>
                        <form action="auditoria" method="post">
                            <div class="table-responsive">
                                <table class="table table-hover" id='tableact'>
                                    <thead>
                                        <tr><th>Estado</th>
                                            <th>Librería</th>
                                            <th>Tipo</th>
                                            <th>Fecha de apertura</th>
                                            <th>Fecha de cierre</th>                                                                                                                                    
                                            <th>Informe</th></tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($current_audits as $audit) {
                                            $libreria = new Libreria($audit->getLibreria());
                                            ?>
                                            <tr>
                                                <td><?php echo $audit->getEstado(); ?></td>
                                                <td><?php echo $libreria->getNombreComercial(); ?></td>
                                                <td><?php echo $audit->getTipoAuditoria(); ?></td>
                                                <td><?php echo Util::formatDate($audit->getFechaApertura()); ?></td>
                                                <td><?php echo Util::formatDate($audit->getFechaCierre()); ?></td>                                                                                                                                                                                                
                                                <td><button onclick="submit" class="btn btn-info" name="id" value="<?php echo $audit->getId(); ?>">Informe</button></td></tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody></table>
                            </div>
                        </form>
                    <?php } ?>
                    <hr>
                    <h4  class="h-inner">Histórial de auditorias</h4>
                    <?php if ($past_audits == null) { ?>
                        <div class="alert alert-warning">Ninguna auditoria realizada.</div>
                    <?php } else { ?>
                        <form action="auditoria" method="post">
                            <div class="table-responsive">
                                <table class="table table-hover" id="tablehis">
                                    <thead>
                                        <tr><th>Estado</th>
                                            <th>Librería</th>
                                            <th>Tipo</th>
                                            <th>Fecha de apertura</th>
                                            <th>Fecha de cierre</th>                                                                                                                                    
                                            <th>Informe</th></tr>
                                    <thead>
                                    <tbody>
                                        <?php
                                        foreach ($past_audits as $audit) {
                                            $libreria = new Libreria($audit->getLibreria());
                                            ?>
                                            <tr><td><?php echo $audit->getEstado(); ?></td>
                                                <td><?php echo $libreria->getNombreComercial(); ?></td>
                                                <td><?php echo $audit->getTipoAuditoria(); ?></td>
                                                <td><?php echo Util::formatDate($audit->getFechaApertura()); ?></td>
                                                <td><?php echo Util::formatDate($audit->getFechaCierre()); ?></td>                                                                                                                                                                                                
                                                <td><button onclick="submit" class="btn btn-info" name="id" value="<?php echo $audit->getId(); ?>">Informe</button></td></tr>
                                            <?php }
                                            ?>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    <?php } ?>
                </div>
            <?php } ?>                                  
        </div>         
    </div>    
</section>
<script>
    $(document).ready(function() {
        $('#tableact').DataTable();
        $('#tablehis').DataTable();
    });
</script>