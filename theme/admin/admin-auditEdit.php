<?php
if (!isset($_POST['id']) ||
        !($audit_id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT)) ||
        $_SESSION['user_type'] != 'ADM') {
    header('Location: inicio');
    exit;
}
require ROOT . DS . 'app' . DS . 'modules' . DS . 'ManagerADM.php';
$manager = new ManagerADM();
$auditores = $manager->auditoresHolologados();
try {
    $auditoria = new Auditoria($audit_id);
    $libreria = new Libreria($auditoria->getLibreria());    
} catch (Exception $e) {
    ?>
    <section class="container"><div class="row"><div class="col-md-12">
                <div class="alert alert-danger">
    <?php echo($e->getMessage()); ?>
                    <a href="<?php echo THEME::roleRedirect($_SESSION['user_type']) ?>">Volver atrás.</a>
                </div>
            </div></div></section>
    <?php
    exit;
}
?>
<section class="container">
    <div class="row">
        <header class="b-line">
            <h1>Editar Auditoría</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>
        <div class="col-sm-3 col-md-2">
<?php Theme::loadAdminSideMenu(); ?>
        </div>
        <div class="col-md-9">
           <?php if (($id_auditor = $auditoria->getAuditor()) != null) { ?>
            <div class="box box-border"><p>Auditor añadido con éxito</p>
                    <form method="post" action="auditoria">
                        <button class="btn btn-info" onclick="submit" value="<?php echo $audit_id;?>" name="id">Ver informe</button>
                </form></div>
           <?php exit; }                      
           if ($auditoria->errors) { ?>            
                <div class="box"><div class="col-md-12">
                        <div class="alert alert-danger"><?php $auditoria->showErrors(); ?></div>
                    </div></div>                      
            <?php } ?>
            <h4>Datos de Librería</h4>
            <div class="col-md-6">
                <strong>Nombre Comercial</strong>
                <div class="well well-sm"><?php echo $libreria->getNombreComercial(); ?></div>
            </div>
            <div class="col-md-6">
                <strong>Razón Social</strong>
                <div class="well well-sm"><?php echo $libreria->getRaZonSocial(); ?></div>
            </div>
            <div class="col-md-6">
                <strong>CIF</strong>
                <div class="well well-sm"><?php echo $libreria->getCIF(); ?></div>
            </div>                        
            <div class="col-md-6">
                <strong>Nombre del Titular</strong>
                <div class="well well-sm"><?php echo $libreria->getNombreTitular(); ?></div>
            </div>
            <div class="col-md-6">
                <strong>Dirección</strong>
                <div class="well well-sm"><?php echo $libreria->getDireccion(); ?></div>
            </div>
            <div class="col-md-6">
                <strong>Código Postal</strong>
                <div class="well well-sm"><?php echo $libreria->getCP(); ?></div>
            </div>                        
            <div class="col-md-6">
                <strong>Población</strong>
                <div class="well well-sm"><?php echo $libreria->getPoblacion(); ?></div>
            </div>                        
            <div class="col-md-6">
                <strong>Provincia</strong>
                <div class="well well-sm"><?php echo $libreria->getProvincia(); ?></div>
            </div>                     
            <div class="clearfix"></div>
            <hr/>        

            <h4>Auditores disponibles:</h4>
            <?php if ($auditores == null) { ?>
                <div class="alert alert-warning">Ningún auditor homologado.</div>
<?php } else { ?>     
                <form class="form" action="admin-auditEdit" method="post">
                <table class="table" id="tableaud">
                    <thead>
                        <tr>   
                            <th>Seleccionar</th>
                            <th>Apellidos</th>
                            <th>Nombre</th>                                    
                        </tr>
                    </thead>
                    <tbody>
    <?php foreach ($auditores as $auditor) { ?>
                            <tr id="aud-<?php echo $auditor->getId(); ?>">
                                <td><input type="radio" name="id_auditor" value="<?php echo $auditor->getId(); ?>"</td>
                                <td><?php echo $auditor->getApellidos(); ?></td>                                                                                     
                                <td><?php echo $auditor->getNombre(); ?></td>
                            </tr>                               
                <?php } ?>                            
                    </tbody></table>                                                                                     
<?php } ?>
            <div class="clearfix"></div>
            <hr>
            <h4>Datos de la Auditoría <small><a class="btn btn-primary" href="auditoria/<?php echo $audit_id; ?>">Ver informe</a></small></h4>                            
                <input type="hidden" name="id" value="<?php echo $audit_id; ?>"> 
                <div class="form-group col-md-6">
                    <label for="tipo_audit" class="control-label">Tipo de auditoría</label>
                    <input type="text" class="form-control" id="tipo_audit" name="tipo_audit" value="<?php echo $auditoria->getTipoAuditoria(); ?>" readonly>
                </div>
                <div class="clearfix"></div>
                <div class="form-group col-md-6">
                    <label for="fecha_apertura_audit" class="control-label">Fecha de apertura<small> (dd/mm/aaaa)</small></label>
                    <div class="input-group date"><input type="text" class="form-control date" id="fecha_apertura_audit" name="fecha_apertura_audit" value="<?php echo Util::formatDateLocal($auditoria->getFechaApertura()); ?>"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>
                </div>
                <div class="form-group col-md-6">
                    <label for="fecha_cierre_audit" class="control-label">Fecha de cierre<small> (dd/mm/aaaa)</small></label> <abbr title="Fecha tope en la que la librería podrá aportar respuesta a la auditoría."><i class="fa fa-question-circle"></i></abbr>
                    <div class="input-group date"><input type="text" class="form-control date" id="fecha_cierre_audit" name="fecha_cierre_audit" value="<?php echo Util::formatDateLocal($auditoria->getFechaCierre()); ?>"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>
                </div>
                    <div class="form-group col-md-12">
                        <input type="submit" class="btn btn-info" value="Guardar cambios" name="setauditor">
                </div>
            </form>                      
        </div>
    </div></section>
<script>$(document).ready(function() {
        $('#tableaud').DataTable({paging: true, "pagingType": "simple", "sDom": '<"top"f>t<"botton"p>', "pageLength": 10});
    });</script>
<script src="<?php echo SITE_URL; ?>/theme/assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo SITE_URL; ?>/theme/assets/js/locales/bootstrap-datepicker.es.js"></script>
<script>$('.date input').datepicker({format: "dd/mm/yyyy", weekStart: 1, daysOfWeekDisabled: "0,6", language: "es"});</script>