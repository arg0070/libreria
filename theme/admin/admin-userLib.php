<?php
if ($_SESSION['user_type'] != 'ADM') {
    header('Location: ../inicio');
}
require ROOT . DS . 'app' . DS . 'modules' . DS . 'ManagerADM.php';
$manager = new ManagerADM();
$librerias = $manager->manageLibs();
?>
<section class="container">
    <div class="row">
        <header class="b-line">
            <h1>Todas las librerías</h1>
            <ol class="breadcrumb"><?php Theme::getBreadCrumb(); ?></ol>
        </header>
        <div class="col-sm-3 col-md-2">
            <?php Theme::loadAdminSideMenu(); ?>
        </div>
        <div class="col-md-9 ">
            <?php if ($librerias == null) { ?> 
                <h4>Ninguna librería en el sistema</h4>                
            <?php } else { ?>      
                <form action="admin-userLibEdit" method="post">
                    <div class="table-responsive">
                    <table class="table table-hover" id="tablelib">
                        <thead>
                            <tr>
                                <th>Nombre comercial</th>
                                <th>Razón social</th>
                                <th>Titular</th>                                
                                <th>Población</th>
                                <th>Provincia</th>   
                                <th>Apto</th>
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($librerias as $libreria) {                                                        
                                ?>
                                <tr>
                                    <td><?php echo $libreria->getNombreComercial(); ?></td>
                                    <td><?php echo $libreria->getRaZonSocial(); ?></td>
                                    <td><?php echo $libreria->getNombreTitular(); ?></td>                                    
                                    <td><?php echo $libreria->getPoblacion(); ?></td>
                                    <td><?php echo $libreria->getProvincia(); ?></td>
                                    <td><?php echo ($libreria->isApto() ? 'SI': 'NO'); ?></td>
                                    <td><button onclick="submit" class="btn btn-info" name="id" value="<?php echo $libreria->getId(); ?>">Editar</button></td>
                                </tr>
                            <?php }
                            ?>                            
                        </tbody>
                    </table> 
                    </div>
                </form>
                <script>
                    $(document).ready(function() {
                        $('#tablelib').DataTable();
                    });
                </script>
            <?php } ?>
        </div> 
    </div>
</section>
