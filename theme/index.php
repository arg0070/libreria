 <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container">
                <div class="col-md-6 col-sm-6"> 
                    <h1><i class="fa fa-chevron-right"></i> Librería de Referencia Cultural</h1></div>
            </div>
        </div>

<section class="container">
    <header class="b-line"></header>
    
            <div class="row">
                <div class="col-md-9">
                    
                        <h1>La librería como Agente Cultural</h1>
                        <p>El Sello de Referencia Cultural para Librerías pretende asociar el concepto de "Librero = Agente Cultural" de forma que se establezca un nuevo rol y un nuevo status que fortalezcan el factor de <strong>Consolidación y Digificación de la Profesión</strong>. Debe darse respuestas inemdiatas a través e una mayor implicación del librero eb la actividad cultural de su entorno.</p>
                        <p>Para ello es necesario promover un cambio cultural en los propios liberos y en los responsables de las Administraciones que han de ver a los profesionales de las librerías como colaboradores permanentes.</p>
                        <br/>
                        <h3>Sello de Referencia Cultural para Liberías</h3>
                        <p>La creación de un distintivo, sello o marca aporta las siguientes <strong>ventajas</strong>:</p>
                        <ul>
                            <li>Permite comprobar el cumplimiento con unos requisitos específicos, facilitando así oportunidades de mejora.</li>
                            <li>Aporta credibilidad y confianza ante el cliente, dadas las garantías que supone obstentar el distintivo o sello.</li>
                            <li>Alinea los intereses de las Administración con los de empresas y consumidores.</li>
                        </ul>
                        <hr>
                        <h2>Actualidad</h2>
                        <article class="media">
                            <a href="#" class="pull-left"><img class="img-thumbnail img-responsive media-object" src="http://placehold.it/250x250" alt="News" title="News"/></a>
                            <header class="news media-heading"><h4>Título de la noticia <small>01 Enero 2001</small></h4></header>
                            <section class="media-body"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </section>
                        </article>      
                        <article class="media">
                            <a href="#" class="pull-right"><img class="img-thumbnail img-responsive media-object" src="http://placehold.it/250x250" alt="News" title="News"/></a>
                            <header class="news media-heading"><h4>Título de la noticia <small>01 Enero 2001</small></h4></header>
                            <section class="media-body"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </section>
                        </article>      
                    
                </div>
                <aside class="col-md-3 l-line">
                    <?php Theme::loadSidePlugins();?>
                </aside>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <h2 class="text-center">Libreros</h2>
                        <img class="img-responsive" src="#" alt="News" title="News"/>
                        <p class="text-center">Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                        <p class="text-center"><a class="btn btn-warning" href="#" role="button">Más información</a></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class=" box">
                        <h2 class="text-center">Auditores</h2>
                        <img class="img-responsive" src="#" alt="News" title="News"/>
                        <p class="text-center">Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                        <p class="text-center"><a class="btn btn-warning" href="#" role="button">Más información</a></p>
                    </div>
                </div>
            </div>
            <hr>
        </section>
        <!-- /container -->