<?php
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}
if (!defined('ROOT')) {
    define('ROOT', rtrim(dirname(__FILE__), '\\/'));
}
define('DEBUG', true);

require_once ROOT . DS . 'app' . DS . 'init.php';

Theme::load();
