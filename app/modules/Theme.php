<?php

/**
 * Description of Theme
 *
 * @author Daniel
 */
class Theme {
    private static $to_footer;
    private static $adm_page = false;
    private static $admin_plugins = array(
        'statistics', 'events'
    );
    private static $public_plugins = array(
        'events', 'newsfeed'
    );
    private static $breadcrumbs = array(
        'index' => array(
            'url' => 'inicio',
            'text' => '<i class="fa fa-home fa-lg"></i> Inicio',
            'parent' => ''
        ), 'eventos' => array(
            'url' => 'eventos',
            'text' => 'Calendario de eventos',
            'parent' => 'index'
        ), 'ayudacuenta' => array(
            'url' => 'ayudacuenta',
            'text' => 'Ayuda con su cuenta',
            'parent' => 'index'
        ), 'registeraud' => array(
            'url' => 'registeraud',
            'text' => 'Registro de Auditores',
            'parent' => 'index'
        ), 'registerlib' => array(
            'url' => 'registerlib',
            'text' => 'Registro de Librerías',
            'parent' => 'index'
        ), 'admin' => array(
            'url' => 'admin',
            'text' => 'Panel de Administración',
            'parent' => 'index'
        ), 'panelib' => array(
            'url' => 'panelib',
            'text' => 'Panel de Control',
            'parent' => 'index'
        ), 'panelau' => array(
            'url' => 'panelau',
            'text' => 'Panel de Control',
            'parent' => 'index'
        ),'opciones' => array(
            'url' => 'opciones',
            'text' => 'Opciones de cuenta',
            'parent' => ''
        ),'auditoria' => array(
            'url' => '#',
            'text' => 'Auditoría',
            'parent' => ''
        ), 'admin-userAud' => array(
            'url' => 'admin-userAud',
            'text' => 'Todos los auditores',
            'parent' => 'admin'
        ), 'admin-userAudEdit' => array(
            'url' => '#',
            'text' => 'Datos del auditor',
            'parent' => 'admin-userAud'
        ), 'admin-userLib' => array(
            'url' => 'admin-userLib',
            'text' => 'Todas las librerías',
            'parent' => 'admin'
        ), 'admin-userLibEdit' => array(
            'url' => '#',
            'text' => 'Datos de Librería',
            'parent' => 'admin-userLib'
        ), 'admin-auditPre' => array(
            'url' => 'admin-auditPre',
            'text' => 'Auditorías pendientes de asignación',
            'parent' => 'admin'
        ), 'admin-auditEdit' => array(
            'url' => '#',
            'text' => 'Editar auditoría',
            'parent' => 'admin-audit'
        ), 'admin-auditPos' => array(
            'url' => 'admin-auditPos',
            'text' => 'Auditorías pendientes de revisión',
            'parent' => 'admin'
        ), 'admin-audit' => array(
            'url' => 'admin-audit',
            'text' => 'Todas las auditorías',
            'parent' => 'admin'
        ), 'admin-certificado' => array(
            'url' => '#',
            'text' => 'Emisión de certificado',
            'parent' => 'admin-auditPos'
        ), 'admin-informes' => array(
            'url' => 'admin-informes',
            'text' => 'Informes',
            'parent' => 'admin'
        )
    );

    public static function load() {
        include_once THEME_PATH . DS . THEME . DS . 'header.php';
        self::loadContent();
        include_once THEME_PATH . DS . THEME . DS . 'footer.php';
        self::setToFooter('');
    }

    public static function loadContent() {
        global $GET;
        $GET = null;
        list($uri, $get) = self::getUri();
        if($get != ''){$GET = $get;}
        if (self::$adm_page && file_exists(THEME_PATH . DS . 'admin' . DS . $uri . '.php')) {
            include THEME_PATH . DS . 'admin' . DS . $uri . '.php';
        } elseif (file_exists(THEME_PATH . DS . $uri . '.php')) {
            include_once THEME_PATH . DS . $uri . '.php';
        } else {
            include_once THEME_PATH . DS . 'index.php';
        }        
    }

    public static function loadSidePlugins() {
        foreach (self::$public_plugins as $plugin) {
            self::loadPlugin($plugin);
        }
    }

    public static function loadAdminSideMenu() {
        include THEME_PATH . DS . 'plugins' . DS . 'adminmenu.php';
    }

    public static function loadPlugin($plugin) {        
            include THEME_PATH . DS . 'plugins' . DS . $plugin . '.php';        
    }

    public static function getUri() {    
        $get = '';
        $requestURI = explode('/', self::RequestUri());
        // Get the name of the page (last element on the Uri).
        $uri = array_pop($requestURI);
        if(preg_match('/^[0-9]+/', $uri)){
            $get .=$uri;
            $uri = array_pop($requestURI);
        }
        return array($uri, $get);
    }

    private static function RequestUri() {
        if (isset($_SERVER['HTTP_X_ORIGINAL_URL'])) { // check IIS 7.x with Microsoft Rewrite Module
            return $_SERVER['HTTP_X_ORIGINAL_URL'];
        } elseif (isset($_SERVER['HTTP_X_REWRITE_URL'])) { // check IIS with ISAPI_Rewrite, etc
            return $_SERVER['HTTP_X_REWRITE_URL'];
        } elseif (isset($_SERVER['REQUEST_URI'])) {
            return $_SERVER['REQUEST_URI'];
        } elseif (isset($_SERVER['ORIG_PATH_INFO'])) { // IIS 5.0, PHP as CGI
            return $_SERVER['ORIG_PATH_INFO'];
        }
    }

    public static function roleRedirect($role) {
        if ($role == 'LIB') {
            self::$adm_page = false;
            return 'panelib';
        } elseif ($role == 'AUD') {
            self::$adm_page = false;
            return 'panelau';
        } elseif ($role == 'ADM') {
            self::$adm_page = true;
            return 'admin';
        }
    }

    public static function getBreadCrumb() {
        if (isset($_SESSION['user_type'])) {
            self::$breadcrumbs['auditoria']['parent'] = self::roleRedirect($_SESSION['user_type']);
            self::$breadcrumbs['opciones']['parent'] = self::roleRedirect($_SESSION['user_type']);
        }
        if (self::$adm_page) {
            self::$breadcrumbs['auditoria']['parent'] = 'admin-audit';
        }
        list($find,) = self::getUri();
        if (!array_key_exists($find, self::$breadcrumbs)) {
            return;
        }
        $path = array();
        do {
            $path[] = '<li><a href="' . self::$breadcrumbs[$find]['url'] . '">' . self::$breadcrumbs[$find]['text'] . '</a></li> ';
            $find = self::$breadcrumbs[$find]['parent'];
        } while ($find != '');
        echo implode('', array_reverse($path));
    }
    
    public static function setToFooter($scripts){
        self::$to_footer .= $scripts;
    }
    
    public static function getToFooter(){
        return self::$to_footer;
    }

}
