<?php

class Manager {

    protected $db_connection = null;
    public $messages = array();
    public $errors = array();

    const MANTENIMIENTO = 'MANTENIMIENTO', CERTIFICACION = 'CERTIFICACION', RENOVACION = 'RENOVACION';

    public function __construct() {
        
    }

    protected function databaseConnection() {
        global $DB;
        // if connection already exists
        if ($this->db_connection != null) {
            return true;
        } else {
            $this->db_connection = $DB;
            return true;
        }
        // default return
        return false;
    }

    public function showErrors() {
        foreach ($this->errors as $error) {
            echo $error . '<br>';
        }
    }

    public function showMessages() {
        foreach ($this->messages as $message) {
            echo $message . '<br>';
        }
    }

     protected function getDocs($autor, $id_ref, $tipo) {
        if (filter_var($id_ref, FILTER_VALIDATE_INT) && filter_var($autor,FILTER_VALIDATE_EMAIL) && 
                filter_var(($tipo = $this->validateDocType($tipo)), FILTER_VALIDATE_INT)) {
            $docs_id = $this->db_connection->column('SELECT idDOCUMENTACION FROM documentacion WHERE AUTOR = :autor AND idREF = :id_ref AND TIPO = :tipo', array('autor' => $autor, 'id_ref' => $id_ref, 'tipo' => $tipo));
            $docs = array();
            for ($i = 0; $i < count($docs_id); $i++) {
                $docs[$i] = new Documentacion($docs_id[$i]);
            }
            return $docs;
        } else {
            $this->errors[] = 'Error al recuperar los archivos.';
            return null;
        }
    }
    
    /**
     * Valida el tipo de documento, devolviendo su id asociado si es válido.
     * @param type $type tipo de documento
     * @return null tipo inválido, int id asociado en la tabla de tipos
     */
    private function validateDocType($type) {
        if ($this->databaseConnection()) {
            $query_type = $this->db_connection->single('SELECT idTIPO FROM documentacion_tipos WHERE NOMBRE = :nombre', array('nombre' => $type), PDO::FETCH_OBJ);
            if ($query_type) {
                return $query_type;
            }
        }
        return null;
    }

    protected function dateError($date, $type, $class=''){
        $this->errors[] = $class.'La fecha de ' . $type . ' introducida <strong>' . $date . '.</strong> NO es válida o NO sigue el formato <strong>dd/mm/aaaa</strong>.<br/> Los datos NO han sido actualziados.<br/>';
    }
    
    protected function dateError2($date1, $type1, $date2, $type2, $class=''){
        $this->errors[] = $class.'La fecha de '.$type2.' introducida <strong>' . $date2 . '.</strong> DEBE ser mayor a la fecha de '.$type1.' <strong>' . $date1 . '</strong>.<br/>Los datos NO han sido actualizados.<br/>';
    }
    
     protected function dateError3($date1, $type1, $date2, $type2, $date3, $type3, $class=''){
        $this->errors[] = $class.'La fecha de ' . $type2 . ' introducida <strong>' . $date2 . '.</strong> debe encontrarse entre la fecha de ' . $type1 . ' y la de ' . $type3 . '.<br/>';
    }
    
    protected function timeError($time, $type){
        $this->errors[] = 'La hora de '.$type.' introducida <strong>' . $time . '</strong> NO es válida o NO sigue el formato <strong>hh:mm</strong>.<br/>';
    }
    
    protected function timeError2($time1, $type1, $time2, $type2){
        $this->errors[] = 'La hora de '.$type2.' introducida <strong>' . $time2 . '.</strong> DEBE ser mayor a la hora de '.$type1.' <strong>' . $time1 . '</strong>.<br/>';
    }

}
