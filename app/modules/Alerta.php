<?php

/**
 * Comprueba si la petición proviene de una llamada Ajax, en cuyo caso se definen
 * las constantes del sistema que se cargarían si le petición se realizase desde
 * index. Este paso es necesario para contar con la conexión a la base de datos.
 */
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if (!defined('DS')) {
        define('DS', DIRECTORY_SEPARATOR);
    }
    if (!defined('ROOT')) {
        define('ROOT', dirname(dirname(dirname(__FILE__))));
    }
    if (!defined('DEBUG')) {
        define('DEBUG', true);
    }
    require_once(ROOT . DS . 'app' . DS . 'init.php');
    $alerta = new Alerta();
}

class Alerta {

    // DataBase connection
    private $db_connection;

    /**
     * Constructor for Ajax requests.     
     */
    public function __construct() {
        if (isset($_POST["action"]) && !empty($_POST["action"])) {
            $action = $_POST["action"];
            switch ($action) {
                case "_loadNotifications": $this->loadNotifications();
                    break;
                case "_delNotifications": $this->deleteAlert();
                    break;
                case "_delAllNotifications": $this->deleteAllAlerts();                    
                    break;
                default: break;
            }
        }
    }

    /**
     * Load notifications.
     * return json array with notifications.
     */
    private function loadNotifications() {
        if ($this->databaseConnection()) {
            $destinatario = $_SESSION['user_email'];
            $query_notifications = $this->db_connection->query('SELECT * FROM alerta WHERE DESTINATARIO= :destinatario ORDER BY FECHA ASC', array('destinatario' => $destinatario), PDO::FETCH_OBJ);
        }
        if ($query_notifications) {
            $out = array();
            // Generate array with notifications values.
            foreach ($query_notifications as $notification) {
                $out[] = array(
                    'id' => $notification->idALERTA,
                    'fecha' => Util::formatDateMin($notification->FECHA),
                    'descripcion' => $this->getDescriptcion($notification->TIPO),
                    'mensaje' => $notification->MENSAJE,
                    'enlace' => $notification->ENLACE,
                    'destinatario' => $notification->DESTINATARIO
                );
            }
            echo json_encode(array('success' => 1, 'result' => $out));
        } else {
            echo json_encode(array('success' => 1, 'error' => 'Ninguna alerta'));
        }
    }

    /**
     * Get description for id alert type.
     * @param type $tipo id of alert type.
     * @return description
     */
    private function getDescriptcion($tipo) {
        return $this->db_connection->single('SELECT DESCRIPCION FROM alerta_tipos WHERE idTIPO= :tipo', array('tipo' => $tipo));
    }

    public function getAlertas() {
        $destinatario = $_SESSION['user_email'];
        if (filter_var($destinatario, FILTER_VALIDATE_EMAIL) && $this->databaseConnection()) {
            $alertas = array();
            $id_alertas = $this->db_connection->column('SELECT idALERTA FROM alerta WHERE DESTINATARIO= :destinatario ORDER BY FECHA ASC', array('destinatario' => $destinatario), PDO::FETCH_OBJ);
            for ($i = 0; $i < count($id_alertas); $i++) {
                $alertas[$i] = new Alerta($id_alertas[$i]);
            }
            return $alertas;
        }
        return null;
    }

    private function deleteAlert() {
        if (($id = filter_var(filter_input(INPUT_POST, 'n_id'), FILTER_VALIDATE_INT)) &&
                ($destinatario = filter_var(filter_input(INPUT_POST, 'n_destinatario'), FILTER_VALIDATE_EMAIL))) {
            if (($destinatario == $_SESSION['user_email']) && $this->databaseConnection()) {
                $query_delete = $this->db_connection->query('DELETE FROM alerta WHERE idALERTA= :id_alerta', array('id_alerta' => $id));
                echo json_encode($query_delete);
            }
        }
    }

    private function deleteAllAlerts() {
        if ($this->databaseConnection()) {
            $this->db_connection->query('DELETE FROM alerta WHERE DESTINATARIO = :destinatario', array('destinatario' => $_SESSION['user_email']));
        }
    }

    public function insert($destinatario, $tipo, $mensaje, $enlace) {
        if (!filter_var($destinatario, FILTER_VALIDATE_EMAIL)) {
            return null;
        }
        if (($id_tipo = $this->getTipoId($tipo)) == null) {
            return null;
        }
        $mensaje = filter_var($mensaje, FILTER_SANITIZE_STRING);
        if ($this->databaseConnection()) {
            $this->db_connection->query('INSERT INTO alerta (FECHA, TIPO, DESTINATARIO, MENSAJE, ENLACE, RECIBIDA) VALUES (:fecha, :tipo, :destinatario, :mensaje, :enlace, :recibida)', array('fecha' => date('Y-m-d'), 'tipo' => $id_tipo, 'destinatario' => $destinatario, 'mensaje' => $mensaje, 'enlace' => $enlace, 'recibida' => 0));
        }
    }

    private function getTipoId($tipo) {
        if ($this->databaseConnection()) {
            return $this->db_connection->single('SELECT idTIPO FROM alerta_tipos WHERE NOMBRE= :tipo', array('tipo' => $tipo));
        }
    }

    protected function databaseConnection() {
        global $DB;
        // if connection already exists
        if ($this->db_connection != null) {
            return true;
        } else {
            $this->db_connection = $DB;
            return true;
        }
        // default return
        return false;
    }

}
