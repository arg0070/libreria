<?php
require ROOT. DS. 'app'. DS. 'lib'. DS. 'PHPMailer.php';

/**
 * Handles the user registration
 * @author Panique
 * @link http://www.php-login.net
 * @link https://github.com/panique/php-login-advanced/
 * @license http://opensource.org/licenses/MIT MIT License
 */
class Registration {

    /**
     * @var object $db_connection The database connection
     */
    private $db_connection = null;

    /**
     * @var bool success state of registration
     */
    public $registration_successful = false;

    /**
     * @var bool success state of verification
     */
    public $verification_successful = false;

    /**
     * @var array collection of error messages
     */
    public $errors = array();

    /**
     * @var array collection of success / neutral messages
     */
    public $messages = array();

    /**
     * the function "__construct()" automatically starts whenever an object of this class is created,
     * you know, when you do "$login = new Login();"
     */
    public function __construct() {
        // if we have such a POST request, call the registerNewUser() method
        if (isset($_POST["registerlib"]) || isset($_POST["registeraud"])) {
            $this->registerNewUser($_POST['user_email'], $_POST['user_password_new'], $_POST['user_password_repeat']);
            // if we have such a GET request, call the verifyNewUser() method
        }
    }

    /**
     * Checks if database connection is opened and open it if not
     */
    private function databaseConnection() {
        global $DB;
        // connection already opened
        if ($this->db_connection != null) {
            return true;
        } else {
            $this->db_connection = $DB;
            return true;
        }
    }

    /**
     * handles the entire registration process. checks all error possibilities, and creates a new user in the database if
     * everything is fine
     */
    private function registerNewUser($user_email, $user_password, $user_password_repeat) {
        // we just remove extra space on email
        $user_email = trim($user_email);

        // check provided data validity
        // TODO: check for "return true" case early, so put this first
        if (!$this->hasErrors($user_email, $user_password, $user_password_repeat)) {
            // check if we have a constant HASH_COST_FACTOR defined
            // if so: put the value into $hash_cost_factor, if not, make $hash_cost_factor = null
            $hash_cost_factor = (defined('HASH_COST_FACTOR') ? HASH_COST_FACTOR : null);

            // crypt the user's password with the PHP 5.5's password_hash() function, results in a 60 character hash string
            // the PASSWORD_DEFAULT constant is defined by the PHP 5.5, or if you are using PHP 5.3/5.4, by the password hashing
            // compatibility library. the third parameter looks a little bit shitty, but that's how those PHP 5.5 functions
            // want the parameter: as an array with, currently only used with 'cost' => XX.
            $user_password_hash = password_hash($user_password, PASSWORD_DEFAULT, array('cost' => $hash_cost_factor));
            // generate random hash for email verification (40 char string)
            $user_activation_hash = sha1(uniqid(mt_rand(), true));
            
            $user_type = (isset($_POST['registerlib'])? 'LIB': 'AUD');

            // write new users data into database
            $query_new_user_insert = $this->db_connection->query('INSERT INTO users (user_password_hash, user_email, user_activation_hash, user_registration_ip, user_registration_datetime, user_type) VALUES(:user_password_hash, :user_email, :user_activation_hash, :user_registration_ip, now(), :user_type)', array('user_password_hash' => $user_password_hash, 'user_email' => $user_email, 'user_activation_hash' => $user_activation_hash, 'user_registration_ip' => $_SERVER['REMOTE_ADDR'], 'user_type' => $user_type));
            // id of new user
            $user_id = $this->db_connection->lastInsertId();

            if ($query_new_user_insert){
                // send a verification email
                if ($this->sendVerificationEmail($user_id, $user_email, $user_activation_hash)) {
                    // when mail has been send successfully the new library or auditor register is created.
                    if ($user_type == 'LIB') {
                        $this->registerNewLibrary($user_email);
                    }
                    elseif ($user_type == 'AUD') {
                        $this->registerNewAuditor($user_email);
                    }
                    $this->messages[] = 'Su cuenta ha sido creada con éxito. Recibirá un correo electrónico para activar su cuenta.';
                    $this->registration_successful = true;
                } else {
                    // delete this users account immediately, as we could not send a verification email
                    $query_delete_user = $this->db_connection->query('DELETE FROM users WHERE user_id=:user_id', array('user_id' => $user_id));

                    $this->errors[] = 'Error al enviar el mensaje de validación. Su cuenta NO ha sido creada.';
                }
            } else {
                $this->errors[] = 'El proceso de registro ha fallado. Por favor, inténtelo de nuevo.';
            }
        }        
    }

    public function registerNewLibrary($user_email) {
        $query_new_library_insert = $this->db_connection->query('INSERT INTO libreria (user_email, RAZON_SOCIAL, CIF, NOMBRE_COMERCIAL, NOMBRE_TITULAR, DIRECCION, CP, POBLACION, PROVINCIA) VALUES(:user_email, :RAZON_SOCIAL, :CIF, :NOMBRE_COMERCIAL, :NOMBRE_TITULAR, :DIRECCION, :CP, :POBLACION, :PROVINCIA)', array('user_email' => $user_email, 'RAZON_SOCIAL' => filter_input(INPUT_POST, 'lib_razon_social'), 'CIF' => filter_input(INPUT_POST, 'lib_cif'), 'NOMBRE_COMERCIAL' => filter_input(INPUT_POST, 'lib_nombre_comercial'), 'NOMBRE_TITULAR' => filter_input(INPUT_POST, 'lib_nombre_titular'), 'DIRECCION' => filter_input(INPUT_POST, 'lib_direccion'), 'CP' => filter_input(INPUT_POST, 'lib_cp'), 'POBLACION' => filter_input(INPUT_POST, 'lib_poblacion'), 'PROVINCIA' => filter_input(INPUT_POST, 'lib_provincia')));
    }
    
    public function registerNewAuditor($user_email){
        $query_new_auditor_insert = $this->db_connection->query('INSERT INTO auditor (user_email, NOMBRE, APELLIDOS, CODIGO_SOLICITUD, HOMOLOGADO, FECHA_HOMOLOGACION) VALUES(:user_email, :NOMBRE, :APELLIDOS, :CODIGO_SOLICITUD, :HOMOLOGADO, :FECHA_HOMOLOGACION)', 
                array('user_email' => $user_email, 'NOMBRE' => filter_input(INPUT_POST, 'aud_nombre'), 'APELLIDOS' => filter_input(INPUT_POST, 'aud_apellidos'), 'CODIGO_SOLICITUD' => null, 'HOMOLOGADO' => false, 'FECHA_HOMOLOGACION' => null));
    }

    public function hasErrors($user_email, $user_password, $user_password_repeat) {
        if (empty($user_password) || empty($user_password_repeat)) {
            $this->errors[] = 'Campo de contraseña vacío.';
        } elseif ($user_password !== $user_password_repeat) {
            $this->errors[] = 'La contraseña introducida y su repetición no coinciden.';
        } elseif (strlen($user_password) < 6) {
            $this->errors[] = 'La contraseña debe tener una longitud mínima de seis (6) caracteres.';
        } if (empty($user_email)) {
            $this->errors[] = 'Campo de correo electrónico vacío.';
        } elseif (strlen($user_email) > 64) {
            $this->errors[] = 'La dirección de correo electrónico no puede tener una longitud superior a los 64 caracteres.';
        } elseif (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = 'Dirección de correo electrónico inválida.';
        } elseif ($this->databaseConnection()) {
            // check if username or email already exists
            $result = $this->db_connection->row('SELECT user_email FROM users WHERE user_email=:user_email', array('user_email' => $user_email));

            // if email find in the database
            if ($result > 0) {
                $this->errors[] = 'La dirección de correo electrónico introducida ya se encuentra registrada. Si esta es su cuenta, por favor, utilice la opción Recordar Contraseña si ha perdido acceso a ella.';
            }
        }
        if ($this->errors) {
            return true;
        }
        return false;
    }

    /*
     * sends an email to the provided email address
     * @return boolean gives back true if mail has been sent, gives back false if no mail could been sent
     */

    public function sendVerificationEmail($user_id, $user_email, $user_activation_hash) {               
          $mail = new PHPMailer;

          // please look into the config/config.php for much more info on how to use this!
          // use SMTP or use mail()
          if (EMAIL_USE_SMTP) {
          // Set mailer to use SMTP
          $mail->IsSMTP();
          //useful for debugging, shows full SMTP errors
          //$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
          // Enable SMTP authentication
          $mail->SMTPAuth = EMAIL_SMTP_AUTH;
          // Enable encryption, usually SSL/TLS
          if (defined(EMAIL_SMTP_ENCRYPTION)) {
          $mail->SMTPSecure = EMAIL_SMTP_ENCRYPTION;
          }
          // Specify host server
          $mail->Host = EMAIL_SMTP_HOST;
          $mail->Username = EMAIL_SMTP_USERNAME;
          $mail->Password = EMAIL_SMTP_PASSWORD;
          $mail->Port = EMAIL_SMTP_PORT;
          } else {
          $mail->IsMail();
          }

          $mail->From = EMAIL_VERIFICATION_FROM;
          $mail->FromName = EMAIL_VERIFICATION_FROM_NAME;
          $mail->AddAddress($user_email);
          $mail->Subject = EMAIL_VERIFICATION_SUBJECT;

          $link = EMAIL_VERIFICATION_URL.'?id='.urlencode($user_id).'&verification_code='.urlencode($user_activation_hash);

          // the link to your register.php, please set this value in config/email_verification.php
          $mail->Body = EMAIL_VERIFICATION_CONTENT.' '.$link;
      
          if(!$mail->Send()) {
          $this->errors[] = 'No se pudo enviar el correo de verificación: ' . $mail->ErrorInfo;
          return false;
          } else {              
          return true;
          }                 
    }

    /**
     * checks the id/verification code combination and set the user's activation status to true (=1) in the database
     */
    public function verifyNewUser($user_id, $user_activation_hash) {
        // if database connection opened
        if ($this->databaseConnection()) {
            // try to update user with specified information
            $query_update_user = $this->db_connection->query('UPDATE users SET user_active = 1, user_activation_hash = NULL WHERE user_id = :user_id AND user_activation_hash = :user_activation_hash', array('user_id' => intval(trim($user_id)), 'user_activation_hash' => $user_activation_hash));

            if ($query_update_user > 0) {
                $this->verification_successful = true;
                return 'Activación de cuenta correcta.';
            } else {
                return 'Error en la activación. Identificador de usuario o código de activación incorrecto.';
            }
        }
    }

    public function showErrors() {
        foreach ($this->errors as $error) {
            echo $error . '<br>';
        }
    }

    public function showMessages() {
        foreach ($this->messages as $message) {
            echo $message . '<br>';
        }
    }

}
