<?php

require_once LIB_PATH . DS . 'password_compatibility_library.php';

/**
 * handles the user login/logout/session
 * @author Panique
 * @link http://www.php-login.net
 * @link https://github.com/panique/php-login-advanced/
 * @license http://opensource.org/licenses/MIT MIT License
 */
class Login {

    /**
     * @var object $db_connection The database connection
     */
    private $db_connection = null;

    /**
     * @var int $user_id The user's id
     */
    private $user_id = null;

    /**
     * @var string $user_email The user's mail
     */
    private $user_email = "";

    /**
     * @var boolean $user_is_logged_in The user's login status
     */
    private $user_is_logged_in = false;

    /**
     * @var boolean $password_reset_link_is_valid Marker for view handling
     */
    private $password_reset_link_is_valid = false;

    /**
     * @var boolean $password_reset_was_successful Marker for view handling
     */
    private $password_reset_was_successful = false;

    /**
     * @var array $errors Collection of error messages
     */
    public $errors = array();

    /**
     * @var array $messages Collection of success / neutral messages
     */
    public $messages = array();

    /**
     * the function "__construct()" automatically starts whenever an object of this class is created,
     * you know, when you do "$login = new Login();"
     */
    public function __construct() {
        // create/read session
        session_start();

        $this->checkLoginType();
        // checking if user requested a password reset mail
        if (isset($_POST["request_password_reset"]) && isset($_POST['user_email'])) {
            $this->setPasswordResetDatabaseTokenAndSendMail(filter_input(INPUT_POST, 'user_email'));
        } elseif (isset($_GET["user_email"]) && isset($_GET["verification_code"])) {
            $this->checkIfEmailVerificationCodeIsValid(filter_input(INPUT_GET, "user_email"), filter_input(INPUT_GET, "verification_code"));
        } elseif (isset($_POST["submit_new_password"])) {
            $this->editNewPassword(filter_input(INPUT_POST, 'user_email'), filter_input(INPUT_POST, 'user_password_reset_hash'), filter_input(INPUT_POST, 'user_password_new'), filter_input(INPUT_POST, 'user_password_repeat'));
        } else if (isset($_GET["id"]) && isset($_GET["verification_code"])) {
            $registro = new Registration();
            $this->messages[] = $registro->verifyNewUser(filter_input(INPUT_GET, "id"), filter_input(INPUT_GET, "verification_code"));
        }

        //unset($_POST);
    }

    public function checkLoginType() {

        // TODO: organize this stuff better and make the constructor very small
        // TODO: unite Login and Registration classes ?
        // check the possible login actions:
        // 1. logout (happen when user clicks logout button)
        // 2. login via session data (happens each time user opens a page on your php project AFTER he has successfully logged in via the login form)
        // 3. login via cookie
        // 4. login via post data, which means simply logging in via the login form. after the user has submit his login/password successfully, his
        //    logged-in-status is written into his session data on the server. this is the typical behaviour of common login scripts.
        // if user tried to log out
        if (isset($_GET["logout"])) {
            $this->doLogout();
            header("Location: inicio");

            // if user has an active session on the server
        } elseif (!empty($_SESSION['user_email']) && ($_SESSION['user_logged_in'] == 1)) {
            $this->loginWithSessionData();

            // checking for form submit from editing screen
            // user try to change his username
            if (isset($_POST["user_edit_submit_email"])) {
                // function below uses use $_SESSION['user_id'] et $_SESSION['user_email']
                $this->editUserEmail(filter_input(INPUT_POST, 'user_email'));
                // user try to change his password
            } elseif (isset($_POST["user_edit_submit_password"])) {
                // function below uses $_SESSION['user_name'] and $_SESSION['user_id']
                $this->editUserPassword(filter_input(INPUT_POST, 'user_password_old'), filter_input(INPUT_POST, 'user_password_new'), filter_input(INPUT_POST, 'user_password_repeat'));
            }

            // login with cookie
        } elseif (isset($_COOKIE['rememberme'])) {
            $this->loginWithCookieData();

            // if user just submitted a login form
        } elseif (isset($_POST["login"])) {
            if (!isset($_POST['user_rememberme'])) {
                $_POST['user_rememberme'] = null;
            }
            // $this->loginWithPostData(filter_input(INPUT_POST, 'user_mail'), filter_input(INPUT_POST,'user_password'), filter_input(INPUT_POST,'user_rememberme'));
            $this->loginWithPostData($_POST['user_mail'], $_POST['user_password'], $_POST['user_rememberme']);
        }
    }

    /**
     * Checks if database connection is opened. If not, then this method tries to open it.
     * @return bool Success status of the database connecting process
     */
    private function databaseConnection() {
        global $DB;
        // if connection already exists
        if ($this->db_connection != null) {
            return true;
        } else {
            $this->db_connection = $DB;
            return true;
        }
        // default return
        return false;
    }

    /**
     * Search into database for the user data of user_name specified as parameter
     * @return user data as an object if existing user
     * @return false if user_name is not found in the database
     * TODO: @devplanete This returns two different types. Maybe this is valid, but it feels bad. We should rework this.
     * TODO: @devplanete After some resarch I'm VERY sure that this is not good coding style! Please fix this.
     */
    private function getUserData($user_email) {
        // if database connection opened
        if ($this->databaseConnection()) {
            // database query, getting all the info of the selected user
            $query_user = $this->db_connection->row('SELECT * FROM users WHERE user_email = :user_email', array("user_email" => $user_email), PDO::FETCH_OBJ);
            // get result row (as an object)
            return $query_user;
        } else {
            return false;
        }
    }

    /**
     * Logs in with S_SESSION data.
     * Technically we are already logged in at that point of time, as the $_SESSION values already exist.
     */
    private function loginWithSessionData() {
        $this->user_email = $_SESSION['user_email'];

        // set logged in status to true, because we just checked for this:
        // !empty($_SESSION['user_name']) && ($_SESSION['user_logged_in'] == 1)
        // when we called this method (in the constructor)
        $this->user_is_logged_in = true;
    }

    /**
     * Logs in via the Cookie
     * @return bool success state of cookie login
     */
    private function loginWithCookieData() {
        if (isset($_COOKIE['rememberme'])) {
            // extract data from the cookie
            list ($user_id, $token, $hash) = explode(':', $_COOKIE['rememberme']);
            // check cookie hash validity
            if ($hash == hash('sha256', $user_id . ':' . $token . COOKIE_SECRET_KEY) && !empty($token)) {
                // cookie looks good, try to select corresponding user
                if ($this->databaseConnection()) {
                    // get real token from database (and all other data)
                    $$result_row = $this->db_connection->row('SELECT user_id, user_email, user_type FROM users WHERE user_id = :user_id
                                                      AND user_rememberme_token = :user_rememberme_token AND user_rememberme_token IS NOT NULL', array('user_id' => $user_id, 'user_rememberme_token' => $token), PDO::FETCH_OBJ);

                    if (isset($result_row->user_id)) {
                        // write user data into PHP SESSION [a file on your server]
                        $_SESSION['user_id'] = $result_row->user_id;
                        $_SESSION['user_email'] = $result_row->user_email;
                        $_SESSION['user_type'] = $result_row->user_type;
                        $_SESSION['user_logged_in'] = 1;

                        // declare user id, set the login status to true
                        $this->user_id = $result_row->user_id;
                        $this->user_email = $result_row->user_email;
                        $this->user_is_logged_in = true;

                        // Cookie token usable only once
                        $this->newRememberMeCookie();
                        return true;
                    }
                }
            }
            // A cookie has been used but is not valid... we delete it
            $this->deleteRememberMeCookie();
            $this->errors[] = 'Cookie de sesión inválida.';
        }
        return false;
    }

    /**
     * Logs in with the data provided in $_POST, coming from the login form
     * @param $user_name
     * @param $user_password
     * @param $user_rememberme
     */
    private function loginWithPostData($user_email, $user_password, $user_rememberme) {
        if (empty($user_email)) {
            $this->errors[] = 'Dirección de correo electrónico no introducida.';
        } else if (empty($user_password)) {
            $this->errors[] = 'Contraseña no introducida.';

            // if POST data (from login form) contains non-empty user_name and non-empty user_password
        } else {
            // user can login with his username or his email address.
            // if user has not typed a valid email address, we try to identify him with his user_name
            if ($this->databaseConnection()) {
                // database query, getting all the info of the selected user
                $result_row = $this->db_connection->row('SELECT * FROM users WHERE user_email = :user_email', array('user_email' => trim($user_email)), PDO::FETCH_OBJ);
            }

            // if this user not exists
            if (!isset($result_row->user_id)) {
                // was MESSAGE_USER_DOES_NOT_EXIST before, but has changed to MESSAGE_LOGIN_FAILED
                // to prevent potential attackers showing if the user exists
                $this->errors[] = 'Fallo al iniciar sesión';
            } else if (($result_row->user_failed_logins >= 3) && ($result_row->user_last_failed_login > (time() - 30))) {
                $this->errors[] = 'Ha introducido una contraseña inválida 3 o más veces. Por favor, espere 30 segundos para volverlo a intentar.';
                // using PHP 5.5's password_verify() function to check if the provided passwords fits to the hash of that user's password
            } else if (!password_verify($user_password, $result_row->user_password_hash)) {
                // increment the failed login counter for that user
                $sth = $this->db_connection->query('UPDATE users '
                        . 'SET user_failed_logins = user_failed_logins+1, user_last_failed_login = :user_last_failed_login '
                        . 'WHERE user_email = :user_email', array('user_email' => $user_email, 'user_last_failed_login' => time()));

                $this->errors[] = 'Contraseña incorrecta. Inténtelo de nuevo.';
                // has the user activated their account with the verification email
            } else if ($result_row->user_active != 1) {
                $this->errors[] = 'Su cuenta todavía no ha sido activada. Por favor, pulse en el enlace de confirmación enviado a su dirección de correo electrónico.';
            } else {
                // write user data into PHP SESSION [a file on your server]
                $_SESSION['user_id'] = $result_row->user_id;
                $_SESSION['user_email'] = $result_row->user_email;
                $_SESSION['user_type'] = $result_row->user_type;
                $_SESSION['user_logged_in'] = 1;

                // declare user id, set the login status to true
                $this->user_id = $result_row->user_id;
                $this->user_email = $result_row->user_email;
                $this->user_is_logged_in = true;

                // reset the failed login counter for that user
                $sth = $this->db_connection->query('UPDATE users '
                        . 'SET user_failed_logins = 0, user_last_failed_login = NULL '
                        . 'WHERE user_id = :user_id AND user_failed_logins != 0', array('user_id' => $result_row->user_id));

                // if user has check the "remember me" checkbox, then generate token and write cookie
                if (isset($user_rememberme)) {
                    $this->newRememberMeCookie();
                } else {
                    // Reset remember-me token
                    $this->deleteRememberMeCookie();
                }

                // OPTIONAL: recalculate the user's password hash
                // DELETE this if-block if you like, it only exists to recalculate users's hashes when you provide a cost factor,
                // by default the script will use a cost factor of 10 and never change it.
                // check if the have defined a cost factor in config/hashing.php
                if (defined('HASH_COST_FACTOR')) {
                    // check if the hash needs to be rehashed
                    if (password_needs_rehash($result_row->user_password_hash, PASSWORD_DEFAULT, array('cost' => HASH_COST_FACTOR))) {

                        // calculate new hash with new cost factor
                        $user_password_hash = password_hash($user_password, PASSWORD_DEFAULT, array('cost' => HASH_COST_FACTOR));

                        // TODO: this should be put into another method !?
                        $query_update = $this->db_connection->query('UPDATE users SET user_password_hash = :user_password_hash WHERE user_id = :user_id', array('user_password_hash' => $user_password_hash, 'user_id' => $result_row->user_id));

                        if ($query_update == 0) {
                            // writing new hash was successful. you should now output this to the user ;)
                        } else {
                            // writing new hash was NOT successful. you should now output this to the user ;)
                        }
                    }
                }
            }
        }
    }

    /**
     * Create all data needed for remember me cookie connection on client and server side
     */
    private function newRememberMeCookie() {
        // if database connection opened
        if ($this->databaseConnection()) {
            // generate 64 char random string and store it in current user data
            $random_token_string = hash('sha256', mt_rand());
            $sth = $this->db_connection->query("UPDATE users SET user_rememberme_token = :user_rememberme_token WHERE user_id = :user_id", array('user_rememberme_token' => $random_token_string, 'user_id' => $_SESSION['user_id']));

            // generate cookie string that consists of userid, randomstring and combined hash of both
            $cookie_string_first_part = $_SESSION['user_id'] . ':' . $random_token_string;
            $cookie_string_hash = hash('sha256', $cookie_string_first_part . COOKIE_SECRET_KEY);
            $cookie_string = $cookie_string_first_part . ':' . $cookie_string_hash;

            // set cookie
            setcookie('rememberme', $cookie_string, time() + COOKIE_RUNTIME, "/", COOKIE_DOMAIN);
        }
    }

    /**
     * Delete all data needed for remember me cookie connection on client and server side
     */
    private function deleteRememberMeCookie() {
        // if database connection opened
        if ($this->databaseConnection()) {
            // Reset rememberme token
            $sth = $this->db_connection->query("UPDATE users SET user_rememberme_token = NULL WHERE user_id = :user_id", array('user_id' => $_SESSION['user_id']));
        }

        // set the rememberme-cookie to ten years ago (3600sec * 365 days * 10).
        // that's obivously the best practice to kill a cookie via php
        // @see http://stackoverflow.com/a/686166/1114320
        setcookie('rememberme', false, time() - (3600 * 3650), '/', COOKIE_DOMAIN);
    }

    /**
     * Perform the logout, resetting the session
     */
    public function doLogout() {
        $this->deleteRememberMeCookie();

        $_SESSION = array();
        session_destroy();

        $this->user_is_logged_in = false;
        $this->messages[] = 'Desconectado.';
    }

    /**
     * Simply return the current state of the user's login
     * @return bool user's login status
     */
    public function isUserLoggedIn() {
        return $this->user_is_logged_in;
    }

    /**
     * Edit the user's email, provided in the editing form
     */
    public function editUserEmail($user_email) {
        // prevent database flooding
        $user_email = substr(trim($user_email), 0, 64);

        if (!empty($user_email) && $user_email == $_SESSION["user_email"]) {
            $this->errors[] = 'La dirección de correo introducida es la misma que la actual. Por favor, seleccione otra.';
            // user mail cannot be empty and must be in email format
        } elseif (empty($user_email) || !filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = 'La dirección de correo introducida no sigue un formato válido.';
        } else if ($this->databaseConnection()) {
            // check if new email already exists
            $result_row = $this->db_connection->row('SELECT * FROM users WHERE user_email = :user_email', array('user_email' => $user_email), PDO::FETCH_OBJ);

            // if this email exists
            if (isset($result_row->user_id)) {
                $this->errors[] = 'La dirección de correo introducida ya se encuentra registrada.';
            } else {
                // write users new data into database
                $query_edit_user_email = $this->db_connection->query('UPDATE users SET user_email = :user_email WHERE user_id = :user_id', array('user_email' => $user_email, 'user_id' => $_SESSION['user_id']));
                if ($_SESSION['user_type'] == 'LIB') {
                    $query_edit_role_email = $this->db_connection->query('UPDATE libreria SET user_email = :user_email WHERE user_email = :user_oldmail', array('user_email' => $user_email, 'user_oldmail' => $_SESSION['user_email']));
                } elseif ($_SESSION['user_type'] == 'AUD') {
                    $query_edit_role_email = $this->db_connection->query('UPDATE auditor SET user_email = :user_email WHERE user_email = :user_oldmail', array('user_email' => $user_email, 'user_oldmail' => $_SESSION['user_email']));
                } else {
                    $query_edit_role_email = 1;
                }

                if ($query_edit_user_email > 0 && $query_edit_role_email > 0) {
                    $_SESSION['user_email'] = $user_email;
                    $this->messages[] = 'Su dirección de correo electrónico ha sido modificada. Su nuevo correo es ' . $user_email;
                } else {
                    $this->errors[] = 'Error al modificar su dirección de correo electrónico. Por favor, inténtelo más tarde.';
                }
            }
        }
    }

    /**
     * Edit the user's password, provided in the editing form
     */
    public function editUserPassword($user_password_old, $user_password_new, $user_password_repeat) {
        if (empty($user_password_new) || empty($user_password_repeat) || empty($user_password_old)) {
            $this->errors[] = 'Campo de contraseña vacío.';
            // is the repeat password identical to password
        } elseif ($user_password_new !== $user_password_repeat) {
            $this->errors[] = 'La contraseña nueva y su repetición no coinciden.';
            // password need to have a minimum length of 6 characters
        } elseif (strlen($user_password_new) < 6) {
            $this->errors[] = 'Su nueva contraseña debe tener una longitu mínima de 6 caracteres.';

            // all the above tests are ok
        } else {
            // database query, getting hash of currently logged in user (to check with just provided password)
            $result_row = $this->getUserData($_SESSION['user_email']);

            // if this user exists
            if (isset($result_row->user_password_hash)) {

                // using PHP 5.5's password_verify() function to check if the provided passwords fits to the hash of that user's password
                if (password_verify($user_password_old, $result_row->user_password_hash)) {

                    // now it gets a little bit crazy: check if we have a constant HASH_COST_FACTOR defined (in config/hashing.php),
                    // if so: put the value into $hash_cost_factor, if not, make $hash_cost_factor = null
                    $hash_cost_factor = (defined('HASH_COST_FACTOR') ? HASH_COST_FACTOR : null);

                    // crypt the user's password with the PHP 5.5's password_hash() function, results in a 60 character hash string
                    // the PASSWORD_DEFAULT constant is defined by the PHP 5.5, or if you are using PHP 5.3/5.4, by the password hashing
                    // compatibility library. the third parameter looks a little bit shitty, but that's how those PHP 5.5 functions
                    // want the parameter: as an array with, currently only used with 'cost' => XX.
                    $user_password_hash = password_hash($user_password_new, PASSWORD_DEFAULT, array('cost' => $hash_cost_factor));

                    // write users new hash into database
                    $query_update = $this->db_connection->query('UPDATE users SET user_password_hash = :user_password_hash WHERE user_id = :user_id', array('user_password_hash' => $user_password_hash, 'user_id' => $_SESSION['user_id']));


                    // check if exactly one row was successfully changed:
                    if ($query_update > 0) {
                        $this->messages[] = 'Contraseña cambiada correctamente.';
                    } else {
                        $this->errors[] = 'Se ha producido un error al modificar la contraseña. Por favor, inténtelo más tarde.';
                    }
                } else {
                    $this->errors[] = 'Su contraseña actual no es correcta.';
                }
            } else {
                $this->errors[] = 'Error al recuperar sus datos.';
            }
        }
    }

    /**
     * Sets a random token into the database (that will verify the user when he/she comes back via the link
     * in the email) and sends the according email.
     */
    public function setPasswordResetDatabaseTokenAndSendMail($user_email) {
        $user_email = trim($user_email);

        if (empty($user_email) || !filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = 'No ha introducido una dirección de correo válida.';
        } else {
            // generate timestamp (to see when exactly the user (or an attacker) requested the password reset mail)
            // btw this is an integer ;)
            $temporary_timestamp = time();
            // generate random hash for email password reset verification (40 char string)
            $user_password_reset_hash = sha1(uniqid(mt_rand(), true));
            // database query, getting all the info of the selected user
            $result_row = $this->getUserData($user_email);

            // if this user exists
            if (isset($result_row->user_id)) {

                // database query:
                $query_update = $this->db_connection->query('UPDATE users SET user_password_reset_hash = :user_password_reset_hash,
                                                               user_password_reset_timestamp = :user_password_reset_timestamp
                                                               WHERE user_email = :user_email', array('user_password_reset_hash' => $user_password_reset_hash,
                    'user_password_reset_timestamp' => $temporary_timestamp,
                    'user_email' => $user_email));

                // check if exactly one row was successfully changed:
                if ($query_update == 1) {
                    // send a mail to the user, containing a link with that token hash string
                    $this->sendPasswordResetMail($user_email, $user_password_reset_hash);
                    return true;
                } else {
                    $this->errors[] = 'Error al actualizar la base de datos. Por favor, inténtelo de nuevo más tarde.';
                }
            } else {
                $this->errors[] = 'La dirección de correo introducida no se encuentra registrada en el sistema.';
            }
        }
        // return false (this method only returns true when the database entry has been set successfully)
        return false;
    }

    /**
     * Sends the password-reset-email.
     */
    public function sendPasswordResetMail($user_email, $user_password_reset_hash) {

        $mail = new PHPMailer;

        // please look into the config/config.php for much more info on how to use this!
        // use SMTP or use mail()
        if (EMAIL_USE_SMTP) {
            // Set mailer to use SMTP
            $mail->IsSMTP();
            //useful for debugging, shows full SMTP errors
            //$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
            // Enable SMTP authentication
            $mail->SMTPAuth = EMAIL_SMTP_AUTH;
            // Enable encryption, usually SSL/TLS
            if (defined(EMAIL_SMTP_ENCRYPTION)) {
                $mail->SMTPSecure = EMAIL_SMTP_ENCRYPTION;
            }
            // Specify host server
            $mail->Host = EMAIL_SMTP_HOST;
            $mail->Username = EMAIL_SMTP_USERNAME;
            $mail->Password = EMAIL_SMTP_PASSWORD;
            $mail->Port = EMAIL_SMTP_PORT;
        } else {
            $mail->IsMail();
        }

        $mail->From = EMAIL_PASSWORDRESET_FROM;
        $mail->FromName = EMAIL_PASSWORDRESET_FROM_NAME;
        $mail->AddAddress($user_email);
        $mail->Subject = EMAIL_PASSWORDRESET_SUBJECT;

        $link = EMAIL_PASSWORDRESET_URL . '?user_email=' . urlencode($user_email) . '&verification_code=' . urlencode($user_password_reset_hash);
        $mail->Body = EMAIL_PASSWORDRESET_CONTENT . ' ' . $link;

        if (!$mail->Send()) {
            $this->errors[] = 'No se pudo enviar el correo de recuperación: ' . $mail->ErrorInfo;
            return false;
        } else {
            $this->messages[] = 'Su correo de recuperación ha sido enviado correctamente.';
            return true;
        }


        return true;
    }

    /**
     * Checks if the verification string in the account verification mail is valid and matches to the user.
     */
    public function checkIfEmailVerificationCodeIsValid($user_email, $verification_code) {
        $user_email = trim($user_email);

        if (empty($user_email) || empty($verification_code)) {
            $this->errors[] = 'Correo o código de verificación inválido';
        } else {
            // database query, getting all the info of the selected user
            $result_row = $this->getUserData($user_email);

            // if this user exists and have the same hash in database
            if (isset($result_row->user_id) && $result_row->user_password_reset_hash == $verification_code) {

                $timestamp_one_hour_ago = time() - 3600; // 3600 seconds are 1 hour

                if ($result_row->user_password_reset_timestamp > $timestamp_one_hour_ago) {
                    // set the marker to true, making it possible to show the password reset edit form view
                    $this->password_reset_link_is_valid = true;
                    $_SESSION['password_reset_valid'] = true;
                    $_SESSION['password_reset_email'] = $user_email;
                    $_SESSION['verification_code'] = $verification_code;
                    header('Location: ayudacuenta');
                } else {
                    $this->errors[] = 'Su código de recuperación ha expirado. Por favor, utilize el código de recuperación en la hora siguiente a su solicitud.';
                }
            } else {
                $this->errors[] = 'La dirección de correo no se encuentra registrada en el sistema o el código es inválido.';
            }
        }
    }

    /**
     * Checks and writes the new password.
     */
    public function editNewPassword($user_email, $user_password_reset_hash, $user_password_new, $user_password_repeat) {
        // TODO: timestamp!
        $user_email = trim($user_email);

        if (empty($user_email) || empty($user_password_reset_hash) || empty($user_password_new) || empty($user_password_repeat)) {
            $this->errors[] = 'Por favor, rellene todos los campos.';
            // is the repeat password identical to password
        } else if ($user_password_new !== $user_password_repeat) {
            $this->errors[] = 'La contraseña introducida y su repetición no coinciden.';
            // password need to have a minimum length of 6 characters
        } else if (strlen($user_password_new) < 6) {
            $this->errors[] = 'Su nueva contraseña debe tener una longitud mínima de seis (6) caracteres.';
            // if database connection opened
        } else if ($this->databaseConnection()) {
            // now it gets a little bit crazy: check if we have a constant HASH_COST_FACTOR defined (in config/hashing.php),
            // if so: put the value into $hash_cost_factor, if not, make $hash_cost_factor = null
            $hash_cost_factor = (defined('HASH_COST_FACTOR') ? HASH_COST_FACTOR : null);

            // crypt the user's password with the PHP 5.5's password_hash() function, results in a 60 character hash string
            // the PASSWORD_DEFAULT constant is defined by the PHP 5.5, or if you are using PHP 5.3/5.4, by the password hashing
            // compatibility library. the third parameter looks a little bit shitty, but that's how those PHP 5.5 functions
            // want the parameter: as an array with, currently only used with 'cost' => XX.
            $user_password_hash = password_hash($user_password_new, PASSWORD_DEFAULT, array('cost' => $hash_cost_factor));

            // write users new hash into database
            $query_update = $this->db_connection->query('UPDATE users SET user_password_hash = :user_password_hash,
                                                           user_password_reset_hash = NULL, user_password_reset_timestamp = NULL
                                                           WHERE user_email = :user_email AND user_password_reset_hash = :user_password_reset_hash', array('user_password_hash' => $user_password_hash,
                'user_password_reset_hash' => $user_password_reset_hash,
                'user_email' => $user_email));

            // check if exactly one row was successfully changed:
            if ($query_update == 1) {
                $_SESSION['password_reset_valid'] = false;
                $_SESSION['password_reset_email'] = null;
                $_SESSION['verification_code'] = null;
                $this->password_reset_was_successful = true;
                $this->messages[] = 'Contraseña cambiada con éxito.';
            } else {
                $this->errors[] = 'Error al cambiar su contraseña. Por favor, inténtelo de nuevo más tarde.';
            }
        }
    }

    /**
     * Gets the success state of the password-reset-link-validation.
     * TODO: should be more like getPasswordResetLinkValidationStatus
     * @return boolean
     */
    public function passwordResetLinkIsValid() {
        return $this->password_reset_link_is_valid;
    }

    /**
     * Gets the success state of the password-reset action.
     * TODO: should be more like getPasswordResetSuccessStatus
     * @return boolean
     */
    public function passwordResetWasSuccessful() {
        return $this->password_reset_was_successful;
    }

    public function showErrors() {
        foreach ($this->errors as $error) {
            echo $error . '<br>';
        }
    }

    public function showMessages() {
        foreach ($this->messages as $message) {
            echo $message . '<br>';
        }
    }

}
