<?php

class Util {

    private static $months = array('January' => 'Enero', 'February' => 'Febrero', 'March' => 'Marzo', 'April' => 'Abril', 'May' => 'Mayo', 'June' => 'Junio', 'July' => 'Julio', 'August' => 'Agosto', 'September' => 'Septiembre', 'October' => 'Octubre', 'November' => 'Noviembre', 'December' => 'Diciembre');
    private static $months_min = array('Jan' => 'Ene', 'Feb' => 'Feb', 'Mar' => 'Mar', 'Apr' => 'Abr', 'May' => 'May', 'Jun' => 'Jun', 'Jul' => 'Jul', 'Aug' => 'Ago', 'Sep' => 'Sep', 'Oct' => 'Oct', 'Nov' => 'Nov', 'Dec' => 'Dic');
    private static $days = array('Monday' => 'Lunes', 'Tuesday' => 'Martes', 'Wednesday' => 'Miercoles', 'Thursday' => 'Jueves', 'Friday' => 'Viernes', 'Saturday' => 'Sábado', 'Sunday' => 'Domingo');

    /**
     * Check if a date is valid (the day exists in the calendar) in different formats.
     * @param type $date date to validate.
     */
    public static function validateDate($date) {
        // Check the format dd/mm/yyyy.
        $date_loc = explode('/', $date);
        if (count($date_loc) == 3 && (preg_match('#^(0[1-9]|[1-2][0-9]|3[0-1])/(0[1-9]|1[0-2])/[0-9]{4}$#', $date))) {
            // Check the date.
            if (checkdate($date_loc[1], $date_loc[0], $date_loc[2])) {
                return $date_loc[2] . '-' . $date_loc[1] . '-' . $date_loc[0];
            }
        }
        // Check the format yyyy-mm-dd
        $date_sql = explode('-', $date);
        if (count($date_sql) == 3 && (preg_match('#^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$#', $date))) {
            // Check the date.
            if (checkdate($date_sql[1], $date_sql[2], $date_sql[0])) {
                return $date;
            }
        }
        return false;
    }

    public static function validateTime($time) {
        $date_val = explode(':', $time);
        // Check the format hh:mm or hh:mm:ss.
        if (count($date_val) >= 2) {
            if (preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $time)) {
                return $time;
            }
        }
        return false;
    }

    public static function formatDate($date, $full = true) {
        if ($date == '0000-00-00') {
            return 'Sin asignar';
        }
        $month = self::$months[strftime("%B", strtotime($date))];
        $day = self::$days[strftime("%A", strtotime($date))];
        if ($full) {
            return strftime("$day %d de $month del %Y", strtotime($date));
        } else {
            return strftime("$month del %Y", strtotime($date));
        }
    }
    
    public static function formatDateMin($date) {
        if ($date == '0000-00-00') {
            return 'Sin asignar';
        }
        $month = self::$months_min[strftime("%b", strtotime($date))];              
            return strftime("%d $month %Y", strtotime($date));       
    }
    
    public static function formatDateLocal($date){
        if ($date == '0000-00-00') {
            return '';
        }
        $date_explode = explode('-', $date);
        return $date_explode[2].'/'.$date_explode[1].'/'.$date_explode[0];
    }
    

    public static function formatTime($time) {
        $date_val = explode(':', $time);
        return $date_val[0] . ':' . $date_val[1];
    }

    /**
     * Compara dos fechas en formato estándar yyyy-mm-dd
     * @param type $min_date fecha menor
     * @param type $max_date fecha mayor
     * @return boolean
     */
    public static function compareTwoDates($min_date, $max_date) {
        if ($min_date < $max_date) {
            return true;
        }
        return false;
    }

    public static function compareThreeDates($min_date, $med_date, $max_date) {
        if ($med_date >= $min_date && $med_date <= $max_date) {
            return true;
        }
        return false;
    }
    
    public static function compareTwoTimes($min_time, $max_time){
        if($min_time < $max_time){
            return true;
        }
        return false;
    }

}
