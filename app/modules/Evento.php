<?php

//Function to check if the request is an AJAX request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if (!defined('DS')) {
        define('DS', DIRECTORY_SEPARATOR);
    }
    if (!defined('ROOT')) {
        define('ROOT', dirname(dirname(dirname(__FILE__))));
    }
    if (!defined('DEBUG')) {
        define('DEBUG', true);
    }
    require_once(ROOT . DS . 'app' . DS . 'init.php');
    $evento = new Evento();
}

class Evento {

    private $db_connection = null;

    public function __construct() {
        if (isset($_POST["action"]) && !empty($_POST["action"])) {
            $action = $_POST["action"];
            switch ($action) {
                case "_loadEvents": $this->getEventsCalendar();
                    break;
                case "_addEvents": $this->addEvents();
                    break;
                case "_delEvents" : $this->removeEvent();
                    break;
                default: break;
            }
        }
    }
    
    public function getEventsPanel(){
        if ($this->databaseConnection()) {
            $destinatario = (isset($_SESSION['user_email'])? $_SESSION['user_email']: '');            
            $query_eventos = $this->db_connection->query('SELECT * FROM evento WHERE DESTINATARIO IS NULL OR DESTINATARIO = :destinatario AND FECHA_FIN >= :fecha ORDER BY FECHA_INICIO ASC LIMIT 4', array('destinatario' => $destinatario ,'fecha'=>date('Y-m-d H:i:s')), PDO::FETCH_OBJ);                        
            return $query_eventos;
        }
        return null;
    }

    private function getEventsCalendar() {
        if ($this->databaseConnection()) {
            $query_eventos = $this->db_connection->query('SELECT * FROM evento WHERE DESTINATARIO IS NULL', array(), PDO::FETCH_OBJ);

            if (isset($_SESSION['user_email'])) {
                $query_eventos_p = $this->db_connection->query('SELECT * FROM evento WHERE DESTINATARIO = :destinatario', array('destinatario' => $_SESSION['user_email']), PDO::FETCH_OBJ);
                $query_eventos = array_merge($query_eventos, $query_eventos_p);
            }
        }
        if ($query_eventos) {
            $css_classes = array('event-important', 'event-info', 'event-warning', 'event-inverse', 'event-success', 'event-special');
            $out = array();
            foreach ($query_eventos as $evento) {
                $out[] = array(
                    'id' => $evento->idEVENTO,
                    'title' => $evento->TITULO,
                    'url' => $evento->URL,
                    'start' => strtotime($evento->FECHA_INICIO) . '000',
                    'end' => strtotime($evento->FECHA_FIN) . '000',
                    'class' => $css_classes[rand(0, count($css_classes) - 1)]
                );
            }
            echo json_encode(array('success' => 1, 'result' => $out));
        } else {
            echo json_encode(array('success' => 1, 'error' => 'Ningun evento'));
        }
    }

    private function addEvents() {

        $titulo = filter_input(INPUT_POST, 'evt_title', FILTER_SANITIZE_STRING);
        $descripcion = filter_input(INPUT_POST, 'evt_desc', FILTER_SANITIZE_STRING);
        $fecha_inicio = filter_input(INPUT_POST, 'date_ini');
        $fecha_fin = filter_input(INPUT_POST, 'date_end');

        if ($this->databaseConnection()) {
            $query_event = $this->db_connection->query('INSERT INTO evento (TITULO, DESCRIPCION, FECHA_INICIO, FECHA_FIN) VALUES (:titulo, :descripcion, :fecha_inicio, :fecha_fin)', array('titulo' => $titulo, 'descripcion' => $descripcion, 'fecha_inicio' => $fecha_inicio, 'fecha_fin' => $fecha_fin));
            echo json_encode($query_event);
        }
    }

    protected function databaseConnection() {
        global $DB;
        // if connection already exists
        if ($this->db_connection != null) {
            return true;
        } else {
            $this->db_connection = $DB;
            return true;
        }
        // default return
        return false;
    }

    private function removeEvent() {
        if (($id = filter_var(filter_input(INPUT_POST, 'evt_id'), FILTER_VALIDATE_INT))) {
            if ($this->databaseConnection()) {
                $query_event = $this->db_connection->query('DELETE FROM evento WHERE idEVENTO = :id_evento', array('id_evento' => $id));
                echo json_encode($query_event);
            }
        }
    }

}
