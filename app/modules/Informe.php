<?php

/**
 * Comprueba si la petición proviene de una llamada Ajax, en cuyo caso se definen
 * las constantes del sistema que se cargarían si le petición se realizase desde
 * index. Este paso es necesario para contar con la conexión a la base de datos.
 */
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if (!defined('DS')) {
        define('DS', DIRECTORY_SEPARATOR);
    }
    if (!defined('ROOT')) {
        define('ROOT', dirname(dirname(dirname(__FILE__))));
    }
    if (!defined('DEBUG')) {
        define('DEBUG', true);
    }
    require_once(ROOT . DS . 'app' . DS . 'init.php');
    $informe = new Informe();
}

class Informe {

    private $extensions = array('Word2007' => 'docx', 'ODText' => 'odt', 'RTF' => 'rtf', 'HTML' => 'html', 'PDF' => 'pdf');

    public function __construct() {

        require_once LIB_PATH . DS . 'PhpWord' . DS . 'Autoloader.php';
        PhpOffice\PhpWord\Autoloader::register();


        if (isset($_POST["action"]) && !empty($_POST["action"])) {
            $action = $_POST["action"];
            switch ($action) {
                case "_exportTable": $this->exportTable();
                    break;
                default: break;
            }
        }
    }

    private function exportTable() {        
        $content = $_POST['content'];
        $header = $_POST['header'];
        


        $phpWord = new \PhpOffice\PhpWord\PhpWord();

// Every element you want to append to the word document is placed in a section.
// To create a basic section:
        $section = $phpWord->addSection();

// After creating a section, you can append elements:
        $section->addText('Informe');

        $rows = count($content[0]);
        $cols = count($content);

        $table = $section->addTable();
        $table->addRow();
        for($h = 0; $h < $cols; $h++){
            $table->addCell()->addText($header[$h]);
        }
        
        for ($r = 0; $r < $rows; $r++) {
            $table->addRow();
            for ($c = 0; $c < $cols; $c++) {
                $table->addCell()->addText($content[$c][$r]);
            }
        }

// Finally, write the document:      
        $path = $this->write($phpWord, date('Ymdhis'), 'ODText');
        echo json_encode($path);
    }

    /**
     * Write documents
     *
     * @param \PhpOffice\PhpWord\PhpWord $phpWord
     * @param string $filename
     */
    function write($phpWord, $filename, $format) {
        $extension = $this->extensions[$format];

        // Write documents                    
        $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, $format);
        $xmlWriter->save(ROOT . "/private/informes/{$filename}.{$extension}");
        return SITE_URL . "/private/informes/{$filename}.{$extension}";
    }

}
