<?php

/**
 * Description of AdminManager
 *
 * @author Daniel
 */
class ManagerADM extends Manager {

    public function __construct() {
        parent::__construct();
    }

    public function manageAuditors() {
        if ($this->databaseConnection()) {
            $query_auditors = $this->db_connection->column('SELECT idAUDITOR FROM auditor', array(), PDO::FETCH_OBJ);
            return $this->returnAuditor($query_auditors);
        }
    }

    public function auditoresHolologados() {
        if ($this->databaseConnection()) {
            $query_auditors = $this->db_connection->column('SELECT idAUDITOR FROM auditor WHERE HOMOLOGADO = 1', array(), PDO::FETCH_OBJ);
            return $this->returnAuditor($query_auditors);
        }
    }

    private function returnAuditor($query) {
        $auditores = array();
        for ($i = 0; $i < count($query); $i++) {
            $auditores[$i] = new Auditor($query[$i]);
        }
        return $auditores;
    }

    public function manageLibs() {
        $librerias = array();
        if ($this->databaseConnection()) {
            $query_libs = $this->db_connection->column('SELECT idLIBRERIA FROM libreria', array(), PDO::FETCH_OBJ);
            for ($i = 0; $i < count($query_libs); $i++) {
                $librerias[$i] = new Libreria($query_libs[$i]);
            }
            return $librerias;
        }
    }

    public function getUnassignedAudits() {
        if ($this->databaseConnection()) {
            $query_audits = $this->db_connection->column('SELECT idAUDITORIA FROM auditoria WHERE idAUDITOR is null', array(), PDO::FETCH_OBJ);
            return $this->returnAudit($query_audits);
        }
    }

    public function getFinishedAudits() {
        if ($this->databaseConnection()) {
            $query_audits = $this->db_connection->column('SELECT idAUDITORIA FROM auditoria WHERE ESTADO = 2', array(), PDO::FETCH_OBJ);
            return $this->returnAudit($query_audits);
        }
    }

    public function manageAudits() {
        if ($this->databaseConnection()) {
            $query_audits = $this->db_connection->column('SELECT idAUDITORIA FROM auditoria', array(), PDO::FETCH_OBJ);
            return $this->returnAudit($query_audits);
        }
    }

    private function returnAudit($query) {
        $auditorias = array();
        for ($i = 0; $i < count($query); $i++) {
            $auditorias[$i] = new Auditoria($query[$i]);
        }
        return $auditorias;
    }

}
