<?php

class Auditoria extends Manager {

    private $id_auditoria;
    private $tipo_auditoria;
    private $alcance;
    private $fecha_apertura;
    private $fecha_realizacion;
    private $fecha_cierre;
    private $hora_inicio;
    private $hora_fin;
    private $estado;
    private $libreria;
    private $auditor;
    private $certificado;
    public $noconformidad_tipo = array('GRAVE' => 'GRAVE', 'LEVE' => 'LEVE', 'CERRADA' => 'CERRADA');
    public $estado_desc = array(0 => 'Pendiente', 1 => 'Abierta', 2 => 'Pendiente de revisión', 3 => 'Cerrada', 4 => 'Fuera de plazo');

    public function __construct($id = null) {
        parent::__construct();
        if (filter_var($id, FILTER_VALIDATE_INT)) {
            $this->id_auditoria = $id;
            $this->checkForm();
            if ($this->databaseConnection()) {
                $query_audit = $this->db_connection->row('SELECT * FROM auditoria WHERE idAUDITORIA = :id_auditoria', array('id_auditoria' => $id), PDO::FETCH_OBJ);
            } else {
                throw new Exception('Error en el identificador de Auditoría.');
            }
            if ($query_audit) {
                $this->tipo_auditoria = $query_audit->TIPO_AUDITORIA;
                $this->alcance = $query_audit->ALCANCE;
                $this->fecha_apertura = $query_audit->FECHA_APERTURA;
                $this->fecha_realizacion = $query_audit->FECHA_REALIZACION;
                $this->fecha_cierre = $query_audit->FECHA_CIERRE;
                $this->hora_inicio = $query_audit->HORA_INICIO;
                $this->hora_fin = $query_audit->HORA_FIN;
                $this->estado = $query_audit->ESTADO;
                $this->libreria = $query_audit->idLIBRERIA;
                $this->auditor = ($query_audit->idAUDITOR == null ? null : $query_audit->idAUDITOR);
                $this->certificado = ($query_audit->idCERTIFICADO == null ? null : $query_audit->idCERTIFICADO);
            } else {
                throw new Exception("Registro no encontrado.");
            }
        }
    }

    private function checkForm() {
        if (isset($_POST['editaudit'])) {
            $this->updateAudit();
            unset($_POST);
        } elseif (isset($_POST['editnocon'])) {
            $this->updateNocon();
            $this->insertNocon();
            unset($_POST);
        } elseif (isset($_POST['editevd'])) {
            $this->updateEvidencias();
            $this->insertEvidencias();
            unset($_POST);
        } elseif (isset($_POST['setauditor'])) {
            $this->setAuditor();
            unset($_POST);
        }
    }

    public function getId() {
        return $this->id_auditoria;
    }

    public function getTipoAuditoria() {
        return $this->tipo_auditoria;
    }

    public function getAlcance() {
        return $this->alcance;
    }

    public function getFechaApertura() {
        return $this->fecha_apertura;
    }

    public function getFechaRealizacion() {
        return $this->fecha_realizacion;
    }

    public function getFechaCierre() {
        return $this->fecha_cierre;
    }

    public function getHoraInicio() {
        return $this->hora_inicio;
    }

    public function getHoraFin() {
        return $this->hora_fin;
    }

    public function getLibreria() {
        return $this->libreria;
    }

    public function getAuditor() {
        return $this->auditor;
    }

    public function getCertificado() {
        return $this->certificado;
    }

    /**
     * Estado de la auditoría.
     *  TODO: cambiar el estado con certificado nulo.
     * @return string
     */
    public function getEstado() {
        $date = date('Y-m-d');
        if ($this->estado == 0 && $this->fecha_apertura != '0000-00-00' && $this->fecha_apertura < $date) {
            $this->setEstado(1);
        } elseif ($this->estado == 1 && $this->fecha_cierre != '0000-00-00' && $this->fecha_cierre < $date) {
            $this->setEstado(4);
        }
        return $this->estado_desc[$this->estado];
    }

    public function setEstado($estado) {
        if ($this->databaseConnection()) {
            $this->db_connection->query('UPDATE auditoria SET ESTADO = :estado WHERE idAuditoria = :id_auditoria', array('estado' => $estado, 'id_auditoria' => $this->id_auditoria));
            $this->estado = $estado;
            
            $alerta = new Alerta();
            $auditor = new Auditor($this->auditor);
            $alerta->insert($auditor->getEmail(), 'AUDIT_STATUS_CHANGE', 'La siguiente auditoría se encuentra ahora '.$this->getEstado(), 'auditoria/'.$this->id_auditoria);
            $libreria = new Libreria($this->libreria);
            $alerta->insert($libreria->getEmail(), 'AUDIT_STATUS_CHANGE', 'La siguiente auditoría se encuentra ahora '.$this->getEstado(), 'auditoria/'.$this->id_auditoria);
        }
    }

    public function isConforme() {
        if ($this->estado == 2 || $this->estado == 3) {
            return true;
        }
        return false;
    }

    /**
     * Return an object list with all the disagreement. 
     * @return object list if there are disagreement | null
     */
    public function getNoConformidades() {
        $nocon = array();
        if ($this->databaseConnection()) {
            $query_noconformidad = $this->db_connection->column('SELECT idNO_CONFORMIDADES FROM no_conformidades WHERE idAUDITORIA = :id_auditoria ORDER BY idNO_CONFORMIDADES ASC', array('id_auditoria' => $this->id_auditoria));
            for ($i = 0; $i < count($query_noconformidad); $i++) {
                $nocon[$i] = new Noconformidad($query_noconformidad[$i]);
            }
            return $nocon;
        }
    }

    /**
     * Return an object list with all the evidences.
     * @return object list if there are evidences | null.
     */
    public function getEvidencias() {
        if ($this->databaseConnection()) {
            $query_evidencias = $this->db_connection->query('SELECT * FROM evidencia WHERE idAUDITORIA = :id_auditoria', array('id_auditoria' => $this->id_auditoria), PDO::FETCH_OBJ);
            if ($query_evidencias) {
                return $query_evidencias;
            }
        }
        return null;
    }

    /**
     * Update the information of an audit.    
     * TODO: fecha_apertura editable? 
     */
    public function updateAudit() {
        $alcance = filter_input(INPUT_POST, 'alcance_audit', FILTER_SANITIZE_STRING);

        $date_apertura = filter_input(INPUT_POST, 'fecha_apertura_audit');
        $date_cierre = filter_input(INPUT_POST, 'fecha_cierre_audit');
        $date_realizacion = filter_input(INPUT_POST, 'fecha_realizacion_audit');

        $time_inicio = filter_input(INPUT_POST, 'inicio_audit');
        $time_fin = filter_input(INPUT_POST, 'fin_audit');

        $estado = (filter_input(INPUT_POST, 'conforme_audit') == 'SI' ? 2 : $this->estado);

        list(, $fecha_cierre, $fecha_realizacion) = $this->validateDates($date_apertura, $date_cierre, $date_realizacion);
        list($hora_inicio, $hora_fin) = $this->validateTime($time_inicio, $time_fin);

        if (!$this->errors && $this->databaseConnection()) {
            $this->db_connection->query('UPDATE auditoria SET ALCANCE = :alcance, FECHA_REALIZACION = :fecha_realizacion, FECHA_CIERRE = :fecha_cierre, HORA_INICIO = :hora_inicio, HORA_FIN = :hora_fin, ESTADO = :estado WHERE idAuditoria = :id_auditoria', array('alcance' => $alcance, 'fecha_realizacion' => $fecha_realizacion, 'fecha_cierre' => $fecha_cierre, 'hora_inicio' => $hora_inicio, 'hora_fin' => $hora_fin, 'estado' => $estado, 'id_auditoria' => $this->id_auditoria));
        }
    }

    /**
     * Validate time format and interval.
     * @param $hora_inicio hora de inicio de la auditoria
     * @param $hora_fin hora de fon de la auditoria
     * @return array with formated time.
     */
    private function validateTime($hora_inicio, $hora_fin) {
        if (!($format_inicio = Util::validateTime($hora_inicio))) {
            $this->errors[] = $this->timeError($hora_inicio, 'inicio');
        }
        if (!($format_fin = Util::validateTime($hora_fin))) {
            $this->errors[] = $this->timeError($hora_fin, 'fin');
        }
        if (!(Util::compareTwoTimes($hora_inicio, $hora_fin))) {
            $this->errors[] = $this->timeError2($hora_inicio, 'inicio', $hora_fin, 'fin');
        }
        return array($format_inicio, $format_fin);
    }

    /**
     * Validate date format and interval.
     * @param type $values array with dates and description.
     * @return array with dates.
     */
    private function validateDates($fecha_apertura, $fecha_cierre, $fecha_realizacion = '') {
        if (!($format_apertura = Util::validateDate($fecha_apertura))) {
            $this->dateError($fecha_apertura, 'apertura');
        }

        if (!($format_cierre = Util::validateDate($fecha_cierre))) {
            $this->dateError($fecha_cierre, 'cierre');
        }

        if (!Util::compareTwoDates($format_apertura, $format_cierre)) {
            $this->dateError2($fecha_apertura, 'apertura', $fecha_cierre, 'cierre');
        }

        if ($fecha_realizacion == '' || $fecha_realizacion == '0000-00-00') {
            $format_realizacion = '0000-00-00';
        } else {
            if (!($format_realizacion = Util::validateDate($fecha_realizacion))) {
                $this->dateError($fecha_realizacion, 'realización');
            }
            if (!Util::compareThreeDates($format_apertura, $format_realizacion, $format_cierre)) {
                $this->dateError3($fecha_apertura, 'apertura', $fecha_realizacion, 'realización', $fecha_cierre, 'cierre');
            }
        }
        return array($format_apertura, $format_cierre, $format_realizacion);
    }

    /**
     * Obtiene los valores de los registros de no conformidad del formulario.
     * @param int $i identificador del registrp
     * @return array valores de los campos del registro
     */
    private function getNoconPost($i) {
        $tipo = filter_input(INPUT_POST, 'tipo_nocon_' . $i, FILTER_SANITIZE_STRING);
        $descripcion = filter_input(INPUT_POST, 'desc_nocon_' . $i, FILTER_SANITIZE_STRING);
        $fecha_deteccion = filter_input(INPUT_POST, 'detect_nocon_' . $i);
        $fecha_resolucion = filter_input(INPUT_POST, 'resol_nocon_' . $i);
        return array($tipo, $descripcion, $fecha_deteccion, $fecha_resolucion);
    }

    /**
     * Actualiza los campos de los registros de no conformidad.
     */
    private function updateNocon() {
        $nocon = $this->getNoConformidades();
        for ($i = 1; $i <= count($nocon); $i++) {
            $id = filter_input(INPUT_POST, 'id_nocon_' . $i, FILTER_VALIDATE_INT);
            if ($nocon[$i - 1]->getId() == $id) {
                list($tipo, $descripcion, $fecha_deteccion, $fecha_resolucion) = $this->getNoconPost($i);
                $errors = $nocon[$i - 1]->update($tipo, $descripcion, $fecha_deteccion, $fecha_resolucion);
                if ($errors != '') {
                    $this->errors[] = $errors;
                }
            }
        }
    }

    /**
     * Inserta los nuevos registros de no conformidad.
     */
    private function insertNocon() {
        $i = count($this->getNoConformidades()) + 1;
        $nocon = new Noconformidad();
        while (isset($_POST['tipo_nocon_' . $i])) {
            list($tipo, $descripcion, $fecha_deteccion, $fecha_resolucion) = $this->getNoconPost($i);
            $errors = $nocon->insert($tipo, $descripcion, $fecha_deteccion, $fecha_resolucion, $this->id_auditoria);
            if ($errors != '') {
                $this->errors[] = $errors;
            }
            $i++;
        }
    }

    public function updateEvidencias() {
        $toupdate = count($this->getEvidencias());
        for ($i = 1; $i <= $toupdate; $i++) {
            $id = filter_input(INPUT_POST, 'id_evd_' . $i);
            if (!filter_var($id, FILTER_VALIDATE_INT)) {
                $this->errors[] = 'Error al actualizar el registro ' . $i . '. Identificador inválido.';
            } else {
                $this->db_connection->bind("nombre", filter_input(INPUT_POST, 'name_evd_' . $i));
                $this->db_connection->bind("desc", filter_input(INPUT_POST, 'desc_evd_' . $i));
                $this->db_connection->bind("id", $id);
                $this->db_connection->query("UPDATE evidencia SET NOMBRE = :nombre, DESCRIPCION = :desc WHERE idEVIDENCIA = :id");
            }
        }
    }

    public function insertEvidencias() {
        $i = count($this->getEvidencias()) + 1;
        while (isset($_POST['name_evd_' . $i])) {
            $this->db_connection->bind("nombre", filter_input(INPUT_POST, 'name_evd_' . $i));
            $this->db_connection->bind("desc", filter_input(INPUT_POST, 'desc_evd_' . $i));
            $this->db_connection->bind("id_auditoria", $this->id_auditoria);
            $this->db_connection->query("INSERT INTO evidencia (NOMBRE, DESCRIPCION, idAUDITORIA) VALUES (:nombre, :desc, :id_auditoria)");
            $i++;
        }
    }

    public function getAuditorDoc($autor) {
        return $this->getDocs($autor, $this->id_auditoria, 'AUD_AUDIT_DOC');
    }

    public function getLibDoc($autor) {
        return $this->getDocs($autor, $this->id_auditoria, 'LIB_AUDIT_DOC');
    }

    public function setAuditor() {
        $date_apertura = filter_input(INPUT_POST, 'fecha_apertura_audit');
        $date_cierre = filter_input(INPUT_POST, 'fecha_cierre_audit');
        list($fecha_apertura, $fecha_cierre, ) = $this->validateDates($date_apertura, $date_cierre);

        $id_auditor = filter_input(INPUT_POST, 'id_auditor');
        if (!filter_var($id_auditor, FILTER_VALIDATE_INT)) {
            $this->errors[] = 'No se ha seleccionado un auditor.';
        }
        if (!$this->errors && $this->databaseConnection()) {
            $this->db_connection->query('UPDATE auditoria SET idAUDITOR = :id_auditor, FECHA_APERTURA = :fecha_apertura, FECHA_CIERRE = :fecha_cierre  WHERE idAuditoria = :id_auditoria', array('id_auditor' => $id_auditor, 'fecha_apertura' => $fecha_apertura, 'fecha_cierre' => $fecha_cierre, 'id_auditoria' => $this->id_auditoria));
        }
    }

    /**
     * Create a new Audit.
     */
    public function newAuditoria($tipo, $id_libreria = null) {
        if (!isset($id_libreria)) {
            $id_libreria = $this->libreria;
        }
        if ($tipo == parent::CERTIFICACION) {
            $this->db_connection->query('INSERT INTO auditoria (TIPO_AUDITORIA,idLIBRERIA, ESTADO) values (:tipo_auditoria,:id_libreria, :estado)', array('tipo_auditoria' => $tipo, 'id_libreria' => $id_libreria, 'estado' => 0));        
        }else {
            $id_certificado = ($tipo == parent::RENOVACION ? null : $this->certificado);
            $fecha_apertura = date('Y-m-d', strtotime('+1 year'));
            $fecha_cierre = date('Y-m-d', strtotime('+ 1 year 3 months'));
            $this->db_connection->query('INSERT INTO auditoria (TIPO_AUDITORIA, FECHA_APERTURA, FECHA_CIERRE, ESTADO, idLIBRERIA, idCERTIFICADO) values (:tipo_auditoria,:fecha_apertura, :fecha_cierre, :estado, :id_libreria, :id_certificado)', array('tipo_auditoria' => $tipo, 'fecha_apertura' => $fecha_apertura, 'fecha_cierre' => $fecha_cierre, 'estado' => 0, 'id_libreria' => $id_libreria, 'id_certificado' => $id_certificado));
        }
        return $this->db_connection->lastInsertId();
    }

    public function setCertificado($id_certificado) {
        $this->db_connection->query('UPDATE auditoria SET idCERTIFICADO = :id_certificado WHERE idAUDITORIA = :id_auditoria', array('id_certificado' => $id_certificado, 'id_auditoria' => $this->id_auditoria));
        $this->certificado = $id_certificado;
    }
    

}
