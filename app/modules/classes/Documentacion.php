<?php

/**
 * Description of Documentacion
 *
 * @author Daniel
 */
class Documentacion {

    private $id;
    private $ruta;
    private $fecha;
    private $autor;
    private $id_ref;
    private $tipo;
    private $db_connection;

    public function __construct($id) {
        if ($this->databaseConnection()) {
            if (filter_var($id, FILTER_VALIDATE_INT)) {
                $query_doc = $this->db_connection->row('SELECT * FROM documentacion WHERE idDOCUMENTACION = :id', array('id' => $id), PDO::FETCH_OBJ);
            } else {
                throw new Exception('Error en el identificador o tipo de documento.');
            }

            if ($query_doc) {
                $this->id = $id;
                $this->ruta = $query_doc->RUTA;
                $this->fecha = $query_doc->FECHA;
                $this->autor = $query_doc->AUTOR;
                $this->id_ref = $query_doc->idREF;
                $this->tipo = $this->idtipo2str($query_doc->TIPO);
            } else {
                throw new Exception('Error al recuperar los datos del documento.');
            }
        }
    }

    private function databaseConnection() {
        global $DB;
        // if connection already exists
        if ($this->db_connection != null) {
            return true;
        } else {
            $this->db_connection = $DB;
            return true;
        }
        // default return
        return false;
    }

    public function getId() {
        return $this->id;
    }

    public function getRuta() {
        return $this->ruta;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getAutor() {
        return $this->autor;
    }

    public function getIdRef() {
        return $this->id_ref;
    }

    public function getTipo() {
        return $this->tipo;
    }

    private function idtipo2str($tipo) {
        $query_types = $this->db_connection->query('SELECT idTIPO, NOMBRE FROM documentacion_tipos', PDO::FETCH_OBJ);
      
      
        for ($i = 0; $i < count($query_types); $i++) {
            if ($query_types[$i]['idTIPO'] == $tipo) {
                return $query_types[$i]['NOMBRE'];
            }
        }
        return null;
    }
    
    public function getName(){
        $match = array();
        preg_match('/[a-zA-Z0-9]+\.[a-zA-Z0-9]+$/', $this->ruta, $match);
        return $match[0];
    }
}
