<?php

/**
 * Description of Auditor
 *
 * @author Daniel
 */
class Auditor extends Manager {

    //private $db_connection;
    private $id;
    private $email;
    private $nombre;
    private $apellidos;
    private $codigo_solicitud;
    private $homologado = false;
    private $fecha_homologacion;
    private $audits = array();

    public function __construct($id) {
        parent::__construct();
        if ($this->databaseConnection()) {
            if (filter_var($id, FILTER_VALIDATE_EMAIL)) {
                $query_aud = $this->db_connection->row('SELECT * FROM auditor WHERE user_email = :id', array('id' => $id), PDO::FETCH_OBJ);
            } elseif (filter_var($id, FILTER_VALIDATE_INT)) {
                $query_aud = $this->db_connection->row('SELECT * FROM auditor WHERE idAUDITOR = :id', array('id' => $id), PDO::FETCH_OBJ);
            } else {
                throw new Exception('Error en el identificador del auditor.');
            }
            if ($query_aud) {
                $this->id = $query_aud->idAUDITOR;
                $this->email = $query_aud->user_email;
                $this->nombre = $query_aud->NOMBRE;
                $this->apellidos = $query_aud->APELLIDOS;
                $this->codigo_solicitud = $query_aud->CODIGO_SOLICITUD;
                $this->homologado = $query_aud->HOMOLOGADO;
                $this->fecha_homologacion = $query_aud->FECHA_HOMOLOGACION;
                $this->checkForm();
            } else {
                throw new Exception('Error al recuperar los datos del auditor.');
            }
        }
    }
    
    private function checkForm() {
        if (isset($_POST['editaud'])) {
            $this->edit();            
            unset($_POST);        
        }
    }

    public function getId() {
        return $this->id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getApellidos() {
        return $this->apellidos;
    }

    public function getCodigoSolicitud() {
        return $this->codigo_solicitud;
    }

    public function isHomologado() {
        return $this->homologado;
    }

    public function getFechaHomologacion() {
        return $this->fecha_homologacion;
    }

    public function getAudits($cerrada) {    
        if ($this->isHomologado()) {
            $audits_id = $this->db_connection->column('SELECT idAUDITORIA FROM auditoria WHERE idAUDITOR = :id_auditor ORDER BY FECHA_REALIZACION DESC', array('id_auditor' => $this->id));
            for ($i = 0; $i < count($audits_id); $i++) {
                $this->audits[$i] = new Auditoria($audits_id[$i]);
            }
        }
        if (count($this->audits) == 0) {            
            return null;
        }
        $audits = array();
        
        foreach ($this->audits as $audit) {
            if (($audit->getCertificado() != null) == $cerrada) {
                $audits[] = $audit;
            }
        }
        if (count($audits) == 0) {            
            return null;
        }
        return $audits;
    }
    
    public function getDocumentacion() {
        return parent::getDocs($this->email, $this->id, 'AUD_DOC');
    }
    
    private function edit() {        
        $homologado = filter_input(INPUT_POST, 'homologado') == 'SI' ? 1 : 0;
        $msg = filter_input(INPUT_POST, 'homo_msg', FILTER_SANITIZE_STRING);
        $date = ($homologado == 1 ? date('Y-m-d') : '0000-00-00');
        
        if(isset($_POST['sendemail']) && (filter_input(INPUT_POST, 'sendemail') == 1)){
           // Send email
        }

        
        if ($this->databaseConnection()) {            
            $query_edit = $this->db_connection->query('UPDATE auditor SET HOMOLOGADO = :homologado, FECHA_HOMOLOGACION = :fecha WHERE idAUDITOR = :id_auditor', array('homologado' => $homologado, 'fecha'=>$date, 'id_auditor' => $this->id));
            if ($query_edit) {                  
                $this->homologado = $homologado;
                $this->fecha_homologacion = $date;
                $alerta = new Alerta();
                $alerta->insert($this->email, 'AUD_STATUS_CHANGE', $msg, '#');
            }
        }
    }
}
