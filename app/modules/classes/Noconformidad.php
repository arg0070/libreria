<?php

class Noconformidad extends Manager {

    private $id;
    private $tipo;
    private $descripcion;
    private $fecha_deteccion;
    private $fecha_resolucion;
    private $id_auditoria;

    public function __construct($id = null) {
        parent::__construct();
        if (filter_var($id, FILTER_VALIDATE_INT)) {
            $this->id = $id;
            if ($this->databaseConnection()) {
                $query_nocon = $this->db_connection->row('SELECT * FROM no_conformidades WHERE idNO_CONFORMIDADES= :id', array('id' => $id), PDO::FETCH_OBJ);
            } 
            if ($query_nocon) {
                $this->tipo = $query_nocon->TIPO;
                $this->descripcion = $query_nocon->DESCRIPCION;
                $this->fecha_deteccion = $query_nocon->FECHA_DETECCION;
                $this->fecha_resolucion = $query_nocon->FECHA_RESOLUCION;
                $this->id_auditoria = $query_nocon->idAUDITORIA;
            } else {
                throw new Exception("Registro no encontrado.");
            }
        }
    }

    public function getId() {
        return $this->id;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getFechaDeteccion() {
        return $this->fecha_deteccion;
    }

    public function getFechaResolucion() {
        return $this->fecha_resolucion;
    }

    public function getIdAuditoria() {
        return $this->id_auditoria;
    }

    public function getDocumentacion($id_autor) {
        return $this->getDocs($id_autor, $this->id, 'LIB_AUDIT_NOCON');
    }

    /**
     * Actualización de una no conformidad existente.
     * @param type $tipo tipo de no conformidad @see Auditoria
     * @param type $descripcion descripción
     * @param type $fecha_deteccion fecha de detección
     * @param type $fecha_resolucion fecha de resolución
     * @return string errores en la validación de datos.
     */
    public function update($tipo, $descripcion, $fecha_deteccion, $fecha_resolucion) {
        list($fecha_deteccion, $fecha_resolucion) = $this->validateDates($fecha_deteccion, $fecha_resolucion);
        if (!$this->errors && $this->databaseConnection()) {
            $this->db_connection->query("UPDATE no_conformidades SET TIPO = :tipo, DESCRIPCION = :desc, FECHA_DETECCION = :fecha_detec, FECHA_RESOLUCION = :fecha_resol WHERE idNO_CONFORMIDADES = :id_nocon", array('tipo' => $tipo, 'desc' => $descripcion, 'fecha_detec' => $fecha_deteccion, 'fecha_resol' => $fecha_resolucion, 'id_nocon' => $this->id));
        }
        return ($this->errors ? implode('', $this->errors) : '');
    }

    /**
     * Insertar un nuevo registro de no conformidad
     * @param type $tipo tipo de no conformidad @see Auditoria
     * @param type $descripcion descripción
     * @param type $fecha_deteccion fecha de detacción
     * @param type $fecha_resolucion fecha de resolución
     * @param type $id_auditoria id de la auditoría asociada
     * @return string errores en la validación de datos
     */
    public function insert($tipo, $descripcion, $fecha_deteccion, $fecha_resolucion, $id_auditoria) {
        list($fecha_deteccion, $fecha_resolucion) = $this->validateDates($fecha_deteccion, $fecha_resolucion);
        if (!$this->errors && $this->databaseConnection()) {
            $this->db_connection->query("INSERT INTO no_conformidades (TIPO, DESCRIPCION, FECHA_DETECCION, FECHA_RESOLUCION, idAUDITORIA) "
                    . "                                       VALUES (:tipo, :descripcion, :fecha_deteccion, :fecha_resolucion, :id_auditoria)", array('tipo' => $tipo, 'descripcion' => $descripcion, 'fecha_deteccion' => $fecha_deteccion, 'fecha_resolucion' => $fecha_resolucion, 'id_auditoria' => $id_auditoria));
        }return ($this->errors ? implode('', $this->errors) : '');
    }

    /**
     * Validación de fechas.
     * @param type $fecha_deteccion fecha de detección de la no conformidad
     * @param type $fecha_resolucion fecha de resolución de la no conformidad
     * @return array fechas validadas y formateadas
     */
    private function validateDates($fecha_deteccion, $fecha_resolucion) {
        if (!($format_deteccion = Util::validateDate($fecha_deteccion))) {
            $this->dateError($fecha_deteccion, 'detección', 'Error en fallo/mejora. ');
        }
        if ($fecha_resolucion == '' || $fecha_resolucion == '0000-00-00') {
            $format_resolucion = '0000-00-00';
        } else {
            if (!($format_resolucion = Util::validateDate($fecha_resolucion))) {
                $this->dateError($fecha_resolucion, 'resolución', 'Error en fallo/mejora. ');
            }
            if (!Util::compareTwoDates($format_deteccion, $format_resolucion)) {
                $this->dateError2($fecha_deteccion, 'detección', $fecha_resolucion, 'resolución', 'Error en fallo/mejora. ');
            }
        }
        return array($format_deteccion, $format_resolucion);
    }

}
