<?php

class Certificado extends Manager {

    private $id;
    private $fecha_emision = null;
    private $fecha_caducidad = null;
    private $fecha_envio = null;
    private $enviado = false;
    private $historico;

    /**
     * Constructor. Inicializa las variables con los datos del vertificado.
     * @param type $id id del certificado.
     */
    public function __construct($id = null) {
        parent::__construct();
        $this->id = $id;
        $this->checkForm();
        if (filter_var($this->id, FILTER_VALIDATE_INT)) {
            if ($this->databaseConnection()) {
                $query_certificado = $this->db_connection->row('SELECT * FROM certificado WHERE idCERTIFICADO = :id', array("id" => $this->id), PDO::FETCH_OBJ);
            }
            if ($query_certificado) {
                $this->fecha_emision = $query_certificado->FECHA_EMISION;
                $this->fecha_caducidad = $query_certificado->FECHA_CADUCIDAD;
                $this->fecha_envio = $query_certificado->FECHA_ENVIO;
                $this->enviado = $query_certificado->ENVIADO;
                $this->historico = $this->initHistorico();
            } else {
                throw new Exception('Error al recuperar los datos del Certificado.');
            }
        }
    }

    private function checkForm() {
        if (isset($_POST["newcert"])) {
            $this->insert();
            unset($_POST['newcert']);
        } elseif (isset($_POST["updatecert"])) {
            $this->update();
            unset($_POST);
        }
    }

    /**
     * Comprueba si el certificado ha expirado.
     */
    public function isValid() {
        return Util::compareTwoDates(date('Y-m-d'), $this->fecha_caducidad);
    }

    public function getFechaEmision() {
        return $this->fecha_emision;
    }

    public function getFechaCaducidad() {
        return $this->fecha_caducidad;
    }

    public function getFechaEnvio() {
        return $this->fecha_envio;
    }

    public function hasBeenSent() {
        return $this->enviado;
    }

    public function getHistorico() {
        return $this->historico;
    }

    private function initHistorico() {
        if ($this->id != null && $this->databaseConnection()) {
            $query_audits = $this->db_connection->column('SELECT idAUDITORIA FROM auditoria WHERE idCERTIFICADO = :id_certificado', array('id_certificado' => $this->id), PDO::FETCH_OBJ);
            $auditorias = array();
            for ($i = 0; $i < count($query_audits); $i++) {
                $auditorias[$i] = new Auditoria($query_audits[$i]);
            }
            return $auditorias;
        }
        return null;
    }

    private function getNextAuditType() {
        $this->historico = $this->initHistorico();
        if (count($this->historico) < 3) {
            return parent::MANTENIMIENTO;
        }
        return parent::RENOVACION;
    }

    private function getPost() {
        $fecha_emision_post = filter_input(INPUT_POST, 'fecha_emision');
        $fecha_caducidad_post = filter_input(INPUT_POST, 'fecha_caducidad');
        $enviado = (filter_input(INPUT_POST, 'enviado') == 'SI' ? 1 : 0);
        if ($enviado) {
            $fecha_envio_post = filter_input(INPUT_POST, 'fecha_envio');
        } else {
            $fecha_envio_post = '';
        }
        list($fecha_emision, $fecha_caducidad, $fecha_envio) = $this->validateDates($fecha_emision_post, $fecha_caducidad_post, $fecha_envio_post);
        return array($fecha_emision, $fecha_caducidad, $fecha_envio, $enviado);
    }

    public function update() {
        list($fecha_emision, $fecha_caducidad, $fecha_envio, $enviado) = $this->getPost();
        if ($this->databaseConnection()) {
            $query_update = $this->db_connection->query('UPDATE certificado SET FECHA_EMISION = :fecha_emision, FECHA_CADUCIDAD = :fecha_caducidad , FECHA_ENVIO = :fecha_envio, ENVIADO = :enviado
                                                           WHERE idCERTIFICADO = :id', array('fecha_emision' => $fecha_emision, 'fecha_caducidad' => $fecha_caducidad, 'fecha_envio' => $fecha_envio, 'enviado' => $enviado, 'id' => $this->id));
            // check if exactly one row was successfully changed:
            return ($query_update == 1);
        }
    }

    public function insert() {        
        list($fecha_emision, $fecha_caducidad, $fecha_envio, $enviado) = $this->getPost();
        if (!($id_auditoria = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT))) {
            $this->errors[] = 'Identificador de auditoría inválido.';
            exit;
        }
        $auditoria = new Auditoria($id_auditoria);   
        // Crear un nuevo certificado para las auditorías de Renovación y Certificación.
        if (!$this->errors && $this->databaseConnection() && ($auditoria->getTipoAuditoria() == parent::CERTIFICACION || $auditoria->getTipoAuditoria() == parent::RENOVACION)) {
            $this->db_connection->query("INSERT INTO certificado (FECHA_EMISION, FECHA_CADUCIDAD, FECHA_ENVIO, ENVIADO) "
                    . "                                       VALUES (:fecha_emision, :fecha_caducidad, :fecha_envio, :enviado)", array('fecha_emision' => $fecha_emision, 'fecha_caducidad' => $fecha_caducidad, 'fecha_envio' => $fecha_envio, 'enviado' => $enviado));
            $id_certificado = $this->db_connection->lastInsertId();
            $this->id = $id_certificado;
            // Establecer el id del nuevo certificado a la auditoría que se está validando.
            $auditoria->setCertificado($id_certificado);
        }

        // Cambiar el estado de la auditoría de Pendiente de Revisión a Cerrada.
        $auditoria->setEstado(3);
        
        // TODO: No crear auditoría si el certificado es rechazado.
        $new_id = $auditoria->newAuditoria($this->getNextAuditType());

        $this->messages[] = 'Se ha creado la siguiente auditoría programada. Puede modificar los datos desde la sección <a href="admin-auditPre" target="blank">Auditorías pendientes de asignación</a> o directamente pulsando el botón:'
                . '<form method="post" action="admin-auditEdit"><button class="btn btn-info" name="id" value="' . $new_id . '" onclick="submit">Editar auditoría</button></form>';
    }

    /**
     * Validación de fechas.
     * @param type $fecha_deteccion fecha de detección de la no conformidad
     * @param type $fecha_resolucion fecha de resolución de la no conformidad
     * @return array fechas validadas y formateadas
     */
    private function validateDates($fecha_emision, $fecha_caducidad, $fecha_envio) {
        if (!($format_emision = Util::validateDate($fecha_emision))) {
            $this->dateError($fecha_emision, 'emisión');
        }
        if (!($format_caducidad = Util::validateDate($fecha_caducidad))) {
            $this->dateError($fecha_caducidad, 'caducidad');
        }
        if (!Util::compareTwoDates($format_emision, $format_caducidad)) {
            $this->dateError2($fecha_emision, 'emisión', $fecha_caducidad, 'caducidad');
        }
        if ($fecha_envio == '') {
            $format_envio = '0000-00-00';
        } else {
            if (!($format_envio = Util::validateDate($fecha_envio))) {
                $this->dateError($fecha_envio, 'envío');
            }
        }
        return array($format_emision, $format_caducidad, $format_envio);
    }

}
