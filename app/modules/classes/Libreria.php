<?php

/**
 * Gestión de librerías 
 */
class Libreria extends Manager {

    private $id;
    private $email;
    private $razon_social;
    private $cif;
    private $nombre_comercial;
    private $nombre_titular;
    private $direccion;
    private $cp;
    private $poblacion;
    private $provincia;
    private $apto;
    private $audits = array();

    /**
     * Constructor
     * @param int/string $id id o correo electrónico de la librería
     * @throws Exception id o correo inválido, o usuario no encontrado.
     */
    public function __construct($id) {
        parent::__construct();
        if ($this->databaseConnection()) {
            if (filter_var($id, FILTER_VALIDATE_EMAIL)) {
                $query_library = $this->db_connection->row('SELECT * FROM libreria WHERE user_email = :id', array('id' => $id), PDO::FETCH_OBJ);
            } elseif (filter_var($id, FILTER_VALIDATE_INT)) {
                $query_library = $this->db_connection->row('SELECT * FROM libreria WHERE idLIBRERIA = :id', array('id' => $id), PDO::FETCH_OBJ);
            } else {
                throw new Exception('Error en el identificador de librería.');
            }

            if ($query_library) {
                $this->id = $query_library->idLIBRERIA;
                $this->email = $query_library->user_email;
                $this->razon_social = $query_library->RAZON_SOCIAL;
                $this->cif = $query_library->CIF;
                $this->nombre_comercial = $query_library->NOMBRE_COMERCIAL;
                $this->nombre_titular = $query_library->NOMBRE_TITULAR;
                $this->direccion = $query_library->DIRECCION;
                $this->cp = $query_library->CP;
                $this->poblacion = $query_library->POBLACION;
                $this->provincia = $query_library->PROVINCIA;
                $this->apto = $query_library->APTO;
                $this->checkForm();
            } else {
                throw new Exception('Error al recuperar los datos de la librería.');
            }
        }
    }

    private function checkForm() {
        if (isset($_POST['newaudit'])) {
            $auditoria = new Auditoria();
            $this->messages[] = $auditoria->newAuditoria($this->id, parent::CERTIFICACION);
            unset($_POST);
        } elseif (isset($_POST['editalib'])) {
            $this->edit();
            unset($_POST);
        }
    }

    public function getId() {
        return $this->id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getRaZonSocial() {
        return $this->razon_social;
    }

    public function getCIF() {
        return $this->cif;
    }

    public function getNombreComercial() {
        return $this->nombre_comercial;
    }

    public function getNombreTitular() {
        return $this->nombre_titular;
    }

    public function getDireccion() {
        return $this->direccion;
    }

    public function getCP() {
        return $this->cp;
    }

    public function getPoblacion() {
        return $this->poblacion;
    }

    public function getProvincia() {
        return $this->provincia;
    }

    public function isApto() {
        return $this->apto;
    }

    /**
     * Genera un array de auditorias en función de si se han realizado o no.
     * @param boolean $cerrada true: auditorias pasadas; false; auditorias pendientes.
     * @return null el auditor no tiene auditorias, array auditorias.
     */
    public function getAudits($cerrada) {
        $audits_id = $this->db_connection->column('SELECT idAUDITORIA FROM auditoria WHERE idLIBRERIA = :id', array('id' => $this->id));

        if (count($audits_id) == 0) {
            return null;
        }

        for ($i = 0; $i < count($audits_id); $i++) {
            $this->audits[$i] = new Auditoria($audits_id[$i]);
        }

        $audits = array();
        foreach ($this->audits as $audit) {
            if (($audit->getEstado() == 'Cerrada') == $cerrada) {
                $audits[] = $audit;
            }
        }
        if (count($audits) == 0) {
            return null;
        }
        return $audits;
    }

    /**
     * Devuelve la última auditoría insertada en la BD.
     * @return object última auditoría insertada
     */
    public function getCurrentAudit() {
        if (count($this->audits) == 0) {
            return null;
        }
        return $this->audits[0];
    }

    /**
     * Evita mostrar las auditorias sorpresa de Renovación.
     * @param Auditoria $audit datos de la auditoria
     * @return boolean mostrar o no el registro al usuario.
     */
    public function showAudit($audit) {
        return (Util::compareTwoDates($audit->getFechaRealizacion(), date('Y-m-d')) && $audit->getTipoAuditoria() == constant(get_class($this) . "::MANTENIMIENTO"));
    }

    public function getDocumentacion() {
        return parent::getDocs($this->email, $this->id, 'LIB_DOC');
    }

    private function edit() {        
        $apto = filter_input(INPUT_POST, 'apto') == 'SI' ? 1 : 0;
        $msg = filter_input(INPUT_POST, 'apto_msg', FILTER_SANITIZE_STRING);

        if(isset($_POST['sendemail']) && (filter_input(INPUT_POST, 'sendemail') == 1)){
           // Send email
        }
        
        if ($this->databaseConnection()) {            
            $query_edit = $this->db_connection->query('UPDATE libreria SET APTO = :apto WHERE idLIBRERIA = :id_libreria', array('apto' => $apto, 'id_libreria' => $this->id));
            if ($query_edit) {                  
                $this->apto = $apto;
                $alerta = new Alerta();
                $alerta->insert($this->email, 'LIB_STATUS_CHANGE', $msg, '#');
            }
        }
    }

}
