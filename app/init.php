<?php

require_once ROOT . DS . 'app' . DS . 'define.php';

if (DEBUG) {
    require_once(LIB_PATH . DS . 'Debug'.DS.'FirePHP.class.php');
    ob_start();
    $firephp = FirePHP::getInstance(true);
    global $firephp;

    /** Use:
      global $firephp;
      $firephp->log($var, 'name');
     */
}

require_once ROOT . DS . 'app' . DS . 'modules' . DS . 'DB.php';

$DB = new DB(DB_NAME, DB_USER, DB_PASS, DB_HOST, DB_PORT);
global $DB;

require_once ROOT . DS . 'app' . DS . 'modules' . DS . 'Theme.php';
require_once ROOT . DS . 'app' . DS . 'modules' . DS . 'Util.php';

require_once ROOT . DS . 'app' . DS . 'modules' . DS . 'Login.php';
require_once ROOT . DS . 'app' . DS . 'modules' . DS . 'Registration.php';
$login = new Login();
global $login;

require ROOT . DS . 'app' . DS . 'modules' . DS . 'Manager.php';
require ROOT . DS . 'app' . DS . 'modules' . DS . 'classes' . DS . 'Auditor.php';
require ROOT . DS . 'app' . DS . 'modules' . DS . 'classes' . DS . 'Auditoria.php';
require ROOT . DS . 'app' . DS . 'modules' . DS . 'classes' . DS . 'Certificado.php';
require ROOT . DS . 'app' . DS . 'modules' . DS . 'classes' . DS . 'Documentacion.php';
require ROOT . DS . 'app' . DS . 'modules' . DS . 'classes' . DS . 'Libreria.php';
require ROOT . DS . 'app' . DS . 'modules' . DS . 'classes' . DS . 'Noconformidad.php';

require_once ROOT . DS . 'app' . DS . 'modules' . DS . 'Evento.php';
require_once ROOT . DS . 'app' . DS . 'modules' . DS . 'Alerta.php';